hablaxServices.factory('MessagesService', ['$q', 'HablaxConfig', '$http', '$serializer', '$timeout', '$localStorage','AuthService','$httpParamSerializerJQLike','StoreService','CommonService','$responseProcess',
function($q, HablaxConfig, $http, $serializer, $timeout, $localStorage, AuthService, $httpParamSerializerJQLike, StoreService, CommonService, $responseProcess) {
  var _APIURL = {
    smsList: HablaxConfig.APIbaseURL + "get_sent_sms",
    smsDel: HablaxConfig.APIbaseURL + "delete/sms",
    smsDelAllDestination: HablaxConfig.APIbaseURL + "delete_sms_history_",
    smsCountryList: HablaxConfig.APIbaseURL + "get_sms_country_list",
    smsSend:HablaxConfig.APIbaseURL + "send_sms",
    smsLastDestinations:HablaxConfig.APIbaseURL + "search_last_sms_destinations_",
    smsSearchHistory:HablaxConfig.APIbaseURL + "search_sms_history_"
  };

    function _serializeData(data){
        try{
            var token = AuthService.hablaxAuth.token.token;
            data = data? data : {};
            data.token = token;
            return $httpParamSerializerJQLike(data,{
                headers : {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                }
            });
        }
        catch(e){
            throw new Error("token not provided",e);
        }
    }

    function _getCountryList(language,dfd) {
        var data2Send = $serializer.serialize({language:language});
        $http.post(_APIURL.smsCountryList, data2Send).then(function(resp) {
                resp = resp.data;
                if (resp.status == "success" && resp.countries) {
                    _countries = resp.countries;
                    dfd.resolve(resp.countries);
                } else {
                    $responseProcess.processErrorResponse(resp,dfd.reject);
                    _countries = [];
                }
            },
            function(err) {
                dfd.reject(err);
                _countries = [];
            }).catch(function(err) {
            _countries = [];
            dfd.reject(err);
        });
    }

  var _countries = [];
  return {
    sentMessagesList: function() {
      var dfd = $q.defer();
      var data2Send = $serializer.serialize();
      $http.post(_APIURL.smsList, data2Send).then(function(resp) {
          resp = resp.data;
          if (resp.status == "success") {
            dfd.resolve(resp);
          } else {
              $responseProcess.processErrorResponse(resp,dfd.reject);
          }
        },
        function(err) {
          dfd.reject(err);
        }).catch(function(err) {
        dfd.reject(err);
      });

      return dfd.promise;
    },
      deleteAllMessage: function(destination, country_code, country_iso) {
          var dfd = $q.defer();
          var data2Send = $serializer.serialize({
              destination:destination,
              country_code: country_code,
              iso: country_iso
          });
          $http.post(_APIURL.smsDelAllDestination, data2Send).then(function(resp) {
                  resp = resp.data;
                  if (resp.status == "success") {
                      dfd.resolve(resp);
                  } else {
                      $responseProcess.processErrorResponse(resp,dfd.reject);
                  }
              },
              function(err) {
                  dfd.reject(err);
              }).catch(function(err) {
                  dfd.reject(err);
              });

          return dfd.promise;
      },
    deleteMessage: function(messageRowId) {
      var dfd = $q.defer();
      var data2Send = $serializer.serialize(messageRowId);
      $http.post(_APIURL.smsDel, data2Send).then(function(resp) {
          resp = resp.data;
          if (resp.status == "success") {
            dfd.resolve(resp);
          } else {
              $responseProcess.processErrorResponse(resp,dfd.reject);
          }
        },
        function(err) {
          dfd.reject(err);
        }).catch(function(err) {
        dfd.reject(err);
      });

      return dfd.promise;
    },
    getCountryList: function() {
      var dfd = $q.defer();
      if(_countries && _countries.length>0){
        $timeout(function(){
          dfd.resolve(_countries)
        },20);
      }
      else{
          StoreService.get("hablax_lang",
              function (value) {
                  if (value) {
                      _getCountryList(value, dfd);
                  } else {
                      _getCountryList("en", dfd);
                  }
              }, function (error) {
                  _getCountryList("en", dfd);
              });
      }
      return dfd.promise;
    },
    sendMessage: function(newMessage) {
      var dfd = $q.defer();
      var data2Send = $serializer.serialize(newMessage);
      $http.post(_APIURL.smsSend, data2Send).then(function(resp) {
          resp = resp.data;
          if (resp.status == "success") {
            dfd.resolve(resp.sms);
          } else {
              $responseProcess.processErrorResponse(resp,dfd.reject);
          }
        },
        function(err) {
          dfd.reject(err);
        }).catch(function(err) {
        dfd.reject(err);
      });

      return dfd.promise;
    },
    searchLastDestinations: function(offset) {
      var dfd = $q.defer();
      var data2Send = $serializer.serialize({offset:offset});
      $http.post(_APIURL.smsLastDestinations, data2Send).then(function(resp) {
            resp = resp.data;
            if (resp.status == "success") {
              dfd.resolve(resp);
            } else {
                $responseProcess.processErrorResponse(resp,dfd.reject);
            }
          },
          function(err) {
            dfd.reject(err);
          }).catch(function(err) {
            dfd.reject(err);
          });

      return dfd.promise;
    },
    smsSearchHistory: function(destination, country_code, country_iso) {
      var dfd = $q.defer();
      var data2Send = $serializer.serialize({
          destination:destination,
          country_code: country_code,
          iso: country_iso

      });
      $http.post(_APIURL.smsSearchHistory, data2Send).then(function(resp) {
            resp = resp.data;
            if (resp.status == "success") {
              dfd.resolve(resp);
            } else {
                $responseProcess.processErrorResponse(resp,dfd.reject);
            }
          },
          function(err) {
            dfd.reject(err);
          }).catch(function(err) {
            dfd.reject(err);
          });

      return dfd.promise;
    }

  }

}]);

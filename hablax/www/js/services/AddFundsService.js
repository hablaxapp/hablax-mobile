hablaxServices.factory('AddFundsService', ['$log', '$q', 'HablaxConfig', '$http', '$serializer', '$timeout', 'AuthService','$httpParamSerializerJQLike','CommonService','$responseProcess',
function($log, $q, HablaxConfig, $http, $serializer, $timeout,AuthService, $httpParamSerializerJQLike, CommonService, $responseProcess) {
  var _APIURL = {
    process_recharges_payment: HablaxConfig.APIbaseURL + "process_funds_payment",
    getFundsAmounts: HablaxConfig.APIbaseURL + "get_funds_amounts",
    process_funds_payment_paypal: HablaxConfig.APIbaseURL + "process_funds_payment_paypal"
  };

  function _serializeData(data){
    try{
      var token = AuthService.hablaxAuth.token.token;
      data = data? data : {};
      data.token = token;
      return $httpParamSerializerJQLike(data,{
         headers : {
          'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
         }
      })
    }
    catch(e){
      throw new Error("token not provided",e);
    }
  }


  return {
    payByCard: function(params){
      var dfd = $q.defer();
      var data2Send = $serializer.serialize(params);
      $http.post(_APIURL.process_recharges_payment, data2Send).then(function(resp) {
          resp = resp.data;
          if (resp.status == "success") {
            $log.debug(resp);
            dfd.resolve(resp);

          } else {
            $responseProcess.processErrorResponse(resp, dfd.reject);
          }
        },
        function(err) {
          dfd.reject(err);
        }).catch(function(err) {
          dfd.reject(err);
        });
      return dfd.promise;
    },
    payByPayPal: function(params){
      params = params ? params : {};
      return $q(function(resolve,reject){
        $http.post(_APIURL.process_funds_payment_paypal,_serializeData(params)).then(function(res){
          res = res.data;
          if(res && res.status=="success") {
            resolve(res);
          }else{
            $responseProcess.processErrorResponse(res, reject);

          }
        },function(err){
          reject(err);
        });
      });
    },
    getFundsAmounts: function(){
      var dfd = $q.defer();
      var data2send = $serializer.serialize();
      $http.post(_APIURL.getFundsAmounts,data2send).then(function(resp){
        resp = resp.data;
        if(resp.status == "success") {
          dfd.resolve(resp);
        } else {
          $responseProcess.processErrorResponse(resp, dfd.reject);

        }
      },function(err){
        dfd.reject(err);
      }).catch(function(err){
        dfd.reject(err);
      });

      return dfd.promise;
    }
    }

}]);

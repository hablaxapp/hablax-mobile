hablaxServices.factory('SupportService', ['$q', 'HablaxConfig', '$http', '$serializer', '$timeout', '$localStorage','CommonService','$responseProcess',
function($q, HablaxConfig, $http, $serializer, $timeout, $localStorage, CommonService, $responseProcess) {
  var _APIURL = {
    getAccessNumbers: HablaxConfig.APIbaseURL + "get_access_numbers"
  };


  return {
      getAccessNumbers: function() {
      var dfd = $q.defer();
      var data2Send = $serializer.serialize();
      $http.post(_APIURL.getAccessNumbers, data2Send).then(function(resp) {
            resp = resp.data;
            if (resp.status == "success") {
              dfd.resolve(resp);
            } else {
                $responseProcess.processErrorResponse(resp,dfd.reject);
            }
          },
          function(err) {
            dfd.reject(err);
          }).catch(function(err) {
            dfd.reject(err);
          });

      return dfd.promise;
    }
}

}]);


// singleton for keeping user data
hablaxServices.factory('UserService',['$timeout','$q','$localStorage', 'HablaxConfig','$http' ,'AuthService', '$httpParamSerializerJQLike','$translate','StoreService','CommonService','$responseProcess',
 function($timeout,$q,$localStorage,HablaxConfig,$http,AuthService,$httpParamSerializerJQLike,$translate,StoreService,CommonService,$responseProcess){

  var _APIURL = {
    get_profile: HablaxConfig.APIbaseURL + "get_profile",
    get_balance: HablaxConfig.APIbaseURL + "get_balance",
    edit_profile: HablaxConfig.APIbaseURL + "edit_profile",
    delete_card: HablaxConfig.APIbaseURL + "delete/card",
    support_email: HablaxConfig.APIbaseURL + "support_email",
    support_email2:HablaxConfig.APIbaseURL +"support_email2"
  };
  function _serializeData(data){
    try{
      var token = AuthService.hablaxAuth.token.token;
      data = data? data : {};
      data.token = token;
      return $httpParamSerializerJQLike(data,{
         headers : {
          'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
         }
      })
    }
    catch(e){
      throw new Error("token not provided",e);
    }
  }
  function _serializeData2(data){
    try{
      // var token = AuthService.hablaxAuth.token.token;
      // data = data? data : {};
      // data.token = token;
      return $httpParamSerializerJQLike(data,{
         headers : {
          'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
         }
      })
    }
    catch(e){
      throw new Error("token not provided",e);
    }
  }
  var UserService = {
    userData:null,
    balance: null, // Object
    balanceUpdated: false,
    needShowCurrencyDisclaimer: true

  };

 UserService.setLanguage=function(selectedLang){
  switch (selectedLang) {
    case "en":
   $translate.use('en');
        StoreService.set('hablax_lang','en');
      break;
      case "es":
      $translate.use('es');
          StoreService.set('hablax_lang','es');
      break;
    default:
    $translate.use('en');
        StoreService.set('hablax_lang','en');
      break;

  }
 };

    StoreService.get("hablax_lang",
        function (value) {
            if (value) {
                UserService.setLanguage(value);
            } else {
                UserService.setLanguage("en");
            }
        }, function (error) {
            UserService.setLanguage("en");
        });


 function _toggleLanguage(newLang) {
     $translate.use(newLang);
     StoreService.set('hablax_lang',newLang);
     language = newLang;
     return language;
 }
 UserService.toggleLanguage=function(newLanguage, success){
     success(_toggleLanguage(newLanguage));

 };

     UserService.getCurrentLanguage=function(success){
         StoreService.get("hablax_lang",
             function (value) {
                 if (value) {
                     success(value);
                 } else {
                     success("en");
                 }
             }, function (error) {
                 success("en");
             });

     };

  UserService.setUser = function(userObj){
    UserService.userData = userObj ? userObj : null;
  };

  UserService.getUserProfile = function(options){
      console.log("getUserProfile");
    options = options ? options : {};
    var dfd = $q.defer();
    // if(UserService.userData && !options.force){
    //   $timeout(function(){
    //     dfd.resolve(UserService.userData);
    //   },20);
    // }
    // else{
      $http.post(_APIURL.get_profile,_serializeData()).then(function(res){
        res = res.data;
        if(res && res.status=="success" && res.account){
          UserService.userData = res.account;
          dfd.resolve(res.account);
        }
        else{
            $responseProcess.processErrorResponse(res);
          dfd.resolve(false);
        }
      },function(err){
        dfd.reject(err)
      });
    // }
    return dfd.promise;
  };

    function _openHelp(lang) {
        var options = null;
        var ref = window.open(encodeURI('https://www.hablax.com/app/help/'+lang), '_blank', options);
        ref.addEventListener('loadstop', function(event) {
            if (event.url.match("/app/help/close")) {
                ref.close();
            }
        });
    }
  UserService.openHelp = function(){
      StoreService.get("hablax_lang",
          function (value) {
              if (value) {
                  _openHelp(value);
              } else {
                  _openHelp("en");
              }
          }, function (error) {
              _openHelp("en");
          });

  };


  UserService.updateProfile = function(params){
    params = params ? params : {};
    var dfd = $q.defer();
    $http.post(_APIURL.edit_profile,_serializeData(params)).then(function(res){
      res = res.data;
      if(res && res.status=="success"){
        // UserService.userData = res.account;
        for(var key in params){
          if(params.hasOwnProperty(key)){
            UserService.userData[key] = params[key];
          }
        }
        dfd.resolve(UserService.userData);
      }
      else{
          $responseProcess.processErrorResponse(res,dfd.reject);
      }
    },function(err){
      dfd.reject(err)
    });
    return dfd.promise;
  };

  UserService.getBalance = function(options){
    options = options ? options : {};

    return $q(function(resolve,reject){
      $http.post(_APIURL.get_balance,_serializeData()).then(function(res){
        res = res.data;
        if(res && res.status=="success"){

          var balanceCurrent = UserService.balance;
          if (UserService.balance!=null && (res.balance!==balanceCurrent.balance || res.currency!==balanceCurrent.currency)) {
              UserService.balanceUpdated = true;
          }
          UserService.balance = {balance: res.balance, currency: res.currency};
          resolve(UserService.balance);
        }
        else{
            $responseProcess.processErrorResponse(res,reject);
        }
      },function(err){
        reject(err)
      });
    });
  };
 UserService.sendEmailToSupport2 = function(params){
      return $q(function(resolve,reject){
        $http.post(_APIURL.support_email2,_serializeData2(params)).then(function(res){
          res = res.data;
          if(res && res.status=="success"){
            resolve(res);
          }
          else{
              $responseProcess.processErrorResponse(res,reject);
          }
        },function(err){
          reject(err)
        });
      });
    };
    UserService.sendEmailToSupport = function(params){
      return $q(function(resolve,reject){
        $http.post(_APIURL.support_email,_serializeData(params)).then(function(res){
          res = res.data;
          if(res && res.status=="success"){
            resolve(res);
          }
          else{
              $responseProcess.processErrorResponse(res,reject);
          }
        },function(err){
          reject(err)
        });
      });
    };

  UserService.deleteCard = function(options){
    options = options ? options : {};

    return $q(function(resolve,reject){
      $http.post(_APIURL.delete_card,_serializeData(options)).then(function(res){
        res = res.data;
        if(res && res.status=="success"){
          resolve(res);
        }
        else{
            $responseProcess.processErrorResponse(res,reject);
        }
      },function(err){
        reject(err);
      });
    });
  };



  return UserService;
}]);

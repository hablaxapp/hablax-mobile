hablaxServices.factory('PushService', ['$q', '$http', '$httpParamSerializerJQLike', '$timeout', '$state', 'HablaxConfig', 'AuthService',
function($q, $http, $httpParamSerializerJQLike, $timeout, $state, HablaxConfig, AuthService) {

    var _APIURL = {
        submitPushToken: HablaxConfig.APIbaseURL + "submitToken",
        submitTest: "lazymining.com/os-pus-web/api.php",
        submitPushTopic: HablaxConfig.APIbaseURL + "submitTopic",
        RemoveToken: HablaxConfig.APIbaseURL + "RemoveToken",
    };

    PushService = {};

    function _serializeData(data){
        try{
            var token = AuthService.hablaxAuth.token.token;
            data = data? data : {};
            data.token = token;
            return $httpParamSerializerJQLike(data,{
                headers : {
                    'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
                }
            })
        }
        catch(e){
            throw new Error("token not provided",e);
        }
    }

    function _serializeData2(data){
        try{
            // var token = AuthService.hablaxAuth.token.token;
            // data = data? data : {};
            // data.token = token;
            return $httpParamSerializerJQLike(data,{
                headers : {
                    'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
                }
            })
        }
        catch(e){
            throw new Error("token not provided",e);
        }
    }

    PushService.submitToken = function(params) {
        var dfd = $q.defer();

        $http.post(_APIURL.submitPushToken, _serializeData(params)).then(function(resp) {
                resp = resp.data;
                dfd.resolve(resp);
            },
            function(err) {
                dfd.reject(err);
            }).catch(function(err) {
            dfd.reject(err);
        });

        return dfd.promise;
    };

    PushService.removeToken = function(pushToken) {
        var dfd = $q.defer();

        $http.post(_APIURL.RemoveToken, _serializeData2({pushToken: pushToken})).then(function(resp) {
                resp = resp.data;
                dfd.resolve(resp);
            },
            function(err) {
                dfd.reject(err);
            }).catch(function(err) {
            dfd.reject(err);
        });

        return dfd.promise;
    };


    PushService.submitTest = function(params) {
        var dfd = $q.defer();

        $http.get(_APIURL.submitTest+"?token=abcz").then(function(resp) {
                resp = resp.data;
                dfd.resolve(resp);
            },
            function(err) {
                dfd.reject(err);
            }).catch(function(err) {
            dfd.reject(err);
        });

        return dfd.promise;
    };

    PushService.submitTopic = function(params) {
        var dfd = $q.defer();
        $http.post(_APIURL.submitPushTopic, _serializeData2(params)).then(function(resp) {
                resp = resp.data;
                dfd.resolve(resp);
            },
            function(err) {
                dfd.reject(err);
            }).catch(function(err) {
            dfd.reject(err);
        });

        return dfd.promise;
    };

    PushService.redirect = function(data) {
        if (data.page != null) {
            $timeout(function () {
                if (data.page == "funds") {
                    $state.go('add-funds');
                } else if (data.page == "recharges") {
                    var params = {
                        pushCountryIso: data.country,
                        rechargeType: data.type
                    };
                    $state.go('tab.recharge', params);
                } else if (data.page == "sms") {
                    $state.go('tab.messages');
                }
            }, 50);
        }

    };

  return PushService;

}]);



hablaxServices.service('SocketService', ['socketFactory', SocketService]);

function SocketService(socketFactory){
  return socketFactory({

    ioSocket: io.connect('https://witty-chat.herokuapp.com')
    // ioSocket: io.connect('http://localhost:3000')
  });
}

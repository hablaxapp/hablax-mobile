hablaxServices.factory('NewsService', ['$q', 'HablaxConfig', '$http', '$serializer', '$timeout', '$localStorage','CommonService','$responseProcess',
function($q, HablaxConfig, $http, $serializer, $timeout, $localStorage,CommonService,$responseProcess) {
  var _APIURL = {
    get_news: HablaxConfig.APIbaseURL + "get_news"
  };

  return {
    getNews: function(page) {
      var dfd = $q.defer();
      var data2Send = $serializer.serialize();//{language:language}
      $http.post(_APIURL.get_news + "/" + page, data2Send).then(function(resp) {
            resp = resp.data;
            if (resp.status == "success") {
              dfd.resolve(resp);
            } else {
                $responseProcess.processErrorResponse(resp,dfd.reject);
            }
          },
          function(err) {
            dfd.reject(err);
          }).catch(function(err) {
            dfd.reject(err);
          });

      return dfd.promise;
    }
}

}]);

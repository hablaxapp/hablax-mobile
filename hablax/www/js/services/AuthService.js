hablaxServices.factory('AuthService', ['$q', '$rootScope', 'HablaxConfig', '$http', '$httpParamSerializerJQLike', '$localStorage', '$timeout', 'StoreService','$responseProcess',
function($q, $rootScope, HablaxConfig, $http, $httpParamSerializerJQLike, $localStorage, $timeout, StoreService, $responseProcess) {

  var _APIURL = {
    login: HablaxConfig.APIbaseURL + "login",
    signup: HablaxConfig.APIbaseURL + "signup",
    validate_phone: HablaxConfig.APIbaseURL + "validate_phone",
    get_registration_countries: HablaxConfig.APIbaseURL + "get_registration_countries",
    lost_password: HablaxConfig.APIbaseURL + "lost_password",
    lost_email: HablaxConfig.APIbaseURL + "lost_email"
  };
  $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

  function _serializeData(data) {
    return $httpParamSerializerJQLike(data, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
      }
    })
  }

  var AuthService = {};

  StoreService.get("hablaxAuth",
      function (value) {
        if (value) {
            StoreService.getObject("hablaxAuth",
                function (value) {
                    AuthService.hablaxAuth = value;
                    AuthService.hablaxToken = value.token.token;
                }, function (error) {
                    // nothing
                });

        } else {
          AuthService.hablaxAuth = null;
          AuthService.hablaxToken = null;
        }
      }, function (error) {
        AuthService.hablaxAuth = null;
        AuthService.hablaxToken = null;
      });
  AuthService.isRemembered = false;
  StoreService.get("hablaxIntroSeen",
      function (value) {
        if (value) {
          AuthService.introSeen = true
        } else {
          AuthService.introSeen = false;
        }
      }, function (error) {
        AuthService.introSeen = false;
      });
  StoreService.get("goLogin",
      function (value) {
        if (value) {
          AuthService.goLogin = true
        } else {
          AuthService.goLogin = false;
        }
      }, function (error) {
        AuthService.goLogin = false;
      });

  AuthService.setAuth = function(auth) {
    if (AuthService.isRemembered && auth && auth.token) {
      StoreService.setObject("hablaxAuth", auth);
      AuthService.hablaxAuth = auth;
      AuthService.hablaxToken = auth.token.token;
    } else {
      AuthService.hablaxAuth = auth? auth : null;
      if(auth && auth.token){
        AuthService.hablaxToken = auth.token.token;
      }
      else{
        AuthService.hablaxToken = null;
      }
    }
  };

  AuthService.login = function(name, password, remember) {
    var dfd = $q.defer();
    var data2Send = _serializeData({
      'email': name,
      'password': password
    });
    $http.post(_APIURL.login, data2Send).then(function(resp) {
        resp = resp.data;
        if (resp.status == "success" && resp.token) {
          AuthService.isRemembered = remember;
          AuthService.setAuth({
            sip: resp.sip,
            token: resp.token
          });
          $rootScope.$broadcast("updateProfile");
          $rootScope.$emit('updateBalance');
          dfd.resolve(resp);
        } else {
            if (resp.error_code == "account_restricted") {
                dfd.reject(resp);
            } else {
                $responseProcess.processErrorResponse(resp,dfd.reject);
            }

        }
      },
      function(err) {
        dfd.reject(err);
      }).catch(function(err) {
        dfd.reject(err);
      });

    return dfd.promise;
  };

  AuthService.signup = function(userObj) {
    var dfd = $q.defer();
    var data2Send = _serializeData(userObj);
    $http.post(_APIURL.signup, data2Send).then(function(resp) {
        resp = resp.data;
        if (resp.status == "success" && resp.token) {
          AuthService.setAuth({
            sip: resp.sip,
            token: resp.token
          });
          dfd.resolve(resp);
        } else {
            $responseProcess.processErrorResponse(resp,dfd.reject);
        }
      },
      function(err) {
        dfd.reject(err);
      }).catch(function(err) {
      dfd.reject(err);
    });

    return dfd.promise;
  };

  AuthService.validatePhone = function(params) {
    var dfd = $q.defer();
    var data2Send = _serializeData(params);
    $http.post(_APIURL.validate_phone, data2Send).then(function(resp) {
        resp = resp.data;
        if (resp.status == "success") {
          dfd.resolve(resp);
        } else {
            $responseProcess.processErrorResponse(resp,dfd.reject);
        }
      },
      function(err) {
        dfd.reject(err);
      }).catch(function(err) {
      dfd.reject(err);
    });

    return dfd.promise;
  };

  AuthService.lostEmail = function(params){
    return $q(function(resolve,reject){
      var data2Send = _serializeData(params);
      $http.post(_APIURL.lost_email, data2Send).then(function(resp) {
          resp = resp.data;
          if (resp.status == "success") {
            resolve(resp);
          } else {
              $responseProcess.processErrorResponse(resp,reject);
          }
        },
        function(err) {
          reject(err);
        }).catch(function(err) {
        reject(err);
      });
    });
  };

  AuthService.lostPassword = function(params){
    return $q(function(resolve,reject){
      var data2Send = _serializeData(params);
      $http.post(_APIURL.lost_password, data2Send).then(function(resp) {
          resp = resp.data;
          if (resp.status == "success") {
            resolve(resp);
          } else {
              $responseProcess.processErrorResponse(resp,reject);
          }
        },
        function(err) {
          reject(err);
        }).catch(function(err) {
        reject(err);
      });
    });
  };

  AuthService.checkSession = function() {

  };

  AuthService.registrationCountries = null;

  function _getRegistrationCountries(language,resolve,reject) {
    if (AuthService.registrationCountries != null) {
        resolve(AuthService.registrationCountries);
    } else {
        var data2Send = _serializeData({language: language});
        $http.post(_APIURL.get_registration_countries, data2Send).then(function(resp) {
                resp = resp.data;
                if (resp.status == "success" && resp.countries) {
                    resolve(resp.countries);
                } else {
                    $responseProcess.processErrorResponse(resp,reject);
                }
            },
            function(err) {
                reject(err);
            }).catch(function(err) {
            reject(err);
        });
    }

  }

  AuthService.getRegistrationCountries = function() {
    return $q(function(resolve,reject){
      StoreService.get("hablax_lang",
          function (value) {
            if (value) {
              _getRegistrationCountries(value, resolve, reject);
            } else {
              _getRegistrationCountries("en", resolve, reject);
            }
          }, function (error) {
            _getRegistrationCountries("en", resolve, reject);
          });

    })
  };



  AuthService.logout = function() {
    return $q(function(resolve,reject){
      $timeout(function(){
          StoreService.remove("hablaxAuth", function(key) {
              AuthService.setAuth(null);
              resolve({result: "success"});
          });

      },100);
    })
  };

  return AuthService;
}]);

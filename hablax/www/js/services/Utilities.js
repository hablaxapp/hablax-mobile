hablaxServices.service('cordovaReady', ['$rootScope', '$q', '$timeout', function($rootScope, $q, $timeout) {
  var loadingDeferred = $q.defer();
  var rejectTimer;
  document.addEventListener('deviceready', function() {
    $timeout.cancel(rejectTimer);
    $timeout(function() {
      $rootScope.$apply(loadingDeferred.resolve);
    });
  });
  rejectTimer = $timeout(function() {
    $rootScope.$apply(loadingDeferred.reject);
  }, 1500);

  return function cordovaReady() {
    return loadingDeferred.promise;
  };
}]);

hablaxServices.factory('$serializer', ['$httpParamSerializerJQLike', 'AuthService', function($httpParamSerializerJQLike, AuthService) {

  return {
    serialize: function(data) {
      try {
        data = data ? data : {};
        if(AuthService.hablaxAuth && AuthService.hablaxAuth.token && AuthService.hablaxAuth.token.token){
          var token = AuthService.hablaxAuth.token.token;
          data.token = token;
        }
        return $httpParamSerializerJQLike(data, {
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
          }
        })
      } catch (e) {
        throw new Error("token not provided", e);
      }
    }
  }

}]);

hablaxServices.factory('$responseProcess', ['$rootScope', '$translate', function($rootScope, $translate) {

    var self = {};
  self.isInvalidTokenErrorCode = function(errorCode) {
    return errorCode == "token_not_valid_or_not_found"
        || errorCode == "error_requesting_token"
        || errorCode == "account_restricted";
  };

  self.processErrorResponse = function(resp, reject) {
    if (self.isInvalidTokenErrorCode(resp.error_code)) {

      $rootScope.$emit('invalid_token');

    } else if (resp.error_code == "transaction_declined") {
      if (reject != null) {
        $translate(['transaction_declined_message']).then(function (translations) {
          reject(translations.transaction_declined_message);
        }, function (translationIds) {
          reject(translationIds.transaction_declined_message);
        });
      }
    } else if (resp.error_code == "transaction_declined_zip") {
      if (reject != null) {
        $translate(['transaction_declined_zip_message']).then(function (translations) {
          reject(translations.transaction_declined_zip_message);
        }, function (translationIds) {
          reject(translationIds.transaction_declined_zip_message);
        });
      }
    } else {
      if (reject != null) {
        reject(resp);
      }
    }

  };

    return self;

}]);




hablaxServices.factory('$localStorage', ['$window', function($window) {
  return {
    set: function(key, value) {
      $window.localStorage[key] = value;
    },
    get: function(key, defaultValue) {
      return $window.localStorage[key] || defaultValue;
    },
    setObject: function(key, value) {
      $window.localStorage[key] = angular.toJson(value);
    },
    getObject: function(key) {
      return angular.fromJson($window.localStorage[key] || '{}');
    },
    remove: function(key) {
      $window.localStorage.removeItem(key);
    }
  };
}]);

angular.module('hablax').filter('recent', function() {
  return function(records, switcher) {
    if (records && records.length && switcher) {
      return records.slice(0, 2);
    }
    return records;
  }
});

angular.module('hablax').filter('timeonly', function() {
  return function(date) {
    var d = moment(input).toDate();
    var hours = d.getHours();
    var mins = d.getMinutes();
    var ampm = hours > 12 ? 'PM' : 'AM';
    var hr = hours > 12 ? 24 - hours : hours;
    return hr + ':' + mins + ampm;
  }
});
angular.module('hablax').filter('dateToISO', function() {
  return function(input) {
    return moment(input).toDate().toISOString();
  };
});

hablaxServices.factory('CallService', ['$log','$q','HablaxConfig', '$http', '$serializer' ,
'AuthService', '$timeout', 'DialogService', '$ionicPlatform', 'UserService', 'StoreService', '$rootScope','CommonService','$responseProcess',
function($log, $q, HablaxConfig, $http, $serializer , AuthService,$timeout, DialogService, $ionicPlatform, UserService, StoreService, $rootScope, CommonService, $responseProcess){
  var _APIURL = {
    get_calls_history: HablaxConfig.APIbaseURL + "get_calls_history",
    delete: HablaxConfig.APIbaseURL + "delete/call",
    get_access_numbers: HablaxConfig.APIbaseURL + "get_access_numbers",
    next_call: HablaxConfig.APIbaseURL + "next_call",
    country_pattern: HablaxConfig.APIbaseURL + "country_pattern"
  };

  var proximitysensor = {
  };

  var _proximitysensorWatchStart = function(_scope, on_approch_callback) {
      if(!navigator.proximity ){
        $log.debug("cannot find navigator.proximity");
        return;
      }

      navigator.proximity.enableSensor();

      // Start watch timer to get proximity sensor value
      var frequency = 100;
      _scope.id = window.setInterval(function() {
          navigator.proximity.getProximityState(function(val) { // on success
              var timestamp = new Date().getTime();
              if(timestamp - _scope.lastemittedtimestamp > 1000) { // test on each 1 secs
                  if( proximitysensor.lastval == 1 && val == 0 ) { // far => near changed
                      _scope.lastemittedtimestamp = timestamp;
                      _scope.lastval = val;
                      on_approch_callback(timestamp);
                  }
              }
              _scope.lastval = val;
          });
      }, frequency);
  };

  var _proximitysensorWatchStop = function(_scope) {
      if(!navigator.proximity ){
        $log.debug("cannot find navigator.proximity");
        return;
      }

      window.clearInterval( _scope.id );

      navigator.proximity.disableSensor();
  };

  function activateProximity(){
    if(window.plugins && window.plugins.insomnia){
      window.plugins.insomnia.keepAwake();
    }
    _proximitysensorWatchStart(proximitysensor, function(timestamp) {
       $log.debug('approched on ' + timestamp);
    });
  }

  function deactivateProximity(){
    if(window.plugins && window.plugins.insomnia){
      window.plugins.insomnia.allowSleepAgain();
    }
    _proximitysensorWatchStop(proximitysensor);
  }

  var _signal = {
    intervalId: null,
    strength: 0
  };
  function _monitorSignal(onData, frequency){
    var isIOS = ionic.Platform.isIOS();
    var isAndroid = ionic.Platform.isAndroid();
    if(window.SignalStrength && window.SignalStrength.dbm && isAndroid){
      // Start watch timer to get signal value
      frequency = frequency ? frequency : 1000;
      _signal.intervalId = window.setInterval(function() {
        window.SignalStrength.dbm(function(db){
          // $log.debug("signal strength");
          // $log.debug(db)
          if(onData){
            onData(db);
          }
        });
      }, frequency);
    }
  }

  function _stopMonitoringSignal(){
    if(!window.SignalStrength ){
      $log.debug("cannot find window.SignalStrength");
      return;
    }

    window.clearInterval( _signal.intervalId );
  }

  function _saveCallOnServer(lang, dialNumber) {
    var data2send = $serializer.serialize({'destination': dialNumber});
    return $q(function(resolve,reject){

      $http.post(_APIURL.next_call+"/"+lang,data2send).then(function(resp){
        resp = resp.data;
        if(resp.status == "success") {
          // var _accessNumbers = resp.numbers;
          // for(var i=0,l=_accessNumbers.length;i<l;i++){
          //   _accessNumbers[i].text = _accessNumbers[i].country + " (" + _accessNumbers[i].phone + ")";
          // }
          resolve(resp);
        }
        else {
            $responseProcess.processErrorResponse(resp,reject);
        }
      },function(err){
        reject(err);
      }).catch(function(err){
        reject(err);
      });
    });
  }

  return {
    notShowAgain: false,
    activateProximity: activateProximity,
    deactivateProximity: deactivateProximity,
    monitorSignal: _monitorSignal,
    stopMonitoringSignal: _stopMonitoringSignal,
    history: function(page){
      var dfd = $q.defer();
      var data2Send = $serializer.serialize({page:page});
      $http.post(_APIURL.get_calls_history + "/" + page,data2Send).then(function(resp){
        resp = resp.data;
        if(resp.status=="success"){
          dfd.resolve(resp);
        }
        else{
            $responseProcess.processErrorResponse(resp,dfd.reject);
        }
      },
      function(err){
        dfd.reject(err);
      }).catch(function(err){
        dfd.reject(err);
      });

      return dfd.promise;
    },
    delete: function(row_id){
      var data2send = $serializer.serialize({'row_id': row_id});
      return $q(function(resolve,reject){
        $http.post(_APIURL.delete,data2send).then(function(resp){
          resp = resp.data;
          if(resp.status == "success") {
            resolve(resp)
          }
          else {
            $responseProcess.processErrorResponse(resp,reject);
          }
        },function(err){
          reject(err);
        }).catch(function(err){
          reject(err);
        });
      });
    },
    deleteAll: function(){
      var dfd = $q.defer();
      $timeout(function(){

          dfd.resolve({status: "success",msg: "Deleted Successfully."});

      },10);

      return dfd.promise;
    },
    getAccessNumbers: function(dialNumber){
      var data2send = $serializer.serialize({'destination': dialNumber});
      return $q(function(resolve,reject){
        $http.post(_APIURL.get_access_numbers,data2send).then(function(resp){
          resp = resp.data;
          if(resp.status == "success") {
            var _accessNumbers = resp.numbers;
            for(var i=0,l=_accessNumbers.length;i<l;i++){
              _accessNumbers[i].text = _accessNumbers[i].country + " (" + _accessNumbers[i].phone + ")";
            }
            resolve(_accessNumbers);
          }
          else {
            $responseProcess.processErrorResponse(resp,reject);
          }
        },function(err){
          reject(err);
        }).catch(function(err){
          reject(err);
        });
      });
    },
    standardNumber: function(dialNumber){
      var standard = dialNumber;
      if (dialNumber.indexOf('00') == 0) {
        standard = dialNumber.substring(2);
      } else if (dialNumber.indexOf('+') == 0) {
        standard = dialNumber.substring(1);
      } else if (dialNumber.indexOf('011') == 0) {
        standard = dialNumber.substring(3);
      }
      standard = "011" + standard;
      return standard;
    },
    saveCallOnServer: function(dialNumber, success){
      StoreService.get("hablax_lang",
          function (value) {
            if (value) {
              success(_saveCallOnServer(value,dialNumber));

            } else {
              success(_saveCallOnServer("en",dialNumber));
            }
          }, function (error) {
            success(_saveCallOnServer("en",dialNumber));
          });
    },
    verifyNumberForCall: function(dialNumber){
      var data2send = $serializer.serialize({'destination': dialNumber});
      return $q(function(resolve,reject){
        $http.post(_APIURL.country_pattern, data2send).then(function(resp){
          resp = resp.data;
          if(resp.status == "success" && resp.country) {
            // var _accessNumbers = resp.numbers;
            // for(var i=0,l=_accessNumbers.length;i<l;i++){
            //   _accessNumbers[i].text = _accessNumbers[i].country + " (" + _accessNumbers[i].phone + ")";
            // }
            resolve(resp.country);
          }
          else {
            $responseProcess.processErrorResponse(resp,reject);
          }
        },function(err){
          reject(err);
        }).catch(function(err){
          reject(err);
        });
      });
    }
  }

}]);

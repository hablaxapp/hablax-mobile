hablaxServices.factory('PaymentService', ['$q','HablaxConfig', '$http', '$serializer', 'AuthService' , '$timeout','CommonService', '$responseProcess', function($q, HablaxConfig, $http, $serializer , AuthService, $timeout, CommonService, $responseProcess){

  var _APIURL = {
    history: HablaxConfig.APIbaseURL + "get_payments_history",
    delete: HablaxConfig.APIbaseURL + "delete/transaction",
    getFundsAmounts: HablaxConfig.APIbaseURL + "get_funds_amounts"
  };
  //$http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
  // function _serializeData(data){
  //   try{
  //     var token = AuthService.hablaxAuth.token.token;
  //     data = data? data : {};
  //     data.token = token;
  //     return $httpParamSerializerJQLike(data,{
  //        headers : {
  //         'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
  //        }
  //     })
  //   }
  //   catch(e){
  //     throw new Error("token not provided",e);
  //   }
  // }

  return {
    history: function(page){
      var dfd = $q.defer();
      var data2Send = $serializer.serialize({page:page});
      $http.post(_APIURL.history + "/" + page,data2Send).then(function(resp){
        resp = resp.data;
        if(resp.status=="success"){
          dfd.resolve(resp);
        }
        else{
          $responseProcess.processErrorResponse(resp,dfd.reject);
        }
      },
      function(err){
        dfd.reject(err);
      }).catch(function(err){
        dfd.reject(err);
      });

      return dfd.promise;
    },
    delete: function(rowid){
      var dfd = $q.defer();
      var data2send = $serializer.serialize({'row_id': rowid});
      $http.post(_APIURL.delete,data2send).then(function(resp){
        resp = resp.data;
        if(resp.status == "success") {
          dfd.resolve(resp)
        }
        else {
            $responseProcess.processErrorResponse(resp,dfd.reject);
        }
      },function(err){
        dfd.reject(err);
      }).catch(function(err){
        dfd.reject(err);
      });

      return dfd.promise;

    },
    deleteAll: function(){
      var dfd = $q.defer();
      $timeout(function(){

          dfd.resolve({status: "success",msg: "Deleted Successfully."});

      },10);

      return dfd.promise;
    },
    getFundsAmounts: function(){
      var dfd = $q.defer();
      var data2send = $serializer.serialize();
      $http.post(_APIURL.getFundsAmounts,data2send).then(function(resp){
        resp = resp.data;
        if(resp.status == "success") {
          dfd.resolve(resp)
        }
        else {
            $responseProcess.processErrorResponse(resp,dfd.reject);
        }
      },function(err){
        dfd.reject(err);
      }).catch(function(err){
        dfd.reject(err);
      });

      return dfd.promise;
    }

  }

}]);

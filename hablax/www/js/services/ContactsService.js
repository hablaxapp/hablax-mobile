hablaxServices.factory('ContactsService', ['$log','$rootScope', '$http', '$timeout', '$localStorage', '$q','HablaxConfig','$serializer', 'cordovaReady', 'StoreService', 'CommonService','$responseProcess',
    function($log,$rootScope, $http, $timeout, $localStorage, $q,HablaxConfig,$serializer, cordovaReady, StoreService, CommonService, $responseProcess) {

  var _APIURL = {
    getContacts: HablaxConfig.APIbaseURL + "get_contacts",
    newContact: HablaxConfig.APIbaseURL + "add_contact",
    contactDel: HablaxConfig.APIbaseURL + "delete_contact",
    editContact: HablaxConfig.APIbaseURL + "edit_contact"
  };
  var service = {};
  service.phoneContacts = [];
  var formatContact = function(contact) {
    return {
      "displayName": contact.name.formatted || contact.name.givenName + " " + contact.name.familyName || "Mystery Person",
      "emails": contact.emails || [],
      "phones": contact.phoneNumbers || [],
      "photos": contact.photos || []
    };
  };
  //get unique contacts based on id
  var getUniqueContacts = function(array) {
    var unique = {};
    var distinct = [];
    for (var i in array) {
      array[i].contact_phone = array[i].contact_phone.replace(/\s/g, '');
      if (typeof(unique[array[i].contact_phone]) == "undefined") {
        distinct.push(array[i]);
      }
      unique[array[i].contact_phone] = 0;
    }
    return distinct;
  };
  var sortContacts=function(contacts) {
    // sort by name
    contacts.sort(function(a, b) {
      if (a.contact_name > b.contact_name) {
        return 1;
      } else if (a.contact_name < b.contact_name) {
        return -1;
      }
      // a must be equal to b
      return 0;
    });
    return contacts;
  };

    var processContacts = function(list, success) {
      var arr = [];
      var hash = {};
      var colorsHash = {};
      var contacts = [];
      if(list.length==undefined){
        contacts.push(list);
      }
      else{
        contacts=list;
      }

      StoreService.getObject("hablaxContactsColors",
          function (value) {
            colorsHash = value || {};
            $log.debug(JSON.stringify(contacts),contacts.length);
            for (var i = 0, len = contacts.length; i < len; i++) {
              contacts[i].number = contacts[i].contact_phone;
              contacts[i].number = contacts[i].number.replace("011"+contacts[i].country_code,"");
              contacts[i].avatarCharacter=contacts[i].contact_name.split(" ")[0].charAt(0);
              contacts[i].avatarCharacter+=contacts[i].contact_name.split(" ")[1] ? contacts[i].contact_name.split(" ")[1].charAt(0) : "";
              colorsHash[contacts[i].row_id] = "#ffffff";
              if (!colorsHash[contacts[i].row_id]) {
                contacts[i].color = "#" + Math.floor(Math.random() * 16777275).toString(16);
                if (contacts[i].color.length < 7) {
                  contacts[i].color = contacts[i].color + 1;
                }
                colorsHash[contacts[i].row_id] = contacts[i].color;
              } else {
                contacts[i].color = colorsHash[contacts[i].row_id];
              }

              while (colorsHash[contacts[i].row_id] == "#ffffff") { // no use white color, if white color, random again
                contacts[i].color = "#" + Math.floor(Math.random() * 16777275).toString(16);
                if (contacts[i].color.length < 7) {
                  contacts[i].color =contacts[i].color + 1;
                }
                colorsHash[contacts[i].row_id] = contacts[i].color;
              }

              if (contacts[i].contact_name && contacts[i].contact_name != "" && contacts[i].contact_phone && contacts[i].contact_phone != "" && !hash[contacts[i].contact_phone] && farmaishiCheck(contacts[i].contact_phone)) {
                hash[contacts[i].contact_phone] = true;
                arr.push(contacts[i]);
              }
            }
            // service.phoneContacts=arr;
            success({contacts: arr, colorsHash: colorsHash});
          }, function (error) {
            // nothing
          });



    };

    var farmaishiCheck=function(username){
      return (username.indexOf("011")==0 || username.indexOf("00")==0 || username.indexOf("+")==0);
    };
  service.getPhoneContactsNumbers = function() {
    var numbers = [];
    for (var i = 0, len = service.phoneContacts.length; i < len; i++) {
      var num = service.phoneContacts[i];
      numbers.push(num.username);
    }
    return numbers;
  };

  service.setPhoneContactNumbers = function(contacts) {
    service.phoneContacts = contacts;
  };
  service.pickContact = function() {

    var deferred = $q.defer();

    if (navigator && navigator.contacts) {

      navigator.contacts.pickContact(function(contact) {

        deferred.resolve(formatContact(contact));
      });

    } else {
      deferred.reject("Bummer.  No contacts in desktop browser");
    }

    return deferred.promise;
  };

  service.getPhoneContacts = function() {
    // display the address information for all contacts
    var deferred = $q.defer();

    function onSuccess(contacts) {
      processContacts(contacts, function(procContactsResult) {
        var procContacts = getUniqueContacts(procContactsResult.contacts);
        procContacts=sortContacts(procContacts);
        service.phoneContacts = procContacts;
        StoreService.setObject('hablaxContactsColors', procContactsResult.colorsHash);
        deferred.resolve(procContacts);
      });
      //Get only unique phonenuber contacts


    }

    var data2Send = $serializer.serialize();
    $http.post(_APIURL.getContacts, data2Send).then(function(resp) {
        resp = resp.data;
        if (resp.status == "success") {
          if (resp.contacts == "none") {
            deferred.resolve(resp.contacts);
          } else {
            onSuccess(resp.contacts);
          }

        } else {
          $responseProcess.processErrorResponse(resp,deferred.reject);
        }
      },
      function(err) {
        deferred.reject(err);
      }).catch(function(err) {
      deferred.reject(err);
    });

    return deferred.promise;
  };

  service.addContact = function(newCont) {
    var deferred = $q.defer();

    function onSuccess(result) {
      newCont.row_id = result.row_id;
      processContacts(newCont, function(addedContacts) {
        $log.debug(addedContacts);
        deferred.resolve(addedContacts.contacts);
      });

    }

    function onError(result) {
    //  alert('onError!', result.code);
      deferred.reject(result);
    }

    var data2Send = $serializer.serialize(newCont);
    $http.post(_APIURL.newContact, data2Send).then(function(resp) {
      resp = resp.data;
      if (resp.status == "success") {
      onSuccess(resp);
      } else {
        $responseProcess.processErrorResponse(resp,deferred.reject);
      }
    },
    function(err) {
        onError(err);
    }).catch(function(err) {
    onError(err);
  });

     return deferred.promise;
   };
   service.editContact = function(contact) {
     var deferred = $q.defer();
       var data2Send = $serializer.serialize(contact);
       $http.post(_APIURL.editContact, data2Send).then(function(resp) {
       resp = resp.data;
       if (resp.status == "success") {
       deferred.resolve(resp);
       } else {
         $responseProcess.processErrorResponse(resp,deferred.reject);
       }
     },
     function(err) {
           deferred.reject(err);
     }).catch(function(err) {
       deferred.reject(err);
   });

      return deferred.promise;
    };
   service.deleteContact= function(contactRowId) {
     var dfd = $q.defer();
     var data2Send = $serializer.serialize(contactRowId);
     $http.post(_APIURL.contactDel, data2Send).then(function(resp) {
         resp = resp.data;
         if (resp.status == "success") {
           dfd.resolve(resp);
         } else {
           $responseProcess.processErrorResponse(resp,dfd.reject);
         }
       },
       function(err) {
         dfd.reject(err);
       }).catch(function(err) {
       dfd.reject(err);
     });

     return dfd.promise;
   };
   service.processContacts = processContacts;
   service.sortContacts = sortContacts;
  return service;
}]);

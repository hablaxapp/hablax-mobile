hablaxServices.factory('RechargeService', ['$log','$q', 'HablaxConfig', '$http', '$serializer', '$timeout', 'AuthService', '$httpParamSerializerJQLike','$localStorage','StoreService','CommonService','$responseProcess',
function($log,$q, HablaxConfig, $http, $serializer, $timeout, AuthService,$httpParamSerializerJQLike, $localStorage,StoreService,CommonService,$responseProcess) {
  var _APIURL = {
    rechargeCountriesList: HablaxConfig.APIbaseURL + "get_countries_to_recharge",
    countryRecharges: HablaxConfig.APIbaseURL + "get_country_recharges",
    destinationCharges: HablaxConfig.APIbaseURL + "get_destination_recharges",
    get_rates: HablaxConfig.APIbaseURL + "get_rates",
    validate_nauta: HablaxConfig.APIbaseURL + "validate_nauta",
    process_recharges_payment: HablaxConfig.APIbaseURL + "process_recharges_payment",
    calculate_paypal_fee : HablaxConfig.APIbaseURL + "calculate_paypal_fee",
    process_recharges_payment_paypal: HablaxConfig.APIbaseURL + "process_recharges_payment_paypal"
  };

  function _serializeData(data){
    try{
      var token = AuthService.hablaxAuth.token.token;
      data = data? data : {};
      data.token = token;
      return $httpParamSerializerJQLike(data,{
         headers : {
          'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
         }
      })
    }
    catch(e){
      throw new Error("token not provided",e);
    }
  }

  var _countries = [];
  var _countriesToRecharge = [];
  var _rates = null;
  var recharge = {};

  var _processTopups = function(dischargeObj){
    for(var i=0,l=dischargeObj.topups.length; i<l; i++){
      dischargeObj.local_info_currency = dischargeObj.local_info_currency ?
        dischargeObj.local_info_currency : dischargeObj.topups[i].local_info_currency;
        if(dischargeObj.destination_currency){
          dischargeObj.topups[i].label
            = dischargeObj.topups[i].product + " " + dischargeObj.destination_currency + " -> " +
            dischargeObj.topups[i].local_amount + " " + dischargeObj.local_info_currency;
        }
        else{
          dischargeObj.topups[i].label
            = dischargeObj.topups[i].local_amount + " " + dischargeObj.local_info_currency;
        }
    }

    return dischargeObj.topups;
  };

  function _getCountriesToRecharge(language, dfd) {
    var data2Send = $serializer.serialize({language:language});
    $http.post(_APIURL.rechargeCountriesList, data2Send).then(function(resp) {
          resp = resp.data;
          if (resp.status == "success" && resp.countries) {
            _countriesToRecharge = resp.countries;
            dfd.resolve(resp.countries);
          } else {
            $responseProcess.processErrorResponse(resp,dfd.reject);
            _countriesToRecharge = [];
          }
        },
        function(err) {
          dfd.reject(err);
          _countriesToRecharge = [];
        }).catch(function(err) {
      _countriesToRecharge = [];
      dfd.reject(err);
    });
  }

  return {
    getCountriesToRecharge: function(lang) {
      var dfd = $q.defer();
      if(_countriesToRecharge && _countriesToRecharge.length>0){
        $timeout(function(){
          dfd.resolve(_countriesToRecharge)
        },20);
      }
      else{
        StoreService.get("hablax_lang",
            function (value) {
              if (value) {
                _getCountriesToRecharge(value, dfd);
              } else {
                _getCountriesToRecharge("en", dfd);
              }
            }, function (error) {
              _getCountriesToRecharge("en", dfd);
            });

      }
      return dfd.promise;
    },
    getCountryRecharges: function(row_id){
      var dfd = $q.defer();
      var data2Send = $serializer.serialize({row_id:row_id});
      $http.post(_APIURL.countryRecharges, data2Send).then(function(resp) {
          resp = resp.data;
          if (resp.status == "success" && resp.country) {
            if(resp.country.services && !(resp.country.services instanceof Array)){
              resp.country.services = [resp.country.services];
            }
            dfd.resolve(resp.country);
          } else {
            $responseProcess.processErrorResponse(resp,dfd.reject);
          }
        },
        function(err) {
          dfd.reject(err);
        }).catch(function(err) {
        dfd.reject(err);
      });
      return dfd.promise;
    },
    getDestinationCharges: function(destination){
      var dfd = $q.defer();
      var data2Send = $serializer.serialize({destination:destination});
      $http.post(_APIURL.destinationCharges, data2Send).then(function(resp) {
          resp = resp.data;
          if (resp.status == "success" && resp.validation && resp.validation.topups) {
            resp.validation.topups = _processTopups(resp.validation);
            dfd.resolve(resp.validation);
          } else {
            $responseProcess.processErrorResponse(resp,dfd.reject);
          }
        },
        function(err) {
          dfd.reject(err);
        }).catch(function(err) {
        dfd.reject(err);
      });
      return dfd.promise;
    },
    getExchangeRates: function(){
      var dfd = $q.defer();
      if(_rates && Object.keys(_rates).length>0){
        $timeout(function(){
          dfd.resolve(_rates)
        },20);
      }
      else{
        var data2Send = $serializer.serialize();
        $http.post(_APIURL.get_rates, data2Send).then(function(resp) {
            resp = resp.data;
            if (resp.status == "success" && resp.rates) {
              $log.debug(resp);
              _rates = resp.rates;
              dfd.resolve(resp);
            } else {
              $responseProcess.processErrorResponse(resp,dfd.reject);
            }
          },
          function(err) {
            dfd.reject(err);
          }).catch(function(err) {
          dfd.reject(err);
        });
      }
      return dfd.promise;
    },
    payByCard: function(params){
      var dfd = $q.defer();
      var data2Send = $serializer.serialize(params);
      $http.post(_APIURL.process_recharges_payment, data2Send).then(function(resp) {
          resp = resp.data;
          if (resp.status == "success") {
            $log.debug(resp);
            dfd.resolve(resp);
          } else {
            $responseProcess.processErrorResponse(resp,dfd.reject);
          }
        },
        function(err) {
          dfd.reject(err);
        }).catch(function(err) {
        dfd.reject(err);
      });
      return dfd.promise;
    },

    payByPayPal: function(params){
      params = params ? params : {};
      return $q(function(resolve,reject){
        $http.post(_APIURL.process_recharges_payment_paypal,_serializeData(params)).then(function(res){
          res = res.data;
          if(res && res.status=="success"){
            resolve(res);
          }
          else{
            $responseProcess.processErrorResponse(res,reject);
          }
        },function(err){
          reject(err);
        });
      });
    },
    calculatePayPalFee: function(params){
      params = params ? params : {};
      return $q(function(resolve,reject){
        $http.post(_APIURL.calculate_paypal_fee,_serializeData(params)).then(function(res){
          res = res.data;
          if(res && res.status=="success"){
            resolve(res);
          }
          else{
            $responseProcess.processErrorResponse(res,reject);
          }
        },function(err){
          reject(err);
        });
      });
    },
    validateNauta: function(destination,product){
      var dfd = $q.defer();
      var data2Send = $serializer.serialize({destination: destination,product: product});
      $http.post(_APIURL.validate_nauta, data2Send).then(function(resp) {
          resp = resp.data;
          if (resp.status == "success") {
            $log.debug(resp);
            dfd.resolve(resp);
          } else {
            $responseProcess.processErrorResponse(resp,dfd.reject);
          }
        },
        function(err) {
          dfd.reject(err);
        }).catch(function(err) {
        dfd.reject(err);
      });
      return dfd.promise;
    },
    validateNautaLocal: function(email){
      var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if(re.test(email)){
        //Email valid. Procees to test if it's from the right domain (Second argument is to check that the string ENDS with this domain, and that it doesn't just contain it)
        if(email.indexOf("@nauta.com.cu", email.length - "@nauta.com.cu".length) !== -1){
          //VALID
          console.log("nauta.com.cu DOMAIN VALID");
          return true;
        } else if (email.indexOf("@nauta.co.cu", email.length - "@nauta.co.cu".length) !== -1) {
          //VALID
          console.log("nauta.co.cu DOMAIN VALID");
          return true;
        }
      }
      return false;
    },
    convertUsingRates: function(input,outputCurrency){
      $log.debug(input);
      $log.debug(outputCurrency);
      var output;
      if(outputCurrency!="USD"){
        output = Math.round((input.price * _rates[outputCurrency]) * 100) / 100;
      }
      return output;
    },
    processTopups: _processTopups
  }

}]);

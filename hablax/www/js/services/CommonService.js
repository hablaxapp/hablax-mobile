hablaxServices.factory('CommonService', ['$q', 'HablaxConfig', '$http', '$serializer', '$timeout', '$localStorage','$responseProcess',
function($q, HablaxConfig, $http, $serializer, $timeout, $localStorage,$responseProcess) {

    var _APIURL = {
        appInfo: HablaxConfig.APIbaseURL
    };

    var CommonService = {
        online:false
    };

    CommonService.isOnline = function() {
        var networkState = navigator.connection.type;

        if(networkState == Connection.NONE) {
            return false;
        } else {
            return true;
        }

    };

    CommonService.updateApp = function() {
        if (ionic.Platform.isAndroid()) {
            //window.open("market://details?id=com.hablax.app", '_system');
            window.open("https://play.google.com/store/apps/details?id=com.hablax.app", '_system');
        } else if (ionic.Platform.isIOS()) {
            window.open("https://itunes.apple.com/us/app/hablax-international-calling/id1202229481", '_system');
        }

    };

    CommonService.appInfo = function() {
        var dfd = $q.defer();
        $http.get(_APIURL.appInfo).then(function(resp) {
                resp = resp.data;
                dfd.resolve(resp);
            },
            function(err) {
                dfd.reject(err);
            }).catch(function(err) {
                dfd.reject(err);
            });

        return dfd.promise;
    };

  return CommonService;

}]);


hablaxServices.factory('DialogService', ['$log','$q', 'HablaxConfig', '$http', '$serializer', '$timeout', '$ionicPopup'
,function($log, $q, HablaxConfig, $http, $serializer, $timeout, $ionicPopup) {

  var service = {};
  // A confirm dialog
  service.confirm = function(title,msg,onSuccess,onFailure) {
   var confirmPopup = $ionicPopup.confirm({
     title: title,
     template: msg
   });

   confirmPopup.then(function(res) {
     if(res) {
       $log.debug('You are sure');
       if(onSuccess){
         onSuccess();
       }
     } else {
       $log.debug('You are not sure');
       if(onFailure){
         onFailure();
       }
     }
   });
  };
  // An alert dialog
  service.alert = function(title,template,onOkCb,$scope, cssClass,tipo) {
    if(tipo!=undefined){

          var options = {
          template : template ? template : "",
          cssClass: cssClass ? cssClass : '' // String, The custom CSS class name
        };
        if (tipo=='error')
        {
          options.title='<div class="error-icon-popup"><i class="icon ion-ios-close-outline" ></i></div>';
        }
        else if(tipo=='warning')
        {
          options.title='<div class="warning-icon-popup"><i class="icon ion-alert-circled"></i></div>';
        }
         else if(tipo=='info')
        {
          options.title='<div class="info-icon-popup"><i class="icon ion-ios-information-outline"></i></div>';
        }
         else if(tipo=='success')
        {
          options.title='<div class="success-icon-popup"><i class="icon ion-ios-checkmark-outline"></i></div>';
        }else{
              var options = {
              title: title ? title: "",
              template : template ? template : "",
              cssClass: cssClass ? cssClass : '' // String, The custom CSS class name
            };
        }

    }
    else
    {
        var options = {
        title: title ? title: "",
        template : template ? template : "",
        cssClass: cssClass ? cssClass : '' // String, The custom CSS class name
      };
    }
    
    if($scope){
      options.scope = $scope;
    }
    var alertPopup = $ionicPopup.alert(options);

    alertPopup.then(function(res) {
     $log.debug('Thank you for not eating my delicious ice cream cone');
     if(onOkCb){
       onOkCb(res);
     }
    });
  };

    service.requestOption = function(config){
      config = config ? config : {};

      var saveText = config.saveText? config.saveText: 'Request option';
      var options = {
        template: config.template,
        title: config.title,
        scope: config.$scope,
        cssClass: config.cssClass ? config.cssClass : '', // String, The custom CSS class name
        buttons: [
          {
            text: config.cancelText? config.cancelText : 'No, thanks',
            onTap: function(e){
              var fn = config.onCancelCb? config.onCancelCb : angular.noop;
              fn();
            }
          },
          {
            text: '<b>' + saveText + '</b>',
            type: 'button-positive',
            onTap: function(e) {
              var fn = config.onSaveCb? config.onSaveCb : angular.noop;
              fn();
            }
          }
        ]
      };
      if(config.subTitle){
        options.subTitle = config.subTitle;
      }
      return $ionicPopup.show(options);
    };

    service.forceUpdate = function(config){
      config = config ? config : {};

      var saveText = config.saveText? config.saveText: 'Update Now';
      var options = {
        template: config.template,
        title: config.title,
        scope: config.$scope,
        cssClass: config.cssClass ? config.cssClass : '', // String, The custom CSS class name
        buttons: [
          //{
          //  text: config.cancelText? config.cancelText : 'No, leave app',
          //  onTap: function(e){
          //    var fn = config.onCancelCb? config.onCancelCb : angular.noop;
          //    fn();
          //  }
          //}
          //,
          {
            text: '<b>' + saveText + '</b>',
            type: 'button-positive',
            onTap: function(e) {
              var fn = config.onSaveCb? config.onSaveCb : angular.noop;
              fn();
            }
          }
        ]
      };
      if(config.subTitle){
        options.subTitle = config.subTitle;
      }
      $ionicPopup.show(options);
    };

  service.show = function(config){
    config = config ? config : {};

    var saveText = config.saveText? config.saveText: 'Save';
    var options = {
      template: config.template,
      title: config.title,
      scope: config.$scope,
      buttons: [
        {
          text: config.cancelText? config.cancelText : 'Cancel',
          onTap: function(e){
            var fn = config.onCancelCb? config.onCancelCb : angular.noop;
            fn();
          }
        },
        {
          text: '<b>' + saveText + '</b>',
          type: 'button-positive',
          onTap: function(e) {
            var fn = config.onSaveCb? config.onSaveCb : angular.noop;
            fn();
          }
        }
      ]
    };
    if(config.subTitle){
      options.subTitle = config.subTitle;
    }
    $ionicPopup.show(options);
  };

  return service;

}]);

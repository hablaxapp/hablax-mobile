
hablaxServices.factory("HablaxConfig",[function(){
  var Environment = function(baseUrl,debugMode,appMode){
      this.baseURL = baseUrl;
      this.APIbaseURL = baseUrl;
      this.DEBUGMODE = debugMode;
      this.appMode = appMode;
  };
  var config = {
      development: new Environment("https://www.hablax.com/app/",true,'development'),
      production: new Environment("https://www.hablax.com/app/",false,'production')
  };

  var env = config.development;
  return env;
}]);

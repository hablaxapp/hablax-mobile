hablaxServices.factory('StoreService', ['$localStorage', '$log', '$ionicPlatform',
    function($localStorage, $log, $ionicPlatform) {


        var StoreService = {};

        StoreService.get = function(key, success, failed) {
            if (ionic.Platform.isAndroid() || ionic.Platform.isIOS()) {
                $ionicPlatform.ready(function() {
                    NativeStorage.getItem(key,
                        function (value) {
                            console.log('Get ' + key + ' = ' + value);
                            if (success) {
                                success(value);
                            }
                        },
                        function (error) {
                            console.log(error.code);
                            if (error.exception !== null && error.exception !== "") console.log(error.exception);
                            var value = $localStorage.get(key);
                            if (value!=null) {
                                StoreService.set(key, value);
                                if (success) {
                                    success(value);
                                }

                            } else {
                                if (failed) {
                                    failed(error);
                                }

                            }


                        });
                });
            } else {
                success($localStorage.get(key));
            }

        };

        StoreService.set = function(key, value, success, failed) {
            if (ionic.Platform.isAndroid() || ionic.Platform.isIOS()) {
                $ionicPlatform.ready(function() {
                    NativeStorage.setItem(key,value,
                        function (obj) {
                            console.log(obj);
                            if (success) {
                                success(obj);
                            }
                        },
                        function (error) {
                            console.log(error.code);
                            if (error.exception !== null && error.exception !== "") console.log(error.exception);

                            if (failed) {
                                failed(error);
                            }
                        });
                });
            } else {
                $localStorage.set(key, value);
            }
        };

        StoreService.setObject = function(key, value, success, failed) {
            if (ionic.Platform.isAndroid() || ionic.Platform.isIOS()) {
                $ionicPlatform.ready(function() {
                    NativeStorage.setItem(key,angular.toJson(value),
                        function (obj) {
                            console.log(obj);
                            if (success) {
                                success(obj);
                            }
                        },
                        function (error) {
                            console.log(error.code);
                            if (error.exception !== null && error.exception !== "") console.log(error.exception);
                            if (failed) {
                                failed(error);
                            }
                        });
                });
            } else {
                $localStorage.setObject(key, value);
            }
        };

        StoreService.getObject = function(key, success, failed) {
            if (ionic.Platform.isAndroid() || ionic.Platform.isIOS()) {
                $ionicPlatform.ready(function() {
                    NativeStorage.getItem(key,
                        function (value) {
                            success(angular.fromJson(value|| '{}'));
                            console.log('Set ' + key + ' = ' + value);
                        },
                        function (error) {

                            var value = $localStorage.getObject(key);
                            if (value!=null) {
                                StoreService.setObject(key, value);
                                if (success) {
                                    success(value);
                                }

                            } else {
                                if (failed) {
                                    failed(error);
                                }

                            }
                            console.log('Error: ' + error);
                        });
                });
            } else {
                success($localStorage.getObject(key));
            }
        };

        StoreService.remove = function(key, success, failed) {

            if (ionic.Platform.isAndroid() || ionic.Platform.isIOS()) {
                $ionicPlatform.ready(function() {
                    NativeStorage.remove(key,
                        function (result) {
                            console.log('Removed ' + key + ' ' + result);
                            success(key);
                        },
                        function (error) {
                            console.log('Error ' + error);
                            failed(error);
                        });
                });
            } else {
                $localStorage.remove(key);
                success(key);
            }
        };

        return StoreService;

    }]);

hablaxServices.factory('RatesService', ['$q', 'HablaxConfig', '$http', '$serializer', '$timeout', '$localStorage','StoreService','CommonService','$responseProcess',
function($q, HablaxConfig, $http, $serializer, $timeout, $localStorage, StoreService, CommonService, $responseProcess) {
  var _APIURL = {
    smsCountryList: HablaxConfig.APIbaseURL + "get_sms_country_list",
    get_rates: HablaxConfig.APIbaseURL + "get_rates",
    get_prices: HablaxConfig.APIbaseURL + "get_prices",
    get_all_rates: HablaxConfig.APIbaseURL + "get_all_rates",
    get_nauta: HablaxConfig.APIbaseURL + "get_nauta_recharges"
  };

    function _getCountryList(language,dfd) {
        var data2Send = $serializer.serialize({language:language});
        $http.post(_APIURL.smsCountryList, data2Send).then(function(resp) {
                resp = resp.data;
                if (resp.status == "success" && resp.countries) {
                    _countries = resp.countries;
                    dfd.resolve(resp.countries);
                } else {
                    $responseProcess.processErrorResponse(resp,dfd.reject);
                    _countries = [];
                }
            },
            function(err) {
                dfd.reject(err);
                _countries = [];
            }).catch(function(err) {
            _countries = [];
            dfd.reject(err);
        });
    }

  var _countries = [];
  var _prices = null;


  return {

    getPrices: function() {
        var dfd = $q.defer();
        if (_prices != null) {
            dfd.resolve(_prices);
        } else {

            var data2Send = $serializer.serialize();
            $http.post(_APIURL.get_prices, data2Send).then(function (resp) {
                    resp = resp.data;
                    if (resp.status == "success") {
                        _prices = resp;
                        dfd.resolve(resp);
                    } else {
                        $responseProcess.processErrorResponse(resp, dfd.reject);
                    }
                },
                function (err) {
                    dfd.reject(err);
                }).catch(function (err) {
                dfd.reject(err);
            });
        }
        return dfd.promise;
    },
    getCountryList: function() {
      var dfd = $q.defer();
      if(_countries && _countries.length>0){
        $timeout(function(){
          dfd.resolve(_countries)
        },20);
      }
      else{
          StoreService.get("hablax_lang",
              function (value) {
                  if (value) {
                      _getCountryList(value, dfd);
                  } else {
                      _getCountryList("en", dfd);
                  }
              }, function (error) {
                  _getCountryList("en", dfd);
              });
      }
      return dfd.promise;
    },
    getRates: function() {
      var dfd = $q.defer();
      var data2Send = $serializer.serialize();
      $http.post(_APIURL.get_rates, data2Send).then(function(resp) {
          resp = resp.data;
          if (resp.status == "success") {
            dfd.resolve(resp);
          } else {
              $responseProcess.processErrorResponse(resp,dfd.reject);
          }
        },
        function(err) {
          dfd.reject(err);
        }).catch(function(err) {
        dfd.reject(err);
      });

      return dfd.promise;
    },
      getAllRates: function(number) {
          var dfd = $q.defer();
          var data2Send = $serializer.serialize({number: number});
          $http.post(_APIURL.get_all_rates, data2Send).then(function(resp) {
                  resp = resp.data;
                  if (resp.status == "success") {
                      dfd.resolve(resp);
                  } else {
                      $responseProcess.processErrorResponse(resp,dfd.reject);
                  }
              },
              function(err) {
                  dfd.reject(err);
              }).catch(function(err) {
              dfd.reject(err);
          });

          return dfd.promise;
      },
       getNauta: function() {
          var dfd = $q.defer();
          var data2Send = $serializer.serialize({});
          $http.post(_APIURL.get_nauta, data2Send).then(function(resp) {
            resp = resp.data;
                  if (resp.status == "success") {
                      dfd.resolve(resp);
                  } else {
                      $responseProcess.processErrorResponse(resp,dfd.reject);
                  }
              },
              function(err) {
                  dfd.reject(err);
              }).catch(function(err) {
              dfd.reject(err);
          });

          return dfd.promise;
      }


}

}]);

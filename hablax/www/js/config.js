angular.module('hablax').config(function($logProvider, $compileProvider,$stateProvider, $urlRouterProvider, $ionicConfigProvider, $httpProvider,$translateProvider) {

  // Setting the custom configurations for several stuff here [Ahsan]

  $compileProvider.aHrefSanitizationWhitelist(/^\s*(geo|mailto|tel|maps):/);

  $ionicConfigProvider.tabs.position("bottom");
  ionic.Platform.isFullScreen = true;
  if(ionic && ionic.Platform && ionic.Platform.isIOS()){
    $ionicConfigProvider.scrolling.jsScrolling(true);
  }
  else{
    $ionicConfigProvider.scrolling.jsScrolling(false);
  }
  $logProvider.debugEnabled(true);

  $httpProvider.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
  $ionicConfigProvider.views.swipeBackEnabled(false);

  //settings for translations
  $translateProvider
  .useStaticFilesLoader({
  prefix: 'translations/locale-',
  suffix: '.json'
  })
  .preferredLanguage('en')
  .useMissingTranslationHandlerLog();

  $translateProvider.useSanitizeValueStrategy(null);

  //ngIntlTelInputProvider.set({initialCountry: 'cu'});

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
  // .state('app', {
  //   url: '/',
  //   abstract:true
  // })
    .state('home', {
      url: '/home',
      cache:false,
      views: {
        'appView': {
          templateUrl: 'templates/home.html',
          controller: "HomeController", //Controller names always go PascalCase [Ahsan]
          controllerAs: "homeCtrl" // Controller alias names always go camelCase [Ahsan]
        }
      }
    })
    .state('login', {
      url: '/login',
      cache:false,
      views: {
        'appView': {
          templateUrl: 'templates/login.html',
          controller: "LoginController", //Controller names always go PascalCase [Ahsan]
          controllerAs: "loginCtrl" // Controller alias names always go camelCase [Ahsan]
        }
      }
    })

    .state('forceUpdate', {
      url: '/forceUpdate',
      cache:false,
      views: {
        'appView': {
          templateUrl: 'templates/force-update.html',
          controller: "ForceUpdateController",
          controllerAs: "updateCtrl"
        }
      }
    })

  .state('signup', {
    url: '/signup?token',
    cache:false,
    views: {
      'appView': {
        templateUrl: 'templates/signup/signup.html',
        controller: "SignupController",
        controllerAs: "signupCtrl"
      }
    }
  })

  .state('validate-phone',{
    url: '/validate-phone',
    cache:true,
    views: {
      'appView': {
        templateUrl: 'templates/signup/validate-phone.html',
        controller: "ValidatePhoneController",
        controllerAs: "validatePhoneCtrl"
      }
    }
  })


  .state('forgot-pass', {
    url: '/forgot-pass',
    // parent: 'app',
    views: {
      'appView': {
        templateUrl: 'templates/forgot-pass.html',
        controller: 'ForgotPasswordCotroller',
        controllerAs: 'forgotCtrl'
      }
    }
  })

  .state('how-it-works', {
    url: '/how-it-works',
    cache: false,
    views: {
      'appView': {
        templateUrl: 'templates/how-it-works.html',
        controller: 'HowItWorksController',
        controllerAs: 'howItWorksCtrl'
      }
    }
  })
  .state('terms-and-conditions', {
    url: '/terms-and-conditions',
    parent: 'tab',
    views: {
      'tabsView@tab': {
        templateUrl: 'templates/terms-and-conditions.html',
        controller: 'TermsController',
        controllerAs: 'termsCtrl'
      }
    }
  })
  .state('pay-history', {
      url: '/pay-history',
      parent: 'tab',
      cache: false,
      views: {
        'tabsView@tab': {
          templateUrl: 'templates/pay-history.html',
          controller: 'PayHistoryController',
          controllerAs: 'payhistoryCtrl'
        }
      }
    })
    .state('call-history', {
      url: '/call-history',
      parent: 'tab',
      views: {
        'tabsView@tab': {
          templateUrl: 'templates/call-history.html',
          controller: 'CallHistoryController',
          controllerAs: 'callhistoryCtrl'
        }
      }
    })
    .state('add-funds', {
      url: '/add-funds',
      parent: 'tab',
      cache: false,
      views: {
        'tabsView@tab': {
          templateUrl: 'templates/add-funds.html',
          controller: 'AddFundsController',
          controllerAs: 'addfundsCtrl'
        }
      }
    })
    .state('support', {
      url: '/support',
      parent: 'tab',
      cache: false,
      views: {
        'tabsView@tab': {
          templateUrl: 'templates/support.html'
          ,controller: 'SupportController',
          controllerAs: 'supportCtrl'
        }
      }
    })
      .state('send-email', {
        url: '/send-email',
        parent: 'tab',
        cache: false,
        views: {
          'tabsView@tab': {
            templateUrl: 'templates/send-email.html',
            controller: 'SendEmailController',
            controllerAs: 'sendEmailCtrl'
          }
        }
      })
       

      .state('rescue-phone-contacts', {
        url: '/rescue-phone-contacts',
        cache:false,
        views: {
          'appView': {
            templateUrl: 'templates/rescue-phone-contacts.html',
            controller: "RescuePhoneContactsController", //Controller names always go PascalCase [Ahsan]
            controllerAs: "rescueContactsCtrl" // Controller alias names always go camelCase [Ahsan]
          }
        }
      })
      .state('send-email2', {
        url: '/send-email2',
        cache: false,
        views: {
          'appView': {
        templateUrl: 'templates/send-email2.html',
        controller: 'SendEmail2Controller',
         controllerAs: 'sendEmail2Ctrl'
          }
        }
        
      })

      .state('how-it-works-inside', {
        url: '/how-it-works-inside',
        parent: 'tab',
        cache: false,
        views: {
          'tabsView@tab': {
            templateUrl: 'templates/how-it-works-inside.html'
            ,controller: 'HowItWorksInsideController',
            controllerAs: 'howItWorksInsideCtrl'
          }
        }
      })
      .state('how-it-works-inside-detail', {
        url: '/how-it-works-inside-detail/:objId',
        params:{title:'', description:''},
        parent: 'tab',
        cache: false,
        views: {
          'tabsView@tab': {
            templateUrl: 'templates/how-it-works-inside-detail.html'
            ,controller: 'HowItWorksInsideDetailController',
            controllerAs: 'howItWorksInsideDetailCtrl'
          }
        }
      })
      .state('who-we-are', {
        url: '/who-we-are',
        parent: 'tab',
        cache: false,
        views: {
          'tabsView@tab': {
            templateUrl: 'templates/who-we-are.html'
            ,controller: 'WhoWeAreController',
            controllerAs: 'whoWeAreCtrl'
          }
        }
      })
      .state('who-we-are-detail', {
        url: '/who-we-are-detail',
        params:{title:'', description:''},
        parent: 'tab',
        cache: false,
        views: {
          'tabsView@tab': {
            templateUrl: 'templates/who-we-are-detail.html'
            ,controller: 'WhoWeAreDetailController',
            controllerAs: 'whoWeAreDetailCtrl'
          }
        }
      })
    .state('profile', {
      url: '/profile',
      parent: 'tab',
      views: {
        'tabsView@tab': {
          templateUrl: 'templates/profile.html',
          controller: 'ProfileController',
          controllerAs: 'profileCtrl',
          resolve: {
            userProfile: function($q, $timeout, UserService,$state, $ionicLoading) {
              var dfd = $q.defer();
              $ionicLoading.show();
              UserService.getUserProfile().then(function(userData) {
                $ionicLoading.hide();
                //userData.anet.payment_profiles = [{value:['33523623'], customerPaymentProfileId:['32523']}];
                if (userData) {
                  dfd.resolve(userData);
                } else {
                  $state.go("home");
                }

              }, function() {
                $ionicLoading.hide();
                dfd.reject();
              });

              // $timeout(function(){
              //   dfd.resolve({})
              // },10);
              return dfd.promise;
            }
          }
        }
      }
    })
    .state('rates-redesign',{
      cache:false,
      url: '/rates-redesign',
      parent: 'tab',
      views: {
        'tabsView@tab': {
          templateUrl: 'templates/rates/rates-redesign.html',
          controller: 'RatesController',
          controllerAs: 'ratesCtrl',
          resolve:{
            typeOfRates:function() {
              return "call";
            }
          }
        }
      }
    })
    .state('rates',{
      url: '/rates',
      abstract:true,
      parent: 'tab',
      views: {
        'tabsView@tab': {
          templateUrl: 'templates/rates/rates.html',
          controller: 'RatesController',
          controllerAs: 'ratesCtrl',
          resolve:{
            typeOfRates:function() {
              return "main";
            }
          }
        }
      }
    })
    .state('rates.call-rates',{
      cache:false,
      url: '/call-rates',
      views: {
        'ratesView@rates': {
          templateUrl: 'templates/rates/call-rates.html',
          controller: 'RatesController',
          controllerAs: 'ratesCtrl',
          resolve:{
            typeOfRates:function() {
              return "call";
            }
          }
        }
      }
    })
    .state('rates.mobile-recharge',{
      cache:false,
      url: '/mobile-recharge',
      views: {
        'ratesView@rates': {
          templateUrl: 'templates/rates/mobile-recharge.html',
          controller: 'RatesController',
          controllerAs: 'ratesCtrl',
          resolve:{
            typeOfRates:function() {
              return "recharge";
            }
          }
        }
      }
    })
    .state('rates.sms-rates',{
      cache:false,
      url: '/sms-rates',
      views: {
        'ratesView@rates': {
          templateUrl: 'templates/rates/sms-rates.html',
          controller: 'RatesController',
          controllerAs: 'ratesCtrl',
          resolve:{
            typeOfRates:function() {
              return "sms";
            }
          }
        }
      }
    })
    .state('tab', {
      url: '/tab',
      // cache:false,
      abstract: true,
      views: {
        'appView': {
          templateUrl: 'templates/tabs.html',
          controller: "TabsController",
          controllerAs: "tabsCtrl",
          resolve: {
            isAuthenticated: function($q, $timeout, AuthService, $state, $ionicLoading, $log, StoreService) {
              var dfd = $q.defer();
              $ionicLoading.show();

              if (!AuthService.hablaxAuth || !AuthService.hablaxAuth.token) {
                StoreService.getObject("hablaxAuth",
                    function (value) {
                      var auth = value;
                      if (!auth || !auth.token) {
                        $state.go("home");
                        dfd.reject();
                        $ionicLoading.hide();

                      } else {
                        AuthService.hablaxAuth = auth;
                        AuthService.hablaxToken = auth.token.token;
                        dfd.resolve(auth);
                        $ionicLoading.hide();
                      }
                    }, function (error) {
                      $state.go("home");
                      dfd.reject();
                      $ionicLoading.hide();
                    });

              } else { // this for login bug
                dfd.resolve(AuthService.hablaxAuth);
                $ionicLoading.hide();
              }

              return dfd.promise;
            }
          }
        }
      }
    })

  // Each tab has its own nav history stack:

  .state('tab.dial', {
    url: '/dial',
    cache:false,
    // parent: 'tab',
    views: {
      'tabsView@tab': {
        templateUrl: 'templates/tab-dial.html',
        controller: 'DialController',
        controllerAs: 'dialCtrl'
      }
    },
    params: {
      contact: null
    }
  })

  .state('tab.contacts', {
      url: '/contacts',
      abstract: true,
      views: {
        'tabsView@tab': {
          templateUrl: 'templates/tab-contacts.html',
          // controller: 'ContactsController',
          resolve:{
            type:function() {
              return "main";
            }
          }
        }
      }
    })
    .state('tab.contacts.all', {
      url: '/all',
      cache:false,
      views: {
        'contactsView@tab.contacts': {
          templateUrl: 'templates/contacts-all.html',
          controller: 'ContactsController',
          controllerAs: 'contactsCtrl',
          resolve:{
            type:function() {
              return "all";
            }
          }
        }
      }
    })
    .state('tab.contacts.hablax', {
      url: '/hablax',
      views: {
        'contactsView@tab.contacts': {
          templateUrl: 'templates/contacts-hablax.html',
          controller: 'ContactsController',
          controllerAs: 'contactsCtrl',
          resolve:{
            type:function() {
              return "hablax";
            }
          }
        }
      }
    })
    .state('tab.messages', {
      url: '/messages',
      abstract: true,
      views: {
        'tabsView@tab': {
          templateUrl: 'templates/messages.html',
          controller: 'MessagesController',
          resolve:{
            mode:function() {
              return "both";
            }
          }
        }
      }
    })
    .state('tab.messages.sent', {
      cache:false,
      url: '/sent',
      views: {
        'messagesView@tab.messages': {
          templateUrl: 'templates/messages-sent.html',
          controller: 'MessagesController',
          controllerAs: 'msgsCtrl',
            resolve:{
              mode:function() {
                return "sent";
              }
            }
        }
      }

    })
    .state('tab.messages.write-message', {
      url: '/write-message',
      cache: false,
      views: {
        'messagesView@tab.messages': {
          templateUrl: 'templates/messages-writeMessage.html',
          controller: 'MessagesController',
          controllerAs: 'msgsCtrl',
          resolve:{
            mode:function() {
              return "write";
            }
          }
        }
        },
        params: {
          contact: null
        }
    })
    .state('tab.recharge', {
      url: '/recharge',
      cache: false,
      // abstract:true,
      views: {
        'tabsView@tab': {
          templateUrl: 'templates/recharge/recharge.html',
          controller: 'RechargeController',
          controllerAs: "rechargeCtrl"
        }
      },
      params: {
        contact: null,
        rechargeType: null,
        pushCountryIso: null
      }
    });


  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/dial');
  // $urlRouterProvider.otherwise('/home');
  // $urlRouterProvider.otherwise('/dial');
  

});


// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('hablax', [
  'ionic',
  'hablax.controllers',
  'hablax.services',
  'hablax.directives',
  'ngCordova',
  'ionic.contrib.drawer',
  // 'btford.socket-io',
  // 'credit-cards'
  'pascalprecht.translate',
  'epic-card-payment',
  'country-fullscreen-select',
  'dial-method-select',
  'timer',
  'angularMoment',
  'ngFitText'
  //'ngIntlTelInput'
])

.run(function($ionicPlatform, PushService, $timeout, UserService, $rootScope) {
  $ionicPlatform.ready(function() {

    //if(window.cordova && window.cordova.plugins.backgroundMode) {
    //  var schedule;
    //  cordova.plugins.backgroundMode.on('activate', function onPause() {
    //    if (schedule==null) {
    //      schedule = setInterval(function(){
    //        if (cordova.plugins.backgroundMode.isActive()) {
    //
    //        }
    //        console.log(cordova.plugins.backgroundMode.isActive());
    //      }, 3000);
    //    }
    //
    //  });
    //  cordova.plugins.backgroundMode.on('deactivate', function onaaaPause() {
    //    if (schedule!=null) {
    //      console.log(8888777777);
    //      clearTimeout(schedule);
    //      schedule = null;
    //    }
    //
    //  });
    //  cordova.plugins.backgroundMode.enable();
    //}

    if (ionic.Platform.isAndroid()) {
      document.body.classList.add('platform-android');
    } else if (ionic.Platform.isIOS()) {
      document.body.classList.add('platform-ios');
    } else {
      // nothing
    }

    // Show the accessory bar by default (show this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
      cordova.plugins.Keyboard.disableScroll(true);
    }

    if (ionic.Platform.isAndroid()) {
      ionic.Platform.fullScreen();
    } else if (ionic.Platform.isIOS()) {
      // no need fullscreen for iOS
    } else {
      // nothing
    }

    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      if (ionic.Platform.isAndroid()) {
        //StatusBar.hide();
        StatusBar.show();
      } else if (ionic.Platform.isIOS()) {
        StatusBar.show();
      } else {
        StatusBar.styleDefault();
      }
    }



    if (ionic.Platform.isAndroid()) {
      //AndroidFullScreen.showUnderSystemUI(function() {
      //  //alert(11);
      //}, function() {
      //  //alert(33);
      //});

      //AndroidFullScreen.immersiveMode(function () {
      //  //alert(22);
      //}, function () {
      //  //alert(44);
      //});

      //AndroidFullScreen.showUnderStatusBar(function () {
      //        //alert(22);
      //      }, function () {
      //        //alert(44);
      //      });
      //AndroidFullScreen.showUnderSystemUI(function () {
      //  //alert(22);
      //}, function () {
      //  //alert(44);
      //});


    }

    //if(window.cordova && window.cordova.plugins.Keyboard) {
    //  window.addEventListener('native.keyboardshow', function (e) {
    //    if (ionic.Platform.isAndroid()) {
    //      // nothing
    //    }
    //  });
    //}

    //if( window.plugins && window.plugins.NativeAudio ) {
    //
    //  window.plugins.NativeAudio.preloadSimple('Phone_Busy_Signal',
    //    'assets/Phone_Busy_Signal.mp3',
    //    function(msg){console.info(msg)},
    //    function(msg){ console.error( 'Error: ' + msg ); });
    //
    //  window.plugins.NativeAudio.preloadComplex('ringback',
    //    'assets/ringback.wav',
    //    1, // volume
    //    1, // voices
    //    0, // delay
    //    function(msg) {
    //      console.info(msg);
    //    },
    //    function(msg){ alert( 'Error: ' + msg ); });
    //}


    if (ionic.Platform.isAndroid() || ionic.Platform.isIOS()) {

      //FCMPlugin.getToken(function (token) {
      //  alert(token);
      //});

      //Note that this callback will be fired everytime a new token is generated, including the first time.
      FCMPlugin.onTokenRefresh(function (token) {

        if (token) {
          //alert(token);
          PushService.submitTopic({
            topicName: "Android",
            topicString: "android"
          });

          PushService.submitTopic({
            topicName: "iOS",
            topicString: "ios"
          });

          PushService.submitTopic({
            topicName: "All Platform",
            topicString: "all"
          });
        }


      });

      //Here you define your application behaviour based on the notification data.
      FCMPlugin.onNotification(function (data) {
        if (data.wasTapped) {
          //Notification was received on device tray and tapped by the user.
          //alert(JSON.stringify(data));
          PushService.redirect(data);

        } else {
          //Notification was received in foreground. Maybe the user needs to be notified.
          //alert(JSON.stringify(data));
        }
      });

      //All devices are subscribed automatically to 'all' and 'ios' or 'android' topic respectively.
      //Must match the following regular expression: "[a-zA-Z0-9-_.~%]{1,900}".
      //FCMPlugin.subscribeToTopic('news');
    }

  });
});

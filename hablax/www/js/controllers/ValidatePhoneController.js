/** ValidatePhoneController*/
hablaxControllers.controller('ValidatePhoneController', function($log, $scope,
  $localStorage,RechargeService, $ionicHistory, $ionicLoading, AuthService, $state, $timeout,$ionicModal, $ionicActionSheet, $translate,
  UserService, StoreService, $ionicPopup,moment) {
  var vm = this;
  vm.selectedCountry = "";
  vm.countryList = [];
  vm.phoneNumber = "";
  vm.validationCode = "";
  vm.dummy = false;
  vm.wizard = {
    step1Section: 'validatePhone'
  };
  vm.popup_translations={};
  vm.codeSentPhoneNumber=null;
 vm.params = {
    email:""
  };

  function _doTranslate() {
    $translate(['rescue_info', 'start_chat', 'show_contact_phones', 'Cancel','this_phone_number_is_already_used_for_registration',
        'invalid_code','Information','entered_number_invalid','resend_code','code_by_email','regen_code','customer_support','change_number','rescue2','enter_email','by_email_ok']).then(function (translations) {
      vm.popup_translations=translations;
    }, function (translationIds) {
      vm.popup_translations=translationIds;
    });

    StoreService.get("hablax_lang",
        function (value) {
          if (value) {
            vm.langCode = value;
          } else {
            vm.langCode = "en";
          }
        }, function (error) {
          vm.langCode = "en";
        });
  }
  _doTranslate();

  $scope.$on("languageChanged",function(e,args){
    _doTranslate();
  });



  function _getCountryList(){
    $ionicLoading.show();
    AuthService.getRegistrationCountries().then(function(countries){
      vm.countryList = countries;
      $ionicLoading.hide();
    },function(err){
      $log.debug(err);
      $ionicLoading.hide();
    });
  }

  vm.goBack = function(){
    //$ionicHistory.goBack();
    $state.go('home');
  };

  vm.onCountrySelect = function(){

  };

  //vm.backToValidate = function($event){
  vm.backToValidate = function(){
    $timeout(function(){
      //$event.stopImmediatePropagation();
      vm.wizard.step1Section = 'validatePhone';
      vm.validationCode = "";
    },10);
  };
  $scope.expiro=false;

  vm.finished= function(){
      if($state.current.name=="validate-phone") {
          $timeout(function() {
              console.log('se acabo el tiempo');
              vm.showRescueActions(1);
              $scope.expiro=true;
          });
      }

    
  };
  vm.validatePhoneNumber = function(step,correo){
    if(step==1){
      if(vm.dummy){
          vm.wizard.step1Section = 'enterCode';
          return;
      }
      var params = {
        country_code: vm.selectedCountry.country_code,
        iso: vm.selectedCountry.iso,
        phone: vm.phoneNumber,
        step: 1
      };

      vm.codeSentPhoneNumber = "+" + params.country_code + params.phone;
    }
    else if(step==2){
      if(vm.dummy){
          $state.transitionTo('signup',{token:'blablablabla'});
          return;
      }
      var params = {
        country_code: vm.selectedCountry.country_code,
        phone: vm.phoneNumber,
        step: 2,
        code: vm.validationCode
      }
    }else if(step==3)
    {
      if(correo!=null)
      {
          var params = {
            country_code: vm.selectedCountry.country_code,
            phone: vm.phoneNumber,
            step: 3,
            correo:correo,
            id_code:$scope.id_code
          }
      }else{
          var params = {
              country_code: vm.selectedCountry.country_code,
              phone: vm.phoneNumber,
              step: 3,
              id_code:$scope.id_code
            }
       
      }
    }

    $ionicLoading.show();
    AuthService.validatePhone(params).then(function(res){
      $log.debug(res);
      $ionicLoading.hide();
      if(step==1)
      {
        vm.wizard.step1Section = 'enterCode';
        $scope.date_end = Number(res.date_end) ;
        $scope.id_code = Number(res.id) ;
      }
      else if(step==2)
      {
        $state.transitionTo('signup',{token:res.token});
      }
      else if(step==3)
      {
        $scope.date_end = Number(res.date_end);
        $scope.expiro=false;
      }
    },function(err){
      $log.debug(err);
      $ionicLoading.hide();
      if(err.error_code=="phone_already_used"){
        $ionicPopup.alert({
          title: vm.popup_translations.Information,
          template: vm.popup_translations.this_phone_number_is_already_used_for_registration
        });
      }
      else if(err.error_code=="invalid_validation_code"){
        $ionicPopup.alert({
          title: vm.popup_translations.Information,
          template: vm.popup_translations.invalid_code
        });

      }
      else{
        $ionicPopup.alert({
          title: vm.popup_translations.Information,
          template: vm.popup_translations.entered_number_invalid
        });
      }
    });
  };


$scope.reenviar_por_correo=function(email)
{

  vm.validatePhoneNumber(3,email.text);
  $scope.closeModal();
};

  vm.openHelp = function(){

    UserService.openHelp();
  };
  $ionicModal.fromTemplateUrl('templates/modals/reenvio_por_correo.html', {
                scope: $scope,
                animation: 'slide-in-up'
              }).then(function(modal) {
                $scope.modal = modal;
              });
  $scope.openModal = function() {
                $scope.modal.show();
              };

              $scope.closeModal = function() {
                $scope.modal.hide();
              };

              // Cleanup the modal when we're done with it!
              $scope.$on('$destroy', function() {
                $scope.modal.remove();
              });

              // Execute action on hide modal
              $scope.$on('modal.hidden', function() {
                // Execute action
              });
              
              // Execute action on remove modal
              $scope.$on('modal.removed', function() {
                // Execute action
              });
vm.irasoporte=function(){
   vm.cerrar_envio_por_email();
  $state.go("rescue-phone-contacts");
}
  vm.showRescueActions = function(expired){
        if(expired==1)
        {
        $scope.titulo = vm.popup_translations.rescue_info;
             var actionSheetButtons = [
        { text: '<i class="as-icon call-btn icon ion-ios-refresh"></i>'+ vm.popup_translations.regen_code + ' '},
        { text: '<i class="as-icon call-btn icon ion-paper-airplane"></i> ' + vm.popup_translations.customer_support + ' '},

      ];

        }else if(expired==3){
          
          $scope.titulo = vm.popup_translations.rescue2;
          var actionSheetButtons = [
        { text: '<i class="as-icon call-btn icon ion-paper-airplane"></i> ' + vm.popup_translations.customer_support + ' '},
  
      ];
        }
        else 
        {
          
          $scope.titulo = vm.popup_translations.rescue_info;
           var actionSheetButtons = [
        { text: '<i class="as-icon call-btn icon ion-ios-refresh"></i>'+ vm.popup_translations.resend_code +': ' + vm.phoneNumber+ ''},
        { text: '<i class="as-icon call-btn icon ion-arrow-swap"></i>'+ vm.popup_translations.change_number + ' '},
        { text: '<i class="as-icon call-btn icon ion-email"></i>'+ vm.popup_translations.code_by_email + ' '},
        { text: '<i class="as-icon call-btn icon ion-paper-airplane"></i> ' + vm.popup_translations.customer_support + ' '},
        // { text: '<i class="as-icon message-btn icon ion-ios-telephone-outline"></i> ' + vm.popup_translations.show_contact_phones + '' }
      ];

        }
      
      var hideSheet = $ionicActionSheet.show({
        buttons: actionSheetButtons,
        //  destructiveText: 'Delete',
        titleText: $scope.titulo,
        cancelText: vm.popup_translations.Cancel,
        cancel: function() {
          // add cancel code..
        },
        buttonClicked: function(index) {
          if(!expired)
        {
          if(index==0){
           vm.validatePhoneNumber(3);
          }
           else if(index == 1)
           {                          
            // 
            $ionicHistory.clearCache(['validate-phone']);
            $state.go('home');
            // $state.transitionTo( $state.current, {reload : Math.random()}, { reload: true, inherit: true, notify: true } );
           
            }
         else if(index==2)
         {

      var content = '<div style="position: absolute; top: 3px; right: 7px; font-size:20px" ng-Click="validatePhoneCtrl.cerrar_envio_por_email();"><i class="icon ion-ios-close-outline"></i></div>'+
      "<div class='call-help-content'>" +
        " <p>Email</p>" +
        " <div class='item item-input'>" +
        " <form ng-submit='validatePhoneCtrl.envio_codigo_por_email()'> <input type='text' placeholder='' ng-model='validatePhoneCtrl.params.correo'></div>" +
        "</div>"+
        "<button type='submit' class='button button-full button-balanced' ng-click='validatePhoneCtrl.envio_codigo_por_email()' style='text-transform: uppercase;'>{{'send' | translate}}</button>"+
        "<button ng-click='validatePhoneCtrl.irasoporte()' class='button button-full button-positive' style='text-transform: uppercase;'>{{'contact_us_link' | translate}}</button></form>";
   

      var options = {
        title:vm.popup_translations.code_by_email,
        template:content,
        scope: $scope,
        cssClass: "local-call-popup"
      };
      vm.code_by_email_popup = $ionicPopup.show(options);

          // vm.sendEmailTemplate = "<div class='send-email-container list'>" +
          //   "<div class='input-label'>Email</div>" +
          //   "<div class='item item-input'>" +
          //     "<input type='text' placeholder='' ng-model='validatePhoneCtrl.params.correo'>" +
          //   "</div>" +
          //   // "<div class='input-label'>Message</div>" +
          //   // "<div class='item item-input'>" +
          //   //   "<textarea class='input' placeholder='' ng-model='rescueContactsCtrl.params.message'></textarea>" +
          //   // "</div>" +
          // "</div>";
          //    var options = {
          //       title: vm.popup_translations.code_by_email,
          //       template:vm.sendEmailTemplate,
          //       scope: $scope,
          //       buttons: [
          //         {
          //           text: "Cancel",
          //           onTap: function(e){
          //             return false;
          //           }
          //         },
          //         {
          //           text: '<b>Send</b>',
          //           type: 'button-positive',
          //           onTap: function(e) {
          //             if(!vm.params.correo){
          //               e.preventDefault();
          //               $ionicLoading.notify(vm.popup_translations.enter_email);
          //               return;
          //             }
          //             $ionicLoading.show();
          //            vm.validatePhoneNumber(3,vm.params.correo)
          //             return false;
          //           }
          //         }
          //       ]
          //     };
          //     vm.send_mail = $ionicPopup.show(options);
          // $scope.openModal();
          }
          // else if(index == 3){
          //   vm.openHelp()
          //   //$state.go('login');
          // }
           else if(index == 3){
            $state.go("rescue-phone-contacts");
            //$state.go('login');
          }  
        }
        else if(expired==3)
        {
          if(index == 0){
            $state.go("rescue-phone-contacts");
           // vm.openHelp()
            }
         
        }
        else
        {
          if(index==0)
          {
            vm.validatePhoneNumber(3);
            console.log('reenviar numero');
          }
         else if(index == 1)
         {
            $state.go("rescue-phone-contacts");
           
          }
        }
          
          return true;
        }
      });

  };
 vm.cerrar_envio_por_email=function()
 {
  vm.code_by_email_popup.close();
 }
 vm.envio_codigo_por_email=function()
 {
   if(!vm.params.correo)
   {
     $ionicLoading.notify(vm.popup_translations.enter_email);
   }else
   {
    $ionicLoading.notify(vm.popup_translations.by_email_ok);
    $ionicLoading.show();
    vm.validatePhoneNumber(3,vm.params.correo);
   }
  
   vm.cerrar_envio_por_email();
 }

  vm.moveUpHeight = 0;
  window.addEventListener('native.keyboardshow', function (e) {
    $scope.$apply(function () {
      vm.moveUpHeight = 100;
      console.log(vm.moveUpHeight);
    });
  });
  window.addEventListener('native.keyboardhide', function () {
    $scope.$apply(function () {
      console.log(vm.moveUpHeight);
      vm.moveUpHeight = 0;

    });
  });

  _getCountryList();
});

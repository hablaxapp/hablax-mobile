hablaxControllers.controller('TabsController', ['$location', 'DialogService', '$anchorScroll','$log', '$scope', '$state' , '$ionicHistory',
  'AuthService', 'UserService', '$ionicModal' ,'$localStorage', '$timeout' ,'RechargeService', '$cordovaInAppBrowser', 'CallService',
  '$rootScope','$ionicLoading','ContactsService', '$ionicScrollDelegate', '$translate', 'CommonService', 'StoreService', 'RatesService',
  '$ionicPopup', 'PushService'
  , function($location, DialogService, $anchorScroll,$log, $scope, $state, $ionicHistory, AuthService, UserService, $ionicModal,
             $localStorage, $timeout, RechargeService, $cordovaInAppBrowser, CallService, $rootScope, $ionicLoading,ContactsService,
             $ionicScrollDelegate, $translate, CommonService, StoreService, RatesService, $ionicPopup, PushService) {

    var vm = this;
    vm.hablaxBalance = UserService.balance;
    vm.contact={
      contact_name:"",
      contact_phone:"",
      contact_email:"",
      context: null
    };
    vm.message_contact_name="";
    vm.message_contact_name_show=false;
    var isIOS = ionic.Platform.isIOS();
    // var target = isIOS ?  "_system" : "_self";
    var inAppBrowserTarget = "_system";
    vm.earpieceToggled = false;
    vm.newContact={
    };
    vm.selectedLang = "en";

    vm.currentVersion = "0.0";
    vm.translations = {};


    function _doTranslate() {
      $translate(['Help_Button_Wifi','Help_Button_Local_Content','Help_Button_Wifi_Content','Local Number','Good','Bad','HELP','contact_saved',
          'contact_could_not_be_saved','fill_input_fields','contact_updated','server_disconnected_please_contact_support',
          'registerd_failed_please_contact_support','user_busy','language_switched_to','contact_name_must_be_filled','Information']).then(function (translations) {
        vm.translations=translations;
      }, function (translationIds) {
        vm.translations=translationIds;
      });

      StoreService.get("hablax_lang",
          function (value) {
            if (value) {
              vm.langCode = value;
            } else {
              vm.langCode = "en";
            }
          }, function (error) {
            vm.langCode = "en";
          });
    }
    _doTranslate();

    RatesService.getPrices(); // pre-load


    vm.showForceUpdate = false;
    vm.checkVersion = function() {
      CommonService.appInfo().then(function (response) {

        if (ionic.Platform.isAndroid() || ionic.Platform.isIOS()) {
          cordova.getAppVersion.getVersionNumber().then(function (version) {
            vm.currentVersion = version;
            vm.showForceUpdate = parseFloat(response.app_minimum_version) > parseFloat(vm.currentVersion);
            if (vm.showForceUpdate) {
              $state.go('forceUpdate');
            }
          });
        }

      }, function (error) {
        // nothing
      });
    };

    vm.checkVersion();


    UserService.getCurrentLanguage(function(lang) {
      vm.selectedLang = lang

    });

    vm.showLanguageOptions = function() {
      var alertPopup = $ionicPopup.alert({
        title: '<br/><strong>Choose your language</strong>',
        template:
        '<ion-list>' +
          '<ion-item ng-click="tabsCtrl.selectedLang=\'en\'">English<i ng-if="tabsCtrl.selectedLang==\'en\'" class="icon ion-checkmark-round pull-right"></i></ion-item>' +
          '<ion-item ng-click="tabsCtrl.selectedLang=\'es\'">Español<i ng-if="tabsCtrl.selectedLang==\'es\'" class="icon ion-checkmark-round pull-right"></i></ion-item>' +
        '</ion-list>',
        scope: $scope,
        cssClass:'local-call-popup custom-popup'
      });

      alertPopup.then(function(res) {
        vm.setNewLanguage(vm.selectedLang);
      });
    };

    $rootScope.$on("message_name_show",function(e,args){
      vm.message_contact_name_show = args.show;
    });

    $rootScope.$on("message_name_update",function(e,args){
      vm.message_contact_name = args.show_name;
    });

    vm.setNewLanguage=function(newLanguage) {
      UserService.toggleLanguage(newLanguage, function(lang) {
        var language = lang =='en' ? 'English' : 'Spanish';
        _doTranslate();
        $ionicLoading.notify(vm.translations.language_switched_to + " " + language);
        $scope.$broadcast('languageChanged');
      });
    };

    vm.signalQuality = vm.translations.Good;
    vm.userProfile=null;
    vm.selectedCountry="";
    // navigate to previous page
    vm.goBack = function() {
      $ionicHistory.goBack();
    };

    function _getSignalQuality() {
      if (vm.signalQuality == null) {
        vm.signalQuality = vm.translations.Good;
      }
      return vm.signalQuality;
    }

    vm.isMessagesState = function(){
      return $state.current.name == "tab.messages.write-message" || $state.current.name == "tab.messages.sent";
    };

    function _getCountryList(){
      $ionicLoading.show();
      AuthService.getRegistrationCountries("en").then(function(countries){
        vm.countryList = countries;
        $ionicLoading.hide();
      },function(err){
        $log.debug(err);
        $ionicLoading.hide();
      });
    }
    _getCountryList();
    function _resetAddEditContact(){
      vm.contact.contact_name="";
      vm.contact.contact_phone="";
      vm.contact.contact_email = "";
      vm.contact.context = null;
      vm.selectedCountry="";
    }
    vm.logout = function(){
      if (ionic.Platform.isAndroid() || ionic.Platform.isIOS()) {
        if (!device.isVirtual) {
          FCMPlugin.getToken(function (token) {
            PushService.removeToken(token).then(function(res){
              // nothing
            });
          });

          UserService.getUserProfile().then(function (data) {
            var topicKey = "iso-"+data.iso.toLowerCase();
            FCMPlugin.unsubscribeFromTopic(topicKey);
          });
        }
      }


      AuthService.logout().then(function(res){
        $state.go("home");
      },function(err){
        $state.go("home");
      });

    };
    vm.addNewContact=function() {
      $ionicLoading.show();
      vm.newContact.contact_name=vm.contact.contact_name;
      vm.newContact.contact_country_iso=vm.selectedCountry.iso;
      vm.newContact.country_code=vm.selectedCountry.country_code; // additional field, API wont use this
      vm.newContact.contact_email = vm.contact.contact_email;

      if (vm.newContact.contact_name == null
          || vm.newContact.contact_name == "") {
        $ionicLoading.hide();
        $ionicPopup.alert({
          title: vm.translations.Information,
          template: vm.translations.contact_name_must_be_filled
        });
        return;
      }

      $log.debug(vm.newContact);
      switch (vm.contact.type) {
        case 'add':
          vm.newContact.contact_phone="011-"+vm.selectedCountry.country_code + "-" +vm.contact.contact_phone;
          var _phoneString = "011"+vm.selectedCountry.country_code +vm.contact.contact_phone;
          if(vm.newContact.contact_name!="" && vm.newContact.contact_phone!=undefined && vm.newContact.contact_country_iso!=undefined){
            ContactsService.addContact(vm.newContact).then(function(result) {
              $log.debug(result);
              $ionicLoading.notify(vm.translations.contact_saved);
              vm.newContact.contact_phone = _phoneString;
              $rootScope.$broadcast("contact-event: added",{contact: angular.copy(vm.newContact)});
              _resetAddEditContact();
              vm.contactModal.hide();
            }, function(result) {
              //if something goes Wrong
              $ionicLoading.notify(vm.translations.contact_could_not_be_saved);
              $log.debug(result);
            });
          }
          else{
            $ionicLoading.hide();
            $ionicLoading.notify(vm.translations.fill_input_fields);
          }
          break;
        case 'edit':
          vm.newContact.contact_phone="011-"+vm.selectedCountry.country_code + "-" +vm.contact.contact_phone;
          var _phoneString = "011"+vm.selectedCountry.country_code +vm.contact.contact_phone;
          vm.newContact.row_id=vm.contact.row_id;
          // vm.newContact.country_code=vm.selectedCountry.country_code;
          if(vm.newContact.contact_name!="" && vm.newContact.contact_phone!=undefined && vm.newContact.contact_country_iso!=undefined){
            ContactsService.editContact(vm.newContact).then(function(result) {
              $log.debug(result);
              $ionicLoading.notify(vm.translations.contact_updated);
              vm.newContact.contact_phone = _phoneString;
              $rootScope.$broadcast("contact-event: edited",{contact: angular.copy(vm.newContact)});
              _resetAddEditContact();
              vm.contactModal.hide();
            }, function(result) {
              //if something goes Wrong
              $ionicLoading.notify(vm.translations.contact_could_not_be_saved);
              $log.debug(result);
            });
          }
          else{
            $ionicLoading.notify(vm.translations.contact_could_not_be_saved);
          }
          break;
      }

    };


    // Create the login modal that we will use later
    $ionicModal.fromTemplateUrl('templates/modals/call-modal.html', {
      scope: $scope
    }).then(function(modal) {
      vm.callModal = modal;
    });

    // Add Contact Modal
    $ionicModal.fromTemplateUrl('templates/modals/edit-contact.html', {
      scope: $scope
    }).then(function(modal) {
      vm.contactModal = modal;
    });
    // Triggered in the login modal to close it
    vm.closeCall = function(opts) {
      opts = opts? opts : {};
      if(opts.endCall){
        vm.endCall();
        $timeout(function(){
          vm.callModal.hide();
        },10);
      }
      else{
        vm.callModal.hide();
      }
    };


    vm.openCallModal = function(){
      vm.callModal.show();
    };
    vm.openContactModal = function(){
      vm.contactModal.show();
    };

    vm.goToDialPad = function(){
      vm.closeCall();
      $state.go("tab.dial");
    };

    vm.callNumber = function(number){
      _resetCallModule();
      vm.endCall();
      $timeout(function () {
        CallService.monitorSignal(function(signalVal){
          $log.debug(signalVal);
          // signalVal = signalVal * -1; //pake it positive
          if(signalVal < -50 && signalVal!=1){
            // alert('having a bad signal');
            $timeout(function(){
              vm.signalQuality = vm.translations.Good;
              // vm.ringBack();
            });
          }
          else{
            $timeout(function(){
              vm.signalQuality = vm.translations.Bad;
              // vm.stopRing();
            });
          }
        },5000);
        if(!vm.hablaxCall.dummy){
          vm.callPhone(number);
        }
        else{
          $timeout(function(){$rootScope.$broadcast('timer-start');},2000);
        }
        // $scope.$broadcast('timer-start');
        vm.openCallModal();
        vm.hablaxCall.isCallInProgress = true;
        CallService.activateProximity();
      }, 10);
    };
    vm.addContact=function(contact) {
      vm.openContactModal();
    };

    vm.onCountrySelect = function(countrySelected){
      vm.selectedCountry = countrySelected;
      vm.tappedNum();
      var index = (vm.contact.contact_phone + '').indexOf("011");
      if(index!=null && index==0){
        vm.contact.contact_phone = vm.contact.contact_phone.replace("011","");
        vm.contact.contact_phone = vm.contact.contact_phone.replace(vm.selectedCountry.country_code,"");
      }
    };

    $rootScope.$on("invalid_token",function(e,args){
      $ionicLoading.hide();
      vm.logout();
    });

    $scope.$on("callNumber",function(e,args){
      vm.dialNumber = args.number;
      if(args.contactObj){
        var result = vm.countryList.filter(function( obj ) {
          return obj.iso == args.contactObj.contact_country_iso && obj.country_code == args.contactObj.country_code;
        });
        vm.selectedCountry=result[0];
      }
      if (vm.isSIPReady()) {
        vm.callNumber(vm.dialNumber);
      } else {
        vm.showSipErrorMessage(vm.translations.server_disconnected_please_contact_support);
      }
    });
    $scope.$on("addContact",function(e,args){
      vm.contact.contact_phone = args.number;
      vm.contact.type="add";
      if(args.page){
        vm.contact.context = args.page;
      }
      var countryCode = args.country_code;
      var iso = args.iso;
      if(countryCode){
        for(var i=0,l=vm.countryList.length; i<l ; i++){
          if(vm.countryList[i].country_code == countryCode && vm.countryList[i].iso == iso){
            vm.selectedCountry = vm.countryList[i];
            break;
          }
        }
      }

      vm.contact.contact_phone = CallService.standardNumber(vm.contact.contact_phone);
      vm.contact.contact_phone = vm.contact.contact_phone.replace("011","");
      vm.contact.contact_phone = vm.contact.contact_phone.replace(vm.selectedCountry.country_code,"");

      vm.addContact();
    });
    $scope.$on("addNewContact",function(e,args){
      vm.contact.type="add";
      vm.addContact();

    });
    $scope.$on("editContact",function(e,args){
      vm.contact = angular.copy(args.editContact);
      // vm.contact.contact_name = args.editContact.contact_name;
      // vm.contact.contact_phone=args.editContacteditContact.contact_phone;
      var result = vm.countryList.filter(function( obj ) {
        return obj.iso == args.editContact.contact_country_iso;
      });
      // vm.contact.row_id=args.editContact.row_id;
      vm.selectedCountry=result[0];
      vm.contact.contact_phone = vm.contact.contact_phone.replace("011","");
      vm.contact.contact_phone = vm.contact.contact_phone.replace(vm.selectedCountry.country_code,"");
      vm.contact.type="edit";
      // vm.contact.contact_email = args.editContact.contact_email;
      vm.addContact();
      $log.debug(vm.contact);
    });

    // SIP JS CODE : REVIEW
    var settings = null;
    var phone = null;
    RechargeService.getExchangeRates();

    vm.updateProfile = function(){
      UserService.getUserProfile().then(function(userData) {
        if (userData) {
          vm.userProfile = userData;
        } else {
          $state.go("home");
        }

      }, function() {
        // nothing
      });

    };

    vm.updateProfile();

    vm.updateBalance = function(balance){
      if (balance != null) {
        vm.hablaxBalance = balance;
      } else {
        UserService.getBalance().then(function(res){
          vm.hablaxBalance = UserService.balance;
        });
      }
    };

    function _tryToRegisterPhone(){
      if(!phone){
        vm.registerPhone();
      }
    }

    $scope.$on('$ionicView.enter', function () {
      //_tryToRegisterPhone();
    });

    vm.registerPhone = function () {
      StoreService.getObject("settings",
          function (value) {
            saved_settings = value;
            if (Object.keys(saved_settings).length > 0) {
              settings = saved_settings;
            }
            else{
              settings = {};
              //settings.host = AuthService.hablaxAuth.sip.host;
              //settings.wss = AuthService.hablaxAuth.sip.wss;
              //settings.user = AuthService.hablaxAuth.sip.username;
              //settings.password = AuthService.hablaxAuth.sip.password;

              settings.host = "astpp.hablax.com";
              settings.wss = "wss://astpp.hablax.com:7443";
              settings.user = "thiensip";
              settings.password = "123456";
              settings.realm = "astpp.hablax.com";
            }

            var configuration = {
              traceSip: true,
              uri: settings.user+"@"+settings.host,
              wsServers: [settings.wss],
              authorizationUser: settings.user,
              password: settings.password,
              stunServers: ["stun.l.google.com:19302", "stun1.l.google.com:19302"]
            };


            if (ionic.Platform.isIOS() || ionic.Platform.isAndroid()) {
              var cordova_SIP = cordova.require("com.onsip.cordova.Sipjs");
              var PhoneRTCMediaHandler = cordova.require("com.onsip.cordova.SipjsMediaHandler")(cordova_SIP);
              configuration.mediaHandlerFactory = PhoneRTCMediaHandler;
              configuration.traceSip = true;
              phone = new cordova_SIP.UA(configuration);
            }
            else {
              phone = new SIP.UA(configuration);
            }

            phone.on('connected', function (e) {
              $log.debug('Connected to the server');
              vm.connected = true;
            });

            phone.on('disconnected', function (e) {
              $log.debug('Disconnected from the server');
              if (vm.connected) {
                vm.connected = false;
              }

            });

            phone.on('registered', function (e) {
              $log.debug('Successfully registered with the server.');
              resgisterDisplayed = true;
              $timeout(function() {
                $scope.registered = true;
              });
              $log.debug("DEVICE REGISTERED!!!!!!");
            });

            phone.on('registrationFailed', function (e) {
              $log.debug("DEVICE REGISTER FAILED");
              //vm.showSipErrorMessage(vm.translations.registerd_failed_please_contact_support);
              var options = {
                all : true
              };
              phone.unregister(options);
            });


            phone.start();
          }, function (error) {
            // nothing
          });

    };

    var initiateHandlers = function(){
      var events = ["progress","failed","rejected","terminated","accepted","ended","bye"];
      vm.hablaxCall.session.on("progress", function (response) {
        if (ionic.Platform.isIOS() || ionic.Platform.isAndroid()) {
          if (_getSignalQuality() === vm.translations.Good) {
            vm.ringBack();
          }
        } else {
          //try {
          //  $log.debug("EVENT: progress " + response.reason_phrase);
          //  if (response.status_code === 183 && response.body && vm.hablaxCall.session.hasOffer && !vm.hablaxCall.session.dialog) {
          //    if (!response.hasHeader('require') || response.getHeader('require').indexOf('100rel') === -1) {
          //      vm.hablaxCall.session.mediaHandler.setDescription(response.body).then(function onSuccess () {
          //        vm.hablaxCall.session.status = SIP.Session.C.STATUS_EARLY_MEDIA;
          //        vm.hablaxCall.session.mute();
          //        vm.ringBack();
          //      }, function onFailure (e) {
          //        vm.hablaxCall.session.logger.warn(e);
          //        vm.hablaxCall.session.acceptAndTerminate(response, 488, 'Not Acceptable Here');
          //        vm.hablaxCall.session.failed(response, SIP.C.causes.BAD_MEDIA_DESCRIPTION);
          //      });
          //    }
          //  }
          //}
          //catch (err) {
          //
          //}
        }
      });
      vm.hablaxCall.session.on("failed", function (e) {
        try {
          $log.debug("EVENT: failed " + e);
          vm.stopRing();
          if(e.status_code==480){
            vm.closeCall({endCall:true});
            $ionicLoading.notify("Network Error");
          }
          else if(e.status_code==487){
            vm.closeCall({endCall:true});
            // $ionicLoading.notify("Network Error");
          }
        }
        catch (err) {

        }
      });
      vm.hablaxCall.session.on("rejected", function (response) {
        try {
          $log.debug("EVENT: rejected " + response);
          if(response.status_code==486 || response.reason_phrase=="Busy Here"){
            vm.closeCall({endCall:true});
            vm.ringBusy();
            $ionicLoading.notify(vm.translations.user_busy);
            vm.stopRing();
          }

        }
        catch (err) {

        }
      });
      vm.hablaxCall.session.on("accepted", function (e) {
        try {
          $log.debug("EVENT: accepted: " + e);
          //  $timeout(function(){
          //    if(!vm.earpieceToggled){
          //      vm.turnEarpieceOn();
          //    }
          //  },100);
          //vm.hablaxCall.session.hold();
          $rootScope.$broadcast('timer-start');
          vm.hablaxCall.session.unmute();
          vm.stopRing();
          vm.turnLoudSpeakerAsCurrentState();
        }
        catch (err) {

        }
      });
      vm.hablaxCall.session.on("hold", function (e) {
        try {
          $log.debug("EVENT: accepted: " + e);
          //alert(232);
        }
        catch (err) {

        }
      });

      vm.hablaxCall.session.on("terminated", function (e) {
        try {
          $log.debug("EVENT: terminated : " + e);
          vm.closeCall({endCall:true});
        }
        catch (err) {

        }
      });
      vm.hablaxCall.session.on("bye", function (e) {
        try {
          $log.debug("EVENT: bye : " + e);
//         if (!vm.callEnded) {
//             vm.closeCall({endCall:true});
//         }
        }
        catch (err) {

        }
      });
    };
    vm.isSIPReady = function () {
      return vm.connected && phone.isRegistered();
    };
    vm.showSipErrorMessage = function (message) {
//       $ionicLoading.notify(message, 4000);

      var content = "<div class='call-help-content'>" + message + "</div>";
      DialogService.alert(vm.translations.HELP,content, angular.noop,$scope,'local-call-popup custom-popup popup-body-center','info');

    };
    vm.callPhone = function (phoneNumber) {
      var selfView = document.getElementById('my-video');
      var remoteView = document.getElementById('their-video');
      var audioView = document.getElementById('audioView');
      var constraints = {video: false, audio: true};
      var options = {
        'eventHandlers': {},
        'extraHeaders': [],
        media: {
          constraints: constraints,
          render: {
            remote: {
              video:remoteView
            },
            local: {
              video: selfView
            },
            audio: audioView
          }
        }
      };

      $log.debug('Now Calling: ', phoneNumber);
      try{
        cordova.plugins.diagnostic.requestMicrophoneAuthorization(function(status){
          if(status === cordova.plugins.diagnostic.permissionStatus.GRANTED){
            vm.invite(phoneNumber);
          }
          else{
            // alert("Microphone not authorized");
          }
        }, function(error){
          console.error(error);
        });
      }
      catch(e){
        vm.invite(phoneNumber);
      }

    };

    vm.invite = function(phoneNumber) {
      $log.debug("Microphone use is authorized");
      var mediaOptions = {
        media : {
          constraints: {
            audio: true,
            video: false
          },
          render: {
            local: {
              video: document.getElementById('localVideo')
            },
            remote: {
              video: document.getElementById('remoteVideo')
            }
          }
        }
      };
      vm.hablaxCall.session = phone.invite(phoneNumber, mediaOptions);
      initiateHandlers();
    };

    vm.ringBusy = function() {
      window.plugins.NativeAudio.play('Phone_Busy_Signal', null, null);
    };

    vm.ringBack = function() {
      vm.turnLoudSpeakerAsCurrentState();
      window.plugins.NativeAudio.loop('ringback', null, null);
    };

    vm.stopRing = function() {
      window.plugins.NativeAudio.stop('ringback', null, null);
    };

    vm.turnLoudSpeakerAsCurrentState = function() {
      if(vm.hablaxCall.dummy) return;
      if(vm.hablaxCall.loudSpeaker){
        vm.turnSpeakerOn();
      }
      else{
        vm.turnEarpieceOn();
      }
    };

    vm.toggleMute = function(){
      vm.hablaxCall.mute = !vm.hablaxCall.mute;
      if(vm.hablaxCall.dummy) return;
      if(vm.hablaxCall.mute){
        vm.hablaxCall.session.mute();
      }
      else{
        vm.hablaxCall.session.unmute();
      }
    };

    vm.toggleBluetooth = function(){
      vm.hablaxCall.loudSpeaker = !vm.hablaxCall.loudSpeaker;
      vm.turnLoudSpeakerAsCurrentState();
    };

    vm.turnEarpieceOn = function(){
      try{
        vm.earpieceToggled = true;
        AudioToggle.setAudioMode(AudioToggle.EARPIECE);
      }
      catch(e){
        $log.debug(e);
      }
    };
    vm.turnSpeakerOn = function(){
      try{
        vm.earpieceToggled = true;
        AudioToggle.setAudioMode(AudioToggle.SPEAKER);
      }
      catch(e){
        $log.debug(e);
      }
    };

    vm.openHelp = function(){
      UserService.openHelp();
    };

    $scope.$on('$cordovaInAppBrowser:exit', function(e, event){
      $cordovaInAppBrowser.close();
    });
    $rootScope.$on('updateProfile', function(e, event){
      vm.updateProfile();
    });
    $rootScope.$on('updateBalance', function(e, event){
      vm.updateBalance(event);
    });

    function _resetCallModule(){
      vm.hablaxCall = {
        isCallInProgress: false,
        mute: false,
        loudSpeaker:false,
        session: null,
        dummy: false
      };
      //vm.callEnded = false;
      vm.earpieceToggled = false;
      vm.hablaxCall.session = null;
      $rootScope.$broadcast('timer-reset-timer');
      vm.updateBalance();
    }

    vm.endCall = function () {
      if (vm.hablaxCall.isCallInProgress) {
        CallService.stopMonitoringSignal();
        CallService.deactivateProximity();
        try {
          vm.stopRing();
          $rootScope.$emit('updateBalance');
          //vm.callEnded = true;
          vm.hablaxCall.session.terminate();
        } catch (e) {
          // if(vm.hablaxCall.session){
          //   try{
          //     vm.hablaxCall.session.cencel();
          //   }
          //   catch(er){
          //     vm.hablaxCall.session.close();
          //   }
          // }
          $log.debug(e);
        } finally {
          _resetCallModule();
        }
      }
    };
    vm.closeContactModal = function() {
      _resetAddEditContact();
      vm.contactModal.hide();
    };
    vm.tappedNum=function() {
      // set the location.hash to the id of
      // the element you wish to scroll to.
      // $location.hash('contactNum');
      // console.log('tapped');
      // call $anchorScroll()
      // $anchorScroll();
      $timeout(function(){
        $ionicScrollDelegate.$getByHandle('editContactDelegate').scrollBottom();
      },10);


    };

    vm.showHelpButtonContent= function(){
      var content = "<div class='call-help-content'>" +
        "<div><b><h4>" + vm.translations["Local Number"] + "</h4></b> <p>" + vm.translations.Help_Button_Local_Content + "</p></div>" +
        "<div><b><h4>" + vm.translations.Help_Button_Wifi + "</h4></b> <p>" + vm.translations.Help_Button_Wifi_Content + "</p></div>" +
        "</div>";
      DialogService.alert(vm.translations.HELP,content,angular.noop,$scope,'local-call-popup custom-popup popup-body-center','info');
    };

    vm.updateBalance();
    _resetCallModule();
  }]);

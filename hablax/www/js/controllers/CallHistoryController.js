hablaxControllers.controller('CallHistoryController',['$translate','$log','$scope','$ionicPlatform','$ionicHistory','$ionicLoading','CallService', 'AuthService' , '$ionicPopup','$localStorage','UserService','$ionicListDelegate','NewsService','StoreService',
function($translate, $log, $scope,$ionicPlatform,$ionicHistory,$ionicLoading,CallService,AuthService,$ionicPopup,$localStorage,UserService, $ionicListDelegate, NewsService, StoreService){

  var callhistoryCtrl = this;

  callhistoryCtrl.records = [];
  callhistoryCtrl.callIndex = 0;
  callhistoryCtrl.allDataLoaded = false;

  callhistoryCtrl.hideNotify = true;
  callhistoryCtrl.notifyMessage = null;

  callhistoryCtrl.goBack = function(){
    $ionicHistory.goBack();
  };

  callhistoryCtrl.translations={};

  function _translate() {
    $translate(['message_sent_successfully', 'confirm_delete', 'confirm_delete_record', 'sure_to_delete','Information', 'record_deleted', 'deleted_all_record',
          'confirm_delete','decir_si','decir_no']).then(function (translations) {
      callhistoryCtrl.translations=translations;
    }, function (translationIds) {
      callhistoryCtrl.translations=translationIds;
    });
  }
  _translate();
  $scope.$on("languageChanged",function(e,args){
    _translate();
  });

  callhistoryCtrl.loadMore = function(){
    CallService.history(callhistoryCtrl.callIndex).then(function(data){
      if(data.status == "success" && data.message_code != "no_results_found") {
        callhistoryCtrl.records = callhistoryCtrl.records.concat(data.calls);
        $log.debug(callhistoryCtrl.records);
        if(data.calls.length<data.count){
          callhistoryCtrl.allDataLoaded = true;
        }
        callhistoryCtrl.callIndex++;

      }
      else{
        callhistoryCtrl.allDataLoaded = true;
      }
      $scope.$broadcast('scroll.infiniteScrollComplete');
      $scope.$broadcast('scroll.refreshComplete');
    },function(data){
      $scope.$broadcast('scroll.infiniteScrollComplete');
      $scope.$broadcast('scroll.refreshComplete');
      console.error(data);
    });
  };

  function _getNews(lang, news) {
    if (lang == "en") {
      callhistoryCtrl.notifyMessage = news.notice.notice.en;
    } else if (lang == "es") {
      callhistoryCtrl.notifyMessage = news.notice.notice.es;
    }
  }

  NewsService.getNews('calls_log').then(function (news) {
    $log.debug(news);
    if (news.status != 'error') {
      callhistoryCtrl.hideNotify = false;
      StoreService.get("hablax_lang",
          function (value) {
            if (value) {
              var language=value;
              _getNews(language, news);
            } else {
              _getNews("en", news);
            }
          }, function (error) {
            _getNews("en", news);
          });

    }
  }, function (resp) {
    //$ionicLoading.notify('Failed to get news');
  });

  callhistoryCtrl.refreshList = function(item) {
    callhistoryCtrl.records = [];
    callhistoryCtrl.callIndex = 0;
    callhistoryCtrl.allDataLoaded = false;
    callhistoryCtrl.loadMore();
  };

  callhistoryCtrl.deleteRecord = function(item) {
    $log.debug(item);
    $ionicListDelegate.closeOptionButtons();
    callhistoryCtrl.showConfirm(callhistoryCtrl.translations.confirm_delete,callhistoryCtrl.translations.confirm_delete_record,function(){
      $ionicLoading.show();
      CallService.delete(item.row_id).then(function(data){
        if(data.status == "success") {
          callhistoryCtrl.records.splice(callhistoryCtrl.records.indexOf(item),1);
          // alert("Deleted Record at: "+ index);
        }
        else {
          console.error("Something is fishy.")
        }
        $ionicLoading.notify(callhistoryCtrl.translations.record_deleted);
      },function(err){
        $ionicLoading.notify("Could not delete record");
        console.error(err);
      });
    },function(){

    });
  };

  callhistoryCtrl.deleteAll = function() {
    callhistoryCtrl.showConfirm(callhistoryCtrl.translations.confirm_delete,callhistoryCtrl.translations.you_sure_delete_all_records,function(){
      $ionicLoading.show();
      CallService.deleteAll().then(function(data){
        if(data.status == "success") {
          callhistoryCtrl.records = [];
            $ionicLoading.notify(callhistoryCtrl.translations.deleted_all_record);
        }
        else {
          console.error("Something is fishy.")
        }
        $ionicLoading.hide();
      },function(err){
        console.error(err);
        $ionicLoading.hide();
      });
    },function(){

    });

  };

  callhistoryCtrl.showConfirm = function(title,msg,onSuccess,onFailure) {
   var confirmPopup = $ionicPopup.confirm({
     title: title,
     template: msg,
     okText:callhistoryCtrl.translations.decir_si,
     cancelText:callhistoryCtrl.translations.decir_no
   });

   confirmPopup.then(function(res) {
     if(res) {
       $log.debug('You are sure');
       if(onSuccess){
         onSuccess();
       }
     } else {
       $log.debug('You are not sure');
       if(onFailure){
         onFailure();
       }
     }
   });
  };

  callhistoryCtrl.more = function(index) {
    $log.debug('you clicked more button on : ' + callhistoryCtrl.records[index].row_id);
  };

  // $scope.$on('$stateChangeSuccess', function() {
  //   callhistoryCtrl.loadMore();
  // });

}]);

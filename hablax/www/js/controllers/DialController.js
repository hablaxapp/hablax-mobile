
hablaxControllers.controller('DialController', ['$log', '$scope', '$rootScope', '$timeout',
    '$translate', 'UserService', 'MessagesService', 'RechargeService',
    '$stateParams',   '$ionicLoading', 'CallService', 'DialogService','$ionicPopup', DialController]);

function DialController($log, $scope, $rootScope, $timeout, $translate, UserService,
                        MessagesService, RechargeService,$stateParams,
                        $ionicLoading, CallService, DialogService,$ionicPopup){

    var vm = this;

    vm.selectedAccessNumber = "";
    vm.dialNumber = "";
    // vm.dialNumber = "01146852500503";
    vm.countryList = [];
    vm.userProfile = UserService.userData;
    vm.rates = {};
    vm.translations = {};
    vm.dialCountry = null;
    vm.choices = {
        show: false
    };
    vm.notShowAgain = false;

    vm.showChoices = function(){
        if(vm.dialNumber!=""){
            vm.choices.show = true;
        }
    };
    vm.hideChoices = function(){
        vm.choices.show = false;
    };

    function _translate() {
        $translate(['HELP', 'Local Number', 'Help_Button_Wifi',  'recharge_popup_subtitle',
                    'Help_Button_Local_Content','Help_Button_Wifi_Content', "not_available",
                    "local_call_not_available", "dial_how_to_enter", "no_thanks",
                    "request_option","request_has_been_sent","llamada_local","not_show_again"]).then(function (translations) {
            vm.translations=translations;
        }, function (translationIds) {
            vm.translations=translationIds;
        });
    }

    _translate();

    $scope.$on("languageChanged",function(e,args){
        setTimeout(function(){ _translate(); }, 300); // apply new language delay, so timeout to resolve this

    });

    vm.howToEnterPopup = function() {
        //var alertPopup = $ionicPopup.alert({
        //    title: vm.translations.Information,
        //    template: '<i class="icon ion-information positive text-center" style="display:block;font-size: 40px;margin-bottom: 15px;"></i>' +
        //    '<p class="text-center">{{"dial_how_to_enter" | translate}}</p>'
        //});

        vm.showDialog(vm.translations.dial_how_to_enter);
    };

    vm.showDialog = function (message) {
        var content = "<div class='call-help-content'>" + message + "</div>";
        // DialogService.alert(vm.translations.HELP,content, angular.noop,$scope,'local-call-popup');
        DialogService.alert(vm.translations.HELP, content, angular.noop,$scope,'local-call-popup custom-popup','info');
    };

    vm.onDigitPress = function(numberPressed){
        if(numberPressed==='+'){
            if(vm.dialNumber.length>0) return;
        }
        if(vm.dialNumber.length<=20){
            vm.dialNumber += numberPressed.toString();
        }

        if(vm.dialNumber.length>=9){
            CallService.verifyNumberForCall(vm.dialNumber).then(function(res){
                $log.debug(res);
                vm.dialCountry = res;
                vm.userProfile = UserService.userData;
                vm.selectedCountry = null;
                for(var i=0,l=vm.countryList.length; i<l ; i++){
                    if(vm.dialCountry.country_code && vm.countryList[i].country_code == vm.dialCountry.country_code && vm.countryList[i].iso == vm.dialCountry.iso){
                        vm.selectedCountry = vm.countryList[i];
                        break;
                    }
                }
            },function(err){
                $log.debug(err);
                vm.dialCountry = null;
                vm.selectedCountry = null;
            })
        }
        else{
            vm.dialCountry = null;
            vm.selectedCountry = null;
        }
    };

    var _accessNumberString = "<div class='item item-select'>" +
        "<select class='full-width' ng-model='dialCtrl.selectedAccessNumber' ng-options='number.text for number in dialCtrl.accessNumbers'>" +
        "<option value=''>-Choose-</option>"
    "</select>"
    "</div>";

    vm.showAlert = function() {
        vm.notShowAgain = CallService.nowShowAgain;
        if (vm.notShowAgain) {
            vm.localCall();
            return;
        }
       var alertPopup = $ionicPopup.alert({
         title: '<div class="info-icon-popup"><i class="icon ion-ios-information-outline"></i></div>',
         template: '<div>' +
                        vm.translations.llamada_local +'<br/>' +
                        '<input type="checkbox" class="not-again" ng-model="dialCtrl.notShowAgain" ng-checked="dialCtrl.notShowAgain" ng-true-value="true" ng-false-value="false"/>'+
                        '<span ng-click="dialCtrl.notShowAgain = !dialCtrl.notShowAgain">' + vm.translations.not_show_again + '</span><br>' +
                   '</div>',
         scope: $scope,
         cssClass:'local-call-popup custom-popup'
       });

       alertPopup.then(function(res) {
         CallService.nowShowAgain = vm.notShowAgain;
         vm.localCall();
       });
     };

    vm.localCall = function(){
        $ionicLoading.show();
        var user = UserService.userData;
        var standardNumber = CallService.standardNumber(vm.dialNumber);
        CallService.getAccessNumbers(standardNumber).then(function(res){
            $ionicLoading.hide();
            vm.accessNumbers = res;
            var num = null;
            for(var i=0,l=vm.accessNumbers.length; i<l; i++){
                if(vm.accessNumbers[i].country_code == user.country_code && vm.accessNumbers[i].iso == user.iso){
                    num = vm.accessNumbers[i];
                    break;
                }
            }

            if(!num){
                DialogService.requestOption({
                    saveText: vm.translations.request_option,
                    cancelText: vm.translations.no_thanks,
                    template: vm.translations.local_call_not_available,
                    title: '<div class="info-icon-popup"><i class="icon ion-ios-information-outline"></i></div>', //vm.translations.not_available,
                    cssClass: 'local-call-popup custom-popup',
                    onSaveCb: function(){
                        $ionicLoading.notify(vm.translations.request_has_been_sent);
                    },
                    onCancelCb: function(){
                        $log.debug('no');
                    },
                    $scope: $scope
                });
            }
            else{
                CallService.saveCallOnServer(standardNumber,function(q) {
                    q.then(function(res){
                        $ionicLoading.hide();
                        window.plugins.CallNumber.callNumber(vm.onSuccess, vm.onError, num.phone, false);
                    },function(err){
                        $ionicLoading.hide();
                    });
                });
            }

        },function(err){
            $ionicLoading.hide();
        });
    };

    var schedule=null;
    vm.onSuccess = function(result){
        if (schedule==null) {
            schedule = setInterval(function(){
                console.log(UserService.balanceUpdated);
                if (UserService.balanceUpdated) {
                    UserService.balanceUpdated = false;
                    if (schedule!=null) {
                        clearTimeout(schedule);
                        schedule = null;
                    }
                } else {
                    $rootScope.$emit('updateBalance');
                }

            }, 3000);
        }
    };

    vm.onError = function(result) {
        console.log("Error:"+result);
    };

    vm.onErasePress = function(numberPressed){
        vm.dialNumber = vm.dialNumber.substr(0,vm.dialNumber.length-1);
        if(vm.dialNumber.length<9){
            vm.dialCountry = null;
            vm.selectedCountry = null;
        }
    };

    vm.onEraseHold = function(){
        vm.dialNumber = "";
        vm.dialCountry = null;
        vm.selectedCountry = null;
    };

    vm.hideKeyboard = function(){
        try{
            if(cordova){
                $timeout(function(){
                    cordova.plugins.Keyboard.close();
                },10);
            }
        }
        catch(e){
            $log.debug(e);
        }
    };

    RechargeService.getExchangeRates().then(function(resp){
        vm.rates = resp;
    });

    MessagesService.getCountryList().then(function(countries) {
        vm.countryList=countries;
        // vm.selectedCountry=vm.countryList[0];
        $log.debug(vm.countryList);
    }, function(resp) {
        //  alert('Failed to get sms Country List');
    });

    vm.makeCall = function(){
        if(vm.dialNumber!=""){
            vm.choices.show = false;
            if(!vm.contact){
                $rootScope.$broadcast("callNumber",{number:vm.dialNumber});
            }
            else{
                $rootScope.$broadcast("callNumber",{number:vm.dialNumber, contactObj: vm.contact});
            }
        }
    };
    vm.addContact = function(){
        if(vm.dialNumber!=""){
            var args = {number:vm.dialNumber,'page':'dialPage'};
            if(vm.dialCountry && vm.dialCountry.country_code){
                args.country_code = vm.dialCountry.country_code;
                args.iso = vm.dialCountry.iso;
            }
            $rootScope.$broadcast("addContact",args);
        }
    };

    if($stateParams.contact){
        vm.dialNumber=$stateParams.contact.contact_phone;
        vm.contact = $stateParams.contact;
        vm.makeCall();
    }
}

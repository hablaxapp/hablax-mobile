hablaxControllers.controller('SignupController', function($log, $scope, AuthService, $ionicPopup,
  $state, $stateParams, $ionicLoading, UserService,$localStorage, $ionicHistory, $translate,StoreService, $ionicPlatform) {
  // Login view's bindings go here.

  $log.debug("inside signup controller");

  var signupCtrl = this;

  signupCtrl.popup_translations={};
  // signupCtrl.countryList=[];
  signupCtrl.selectedCountry=null;
  signupCtrl.phoneNumber='';
  function _translate() {
    $translate(['email_already_registered']).then(function (translations) {
      $log.debug(translations,translations.recharge_popup_subtitle);
      signupCtrl.popup_translations=translations;
    }, function (translationIds) {
      signupCtrl.popup_translations=translationIds;
    });

      StoreService.get("hablax_lang",
              function (value) {
                if (value) {
                  var language=value;
                  self.langCode = value;
                } else {
                  self.langCode = "en";
                }
              }, function (error) {
                self.langCode = "en";
              });
  }
  _translate();

  $scope.$on('languageChanged',function(){
    _translate();
  });


  $ionicPlatform.onHardwareBackButton(function(event) {
    if($state.current.name=="signup") {
      $ionicHistory.clearCache();
      $ionicHistory.clearHistory();
      $state.go('home');

    }

  });

  $log.debug($stateParams);
  signupCtrl.selector = "";
  signupCtrl.User = {
    first_name: '',
    last_name: '',
    email: '',
    password: '',
    referer: '',
    currency: '',
    token: $stateParams.token,
    like_iso_code_country: ''
  };

  signupCtrl.register = function() {
    $log.debug(signupCtrl.User);
    signupCtrl.User.like_iso_code_country = signupCtrl.selectedCountry.iso;
   
    $ionicLoading.show();
    AuthService.signup(signupCtrl.User).then(function(data) {
      $ionicLoading.hide();
      $log.debug(data);
      $ionicHistory.nextViewOptions({
        disableAnimate: true,
        disableBack: true
      });
      $state.go('tab.dial');

    }, function(data) {
      $ionicLoading.hide();
      if(data && data.errors){
        if(data.errors[0]=="email_already_registered"){
          signupCtrl.showAlert('Error',signupCtrl.popup_translations.email_already_registered,angular.noop);
        }
      }
      console.error(data);
    });
  };

  signupCtrl.showAlert = function(title,template,onOkCb) {
   var alertPopup = $ionicPopup.alert({
     title: title,
     template: template
   });

   alertPopup.then(function(res) {
     $log.debug('Thank you for not eating my delicious ice cream cone');
     if(onOkCb){
       onOkCb();
     }
   });
  };

});

/** HomeController*/
hablaxControllers.controller('HomeController', function($scope,$state,$rootScope,$log,$localStorage,PaypalService,
                                UserService, AuthService, CommonService, DialogService,$translate, StoreService,
                                $document, $animate) {
    // Login view's bindings go here.
    $log.debug("inside home controller");
    var self=this;
    self.selectedLang="";

    self.popup_translations={};
    function _translate() {
        $translate(['update_now', 'must_update_latest', 'HELP']).then(function (translations) {
            self.popup_translations=translations;
            console.log(self.popup_translations);
        }, function (translationIds) {
            self.popup_translations=translationIds;
            console.log(self.popup_translations);
        });
    }
    _translate();

    $scope.$on("languageChanged",function(langCode,args){
        _translate();
        self.selectedLang = langCode;
    });

    self.showForceUpdate = false;
    self.checkVersion = function() {
        CommonService.appInfo().then(function (response) {

            if (ionic.Platform.isAndroid() || ionic.Platform.isIOS()) {
                cordova.getAppVersion.getVersionNumber().then(function (version) {
                    currentVersion = version;
                    self.showForceUpdate = parseFloat(response.app_minimum_version) > parseFloat(currentVersion);
                    if (self.showForceUpdate) {
                        $state.go('forceUpdate');
                    }
                });
            }

        }, function (error) {
            // nothing
        });
    };


    self.checkVersion();
    ionic.Platform.ready(function() {
     if (ionic.Platform.isAndroid() || ionic.Platform.isIOS()) {
        navigator.globalization.getPreferredLanguage(
            function (language) {
                var values = language.value.split("-");
                if (values.length > 0) {
                    var languageCode = values[0];
                    self.selectedLang = languageCode;
                    UserService.setLanguage(languageCode);
                }

            },
            function () {
                UserService.setLanguage("en");
            }
        );
    }
  });
   

    function _useSystemLanguage() {
         ionic.Platform.ready(function() {
        if (ionic.Platform.isAndroid() || ionic.Platform.isIOS()) {
            navigator.globalization.getPreferredLanguage(
                function (language) {
                    var values = language.value.split("-");
                    if (values.length > 0) {
                        var languageCode = values[0];
                        UserService.setLanguage(languageCode);
                        self.selectedLang=languageCode;
                    }

                },
                function () {
                    UserService.setLanguage("en");
                    self.selectedLang="en";
                }
            );
        } else {
            UserService.setLanguage("en");
            self.selectedLang="en";
        }
        });
    }
    _useSystemLanguage();


    self.welcomeText = [
        "Welcome!", "¡Bienvenido!", "Bienvenue!", "Bem vinda!", "Byenveni!"
    ];
    self.indexText = 0;


    self.afterShow = function() {
        self.show = false;
    };

    self.afterHide = function() {
        self.indexText++;
        if (self.indexText >= self.welcomeText.length) {
            self.indexText = 0;
        }
        self.show = true;
    };


    self.goLogin=function() {
        StoreService.set("goLogin",true);

        StoreService.get("hablaxIntroSeen",
            function (value) {
                if (value) {
                    $state.go('login');
                } else {
                    $state.go('how-it-works');
                }
            }, function (error) {
                $state.go('how-it-works');
            });
    };

    self.goValidatePhone=function() {
        StoreService.set("goLogin",false);

        StoreService.get("hablaxIntroSeen",
            function (value) {
                if (value) {
                    $state.go('validate-phone');
                } else {
                    $state.go('how-it-works');
                }
            }, function (error) {
                $state.go('how-it-works');
            });

    };

    self.moveUpHeight = 0;
    window.addEventListener('native.keyboardshow', function (e) {
        $scope.$apply(function () {
            self.moveUpHeight = 100;
            console.log(self.moveUpHeight);
        });
    });
    window.addEventListener('native.keyboardhide', function () {
        $scope.$apply(function () {
            console.log(self.moveUpHeight);
            self.moveUpHeight = 0;

        });
    });

});

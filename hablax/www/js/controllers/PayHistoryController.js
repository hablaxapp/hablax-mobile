hablaxControllers.controller('PayHistoryController',['$translate','$log','$scope','$ionicPlatform','$ionicHistory','$ionicLoading','PaymentService', 'UserService' , '$ionicPopup','$localStorage','NewsService',
function($translate,$log, $scope,$ionicPlatform,$ionicHistory,$ionicLoading,PaymentService,UserService,$ionicPopup,$localStorage,NewsService){

  var payhistoryCtrl = this;

  payhistoryCtrl.records = [];
  payhistoryCtrl.callIndex = 0;
  payhistoryCtrl.allDataLoaded = false;

  payhistoryCtrl.hideNotify = true;
  payhistoryCtrl.notifyMessage = null;

  payhistoryCtrl.translations={};

  function _translate() {
    $translate(['deleted_all_record','transaction_deleted','transaction_could_not_be_deleted','confirm_delete','you_sure_delete_all_records','confirm_delete_record','decir_si','decir_no']).then(function (translations) {
      payhistoryCtrl.translations=translations;
    }, function (translationIds) {
      payhistoryCtrl.translations=translationIds;
    });
  }
  _translate();
  $scope.$on("languageChanged",function(e,args){ _translate();
  });


  payhistoryCtrl.loadMore= function(){
    if(payhistoryCtrl.callIndex==0){
      // $ionicLoading.show();
    }
    PaymentService.history(payhistoryCtrl.callIndex).then(function(data){
      if(data.status == "success" && data.message_code != "no_results_found") {
        payhistoryCtrl.records = payhistoryCtrl.records.concat(data.payments);
        $log.debug(payhistoryCtrl.records);
        if(data.payments.length<data.count){
          payhistoryCtrl.allDataLoaded = true;
        }
        payhistoryCtrl.callIndex++;

      } else {
        payhistoryCtrl.allDataLoaded = true;
      }

      $scope.$broadcast('scroll.infiniteScrollComplete');
      $scope.$broadcast('scroll.refreshComplete');
    },function(data){
      $scope.$broadcast('scroll.infiniteScrollComplete');
      $scope.$broadcast('scroll.refreshComplete');
      console.error(data);
    });
  };

  function _getNews(lang, news) {
    if (lang == "en") {
      payhistoryCtrl.notifyMessage = news.notice.notice.en;
    } else if (lang == "es") {
      payhistoryCtrl.notifyMessage = news.notice.notice.es;
    }
  }

  NewsService.getNews('payments_log').then(function (news) {
    $log.debug(news);
    if (news.status != 'error') {
      payhistoryCtrl.hideNotify = false;
      StoreService.get("hablax_lang",
          function (value) {
            if (value) {
              var language=value;
              _getNews(language, news);
            } else {
              _getNews("en", news);
            }
          }, function (error) {
            _getNews("en", news);
          });

    }
  }, function (resp) {
    //$ionicLoading.notify('Failed to get news');
  });

  payhistoryCtrl.refreshList = function() {
    payhistoryCtrl.records = [];
    payhistoryCtrl.callIndex = 0;
    payhistoryCtrl.allDataLoaded = false;
    payhistoryCtrl.loadMore();
  };

  payhistoryCtrl.goBack = function(){
    $ionicHistory.goBack();
  };

  payhistoryCtrl.deleteRecord = function(index) {
    payhistoryCtrl.showConfirm(payhistoryCtrl.translations.confirm_delete,payhistoryCtrl.translations.confirm_delete_record,function(){
      $ionicLoading.show();
      PaymentService.delete(payhistoryCtrl.records[index].row_id).then(function(data){
        $log.debug(data);
        if(data.status == "success") {
          payhistoryCtrl.records.splice(index,1);
          // alert("Deleted Record at: " + index);
        }
        else {
          console.error("Something is fishy.")
        }
        $ionicLoading.notify(payhistoryCtrl.translations.transaction_deleted);
      },function(err){
        $ionicLoading.notify(payhistoryCtrl.translations.transaction_could_not_be_deleted);
        console.error(err);
      });
    },function(){

    });
  }

  payhistoryCtrl.deleteAll = function() {
    payhistoryCtrl.showConfirm(payhistoryCtrl.translations.confirm_delete,payhistoryCtrl.translations.you_sure_delete_all_records,function(){
      $ionicLoading.show();
      PaymentService.deleteAll().then(function(data){
        if(data.status == "success") {
          payhistoryCtrl.records = [];
          $ionicLoading.notify(payhistoryCtrl.translations.deleted_all_record);
        }
        else {
          console.error("Something is fishy.")
        }
      },function(err){
        console.error(err);
      });
    },function(){

    });

  }
  payhistoryCtrl.showConfirm = function(title,msg,onSuccess,onFailure) {
   var confirmPopup = $ionicPopup.confirm({
     title: title,
     template: msg,
     okText:payhistoryCtrl.translations.decir_si,
     cancelText:payhistoryCtrl.translations.decir_no
   });

   confirmPopup.then(function(res) {
     if(res) {
       $log.debug('You are sure');
       if(onSuccess){
         onSuccess();
       }
     } else {
       $log.debug('You are not sure');
       if(onFailure){
         onFailure();
       }
     }
   });
  };

  payhistoryCtrl.more = function(index) {
    $log.debug('you clicked more button on : ' + payhistoryCtrl.records[index].row_id);
  }

}]);


hablaxControllers.controller('RechargeController',
function($log, $scope, RechargeService, $ionicLoading, UserService, $ionicHistory, $ionicPopup,$cordovaInAppBrowser,
       $ionicScrollDelegate,$rootScope,$translate,$ionicModal, $localStorage, $state, PaypalService, DialogService, $stateParams,
       ContactsService, NewsService, StoreService) {

  $scope.settings = {
    enableFriends: true
  };

  var vm =  this;
  vm.wizard = {
    step: 1
  };
  vm.userProfile = UserService.userData;
  // $log.debug(vm.selectedCountry);
  vm.cart = {
    items: [],
    subTotal:0.00,
    totalToPay: 0.00,
    reset: function(){
      this.items.length = 0.00;
      this.subTotal = 0.00;
      this.totalToPay = 0.00;
    }
  };
  vm.cardMonths = [{"month":1},{"month":2},{"month":3},{"month":4},{"month":5},{"month":6},{"month":7},{"month":8},{"month":9},{"month":10},{"month":11},{"month":12}];
  vm.cardYears = [{"year":2016},{"year":2017},{"year":2018},{"year":2019},{"year":2020},{"year":2021},{"year":2022},{"year":2023},{"year":2024},{"year":2025},{"year":2026},{"year":2027},{"year":2028},{"year":2029},{"year":2030}];
  vm.card = {
    show: false,
    number: "",
    // month: "",
    // year: "",
    cvc: "",
    remember:true,
    postalCode: "",
    reset: function(hide){
      if(hide){
        this.show = false;
      }
      this.month = "";
      this.year = "";
      this.number = "";
      this.postalCode = "";
      this.cvc = "";
    }
  };

  vm.paypalReady ={
    show: false
  };
  vm.hideNotify = true;
  vm.notifyMessage = null;

  vm.countryList = [];

  //StoreService.getObject("rechargeCountry",
  //    function (value) {
  //      vm.selectedCountry = value;
  //    }, function (error) {
  //      // nothing
  //    });

  vm.countryServices = null;
  vm.selectedService = null;
  vm.rechargeValidation = null;
  vm.selectedTopup = null;
  // vm.rechargeNumber = 3221385168;
  vm.rechargeNumber = "";
  $ionicLoading.show();
  vm.popup_translations={};

  function _translate() {
    $translate(['recharge_popup_title', 'recharge_popup_subtitle', 'add_more','pay_now', 'Tax_exempt','confirm_delete_recharge_1','confirm_delete_recharge',
          'ok','cancel','payment_successful','what_to_do','add_another_recharge','close','could_not_validate_number','this_country_not_available_for_recharging',
        'please_select_topup_amount','please_enter_valid_email', 'error', 'cent', 'phone_number_must_not_empty','invalid_nauta_email',
          'please_enter_number']).then(function (translations) {
      $log.debug(translations,translations.recharge_popup_subtitle);
      vm.popup_translations=translations;
    }, function (translationIds) {
      vm.popup_translations=translationIds;
    });

    StoreService.get("hablax_lang",
        function (value) {
          if (value) {
            var language=value;
            vm.langCode = value;
          } else {
            vm.langCode = "en";
        }
        }, function (error) {
          vm.langCode = "en";
        });
  }
  _translate();

  $scope.$on('languageChanged',function(){
    _translate();
  });


  function _getNews(lang, news) {
    if (lang == "en") {
      vm.notifyMessage = news.notice.notice.en;
    } else if (lang == "es") {
      vm.notifyMessage = news.notice.notice.es;
    }
  }

  NewsService.getNews('recharges').then(function (news) {
    $log.debug(news);
    if (news.status != 'error') {
      vm.hideNotify = false;
      StoreService.get("hablax_lang",
          function (value) {
            if (value) {
              var language=value;
              _getNews(language, news);
            } else {
              _getNews("en", news);
            }
          }, function (error) {
            _getNews("en", news);
          });

    }
  }, function (resp) {
    //$ionicLoading.notify('Failed to get news');
  });
  ContactsService.getPhoneContacts().then(function(res){
    vm.contacts = res;
  },function(err){
    $log.debug(err);
  });
  RechargeService.getExchangeRates().then(function(res){
    $log.debug(res);
    vm.rates = res;
  },function(err){
    $log.debug(err);
  });
  $ionicLoading.show();
  RechargeService.getCountriesToRecharge("en").then(function(countries){
    vm.countryList = countries;

    if($stateParams.contact){
      _performStepsForContact();
    } else if ($stateParams.pushCountryIso) {
      _performStepsForPushNotification();
    } else {
      StoreService.getObject("rechargeCountry",
          function (value) {
            var localcountry = value.country;
            var result = vm.countryList.filter(function( obj ) {
              return obj.country == localcountry;
            });
            vm.selectedCountry=result[0];
          }, function (error) {
            // nothing
          });
    }

    $ionicLoading.hide();
  },function(err){
    $log.debug(err);
    $ionicLoading.hide();
  });


  function _performStepsForPushNotification() {
    var result = vm.countryList.filter(function( obj ) {
      console.log("both",obj,$stateParams.pushCountryIso);
      return (obj.iso == $stateParams.pushCountryIso);
    });
    vm.selectedCountry=result[0];

    vm.onCountrySelect({
      onSuccess: function(services){
        // console.log(vm.selectedCountry);
        if (vm.selectedCountry.country=='Cuba') {
          $scope.cuba=true;
        }
        services = services.services;
        console.log(services);
        var service = null;

        for(var i=0,l=services.length; i<l; i++){
          if($stateParams.rechargeType==services[i].service){
            service = services[i];
            break;
          }
        }
        vm.selectService(service);
      },
      onError: function(err){
        console.log(err);
      }
    });
  }

  function _performStepsForContact(){
    var phone = $stateParams.contact.contact_phone.replace("011","");
    phone = phone.replace($stateParams.contact.country_code,"");
    vm.rechargeNumber=phone;
    if($stateParams.contact.contact_email){
      vm.rechargeEmail = $stateParams.contact.contact_email;
    }
    var result = vm.countryList.filter(function( obj ) {
      console.log("both",obj,$stateParams.contact);
      return (obj.iso == $stateParams.contact.contact_country_iso && obj.country_code == $stateParams.contact.country_code);
    });
    vm.selectedCountry=result[0];

    vm.onCountrySelect({
      onSuccess: function(services){
        services = services.services;
        console.log(services);
        var service = null;

        for(var i=0,l=services.length; i<l; i++){
          if($stateParams.rechargeType==services[i].service){
            service = services[i];
            break;
          }
        }
        vm.selectService(service);
      },
      onError: function(err){
        console.log(err);
      }
    });
  }


  // Create the login modal that we will use later
  vm.selectedContact = null;
  $ionicModal.fromTemplateUrl('templates/modals/select-contact.html', {
    scope: $scope
  }).then(function(modal) {
    vm.contactModal = modal;
  });

  vm.selectContactAsDestination = function(type){
    vm.contactModal.type= type;
    vm.contactModal.show();
  };
  vm.hideContactModal = function(){
    vm.contactModal.hide();
    vm.selectedContact = null;
  };
  vm.onContactSelect = function(){
    if(vm.contactModal.type=="phone"){
      vm.rechargeNumber = angular.copy(vm.selectedContact.number);
    }
    else{
      vm.rechargeEmail = angular.copy(vm.selectedContact.contact_email);
    }
    vm.hideContactModal();
  };

  vm.help = function(){
    // $ionicPopup.alert({
    //      title: 'TAX/VAT',
    //      template: '35c over sales total.'
    //    });

    DialogService.alert("", vm.popup_translations.Tax_exempt, angular.noop,$scope,'local-call-popup custom-popup popup-body-center','info');
  };
  vm.paypalHelp = function(){
    DialogService.alert("", "2.9% + USD 0.30 "+vm.popup_translations.cent, angular.noop,$scope,'local-call-popup custom-popup popup-body-center','info');
  };

  $scope.$on('$cordovaInAppBrowser:exit', function(e, event){
    $cordovaInAppBrowser.close();
  });
  vm.onCountrySelect = function(config){
    config = config? config : {};
    $ionicLoading.show();
    if(vm.selectedCountry) {
      if (vm.selectedCountry.country=='Cuba') {
        $scope.cuba=true;
      }else{
        $scope.cuba=false;

      }
      RechargeService.getCountryRecharges(vm.selectedCountry.row_id).then(function (res) {
        $log.debug(res);
        $ionicLoading.hide();
        vm.wizard.step = 2;
        vm.countryServices = res;
        StoreService.setObject('rechargeCountry', vm.selectedCountry);
        if (config.onSuccess) {
          config.onSuccess(vm.countryServices);
        }
      }, function (err) {
        $ionicLoading.hide();
        $log.debug(err);
        if (config.onError) {
          config.onError(err);
        }
      });
    }
    else{
      $ionicLoading.notify(vm.popup_translations.this_country_not_available_for_recharging);
    }
  };

  // vm.prepareServices = function(){
  //   vm.wizard.step = 2;
  // }
  vm.selectService = function(service){
    vm.selectedService = service;
    vm.wizard.step = 3;
    if(vm.countryServices.validate_in=='server'){
      vm.serverValidateNext(true);
    }
  };

  vm.validateNumber = function(number){
    // 3221385168
    if(!number){
      DialogService.alert("", vm.popup_translations.please_enter_number, angular.noop,$scope,'local-call-popup custom-popup popup-body-center','error');
      return;
    }
    var num = angular.copy(number);
    num = vm.selectedCountry.country_code + num.toString();
    $ionicLoading.show();
    RechargeService.getDestinationCharges(num).then(function(res){
      $log.debug(res);
      vm.rechargeValidation = res;
      // vm.selectedTopup = vm.rechargeValidation.topups[0];
      vm.selectedTopup = "";
      vm.wizard.step = 4;
      $ionicLoading.hide();
    },function(err){
      $log.debug(err);
      DialogService.alert(vm.popup_translations.error,vm.popup_translations.could_not_validate_number);
      $ionicLoading.hide();
    })
  };

  vm.validateNauta = function(email){
    if(vm.selectedTopup==""){
      DialogService.alert("", vm.popup_translations.please_select_topup_amount, angular.noop,$scope,'local-call-popup custom-popup popup-body-center','error');

      return;
    }
    if(!email){
      DialogService.alert("", vm.popup_translations.please_enter_valid_email, angular.noop,$scope,'local-call-popup custom-popup popup-body-center','error');
      return;
    } //TODO: uncomment this for validate nauta
    //else if (!RechargeService.validateNautaLocal(email)) {
    //  DialogService.alert(vm.popup_translations.error,vm.popup_translations.invalid_nauta_email);
    //  return;
    //}

    $ionicLoading.show();
    var product = vm.selectedTopup.product;
    RechargeService.validateNauta(email,product).then(function(res){
      $log.debug(res);
      if (res.validation.verified == 1) {
        vm.payOrAddRecharge(true);
      } else if (res.validation.verified == 0) {
        DialogService.alert("", vm.popup_translations.invalid_nauta_email, angular.noop,$scope,'local-call-popup custom-popup popup-body-center','warning');
      }


      $ionicLoading.hide();
    },function(err){
      $log.debug(err);
      DialogService.alert("", vm.popup_translations.could_not_validate_nauta, angular.noop,$scope,'local-call-popup custom-popup popup-body-center','error');
      $ionicLoading.hide();
    })
  };

  vm.serverValidateNext = function(dontChangeStep){
    $ionicLoading.show();
    $log.debug(vm.wizard);
    var obj = angular.copy(vm.selectedService);
    obj.topups = obj.recharges;
    obj.topups = RechargeService.processTopups(obj);

    vm.rechargeValidation = obj;
    if(vm.selectedService.service=='mobile'){
      vm.rechargeValidation.destination_msisdn = vm.rechargeValidation.destination_msisdn ?
        vm.rechargeValidation.destination_msisdn : vm.rechargeNumber.toString();
    }
    else{
      vm.rechargeValidation.destination_email = vm.rechargeEmail;
    }
    // vm.selectedTopup = vm.rechargeValidation.topups[0];
    vm.selectedTopup = "";
    if(!dontChangeStep){
      vm.wizard.step =4;
    }
    $ionicLoading.hide();
  };

  vm.showCart = function(){
    var obj = {};
    obj.iso = vm.selectedCountry.iso;
    obj.country_code = vm.selectedCountry.country_code;
    obj.serviceType = vm.selectedService.service;
    obj.country_id = vm.selectedService.country_id;
    obj.validate_in = vm.countryServices.validate_in;
    obj.recharge_id = vm.selectedTopup.id;
    obj.product = vm.selectedTopup.product;
    if(obj.serviceType=='mobile'){
      obj.number = vm.rechargeNumber;
      obj.destination = obj.country_code + obj.number;
    }
    else{
      obj.email = vm.rechargeEmail;
      obj.destination = obj.email;
    }
    obj.amount = vm.selectedTopup.local_amount + " " + vm.rechargeValidation.local_info_currency;
    obj.displayCurrency = UserService.userData.currency;
    // obj.price = RechargeService.convertUsingRates({
    //   price: vm.selectedTopup.price,
    //   currency: vm.rechargeValidation.local_info_currency? vm.rechargeValidation.local_info_currency : obj.displayCurrency
    // },obj.displayCurrency);
    obj.price = vm.selectedTopup.price;
    obj.bonus = vm.selectedTopup.bonus;

    vm.cart.items.push(obj);
    _calculateCartTotal();
    vm.wizard.step = 5;


  };

  function _calculateCartTotal(){
    if(vm.cart.items.length==0) return;
    var currency = vm.cart.items[0].displayCurrency;
    var total = 0;
    for(var i=0,l=vm.cart.items.length; i<l;i++){
      total += parseFloat(vm.cart.items[i].price);
    }
    vm.cart.currency = currency;
    vm.cart.subTotal = total;
    vm.cart.tax = 0;
    vm.cart.totalToPay = vm.cart.subTotal + parseFloat(vm.cart.tax);
  }
  vm.backToStep = function(step){
    vm.wizard.step = step;
  };

  vm.deleteItem = function(item){
    $scope.showConfirm(vm.popup_translations.confirm_delete_recharge_1,vm.popup_translations.confirm_delete_recharge,vm.popup_translations.ok,vm.popup_translations.cancel,function(){
      vm.cart.items.splice(vm.cart.items.indexOf(item),1);
      _calculateCartTotal();
      if(vm.cart.items.length ==0){
        vm.cart.reset();
        _resetSteps();
        vm.card.reset(true);
        vm.wizard.step=1;
      }
    },function(){

    });
  };

  vm.onCountrySelectWrap = function(countrySelected) {
    vm.selectedCountry = countrySelected;
    vm.onCountrySelect();
  };

  function _resetSteps() {
    vm.countryServices = null;
    vm.selectedService = null;
    vm.rechargeValidation = null;
    vm.selectedTopup = null;
  }

  function _createCardParams() {
    var params = {};
    if(vm.card.mode=="new"){
      params.card_number = vm.card.number;
      params.exp_month = vm.card.month.month;
      params.exp_year = vm.card.year.year;
      params.cvv = vm.card.cvc;
      params.zip = vm.card.postalCode;
      params.save_card = vm.card.remember;
      params.card_option = "new";
    }
    else{
      params.card_number = vm.card.selectedCard.customerPaymentProfileId["0"];
      params.card_option = "saved";
    }


    var recharges = [];
    for(var i=0,l=vm.cart.items.length; i<l; i++){
      var str = "";
      var obj = vm.cart.items[i];
      str += obj.destination + "|";
      str += obj.serviceType + "|";
      str += obj.validate_in + "|";
      str += obj.product + "|";
      str += obj.recharge_id + "|";
      str += obj.country_id;
      recharges.push(str);
    }

    params.recharges = recharges;
    return params;

  }

  function _createPayPalParams(){
    var params = {};
    var recharges = [];
    for(var i=0,l=vm.cart.items.length; i<l; i++){
      var str = "";
      var obj = vm.cart.items[i];
      str += obj.destination + "|";
      str += obj.serviceType + "|";
      str += obj.validate_in + "|";
      str += obj.product + "|";
      str += obj.recharge_id + "|";
      str += obj.country_id;
      recharges.push(str);
    }

    params.recharges = recharges;
    return params;
  }

  function _shopPopupPayOrAddRecharge() {
    var myPopup = $ionicPopup.show({
       template: vm.popup_translations.recharge_popup_title,
      title: '<div class="success-icon-popup"><i class="icon ion-ios-checkmark-outline"></i></div>',
      // subTitle: vm.popup_translations.recharge_popup_subtitle,
      scope: $scope,
      cssClass: 'local-call-popup custom-popup popup-body-center',
      buttons: [
        {
          text: vm.popup_translations.add_more,
          type: 'button-energized',
          onTap: function(e) {
            $log.debug("tapped add more");
            vm.showCart();
            // _resetSteps();
            vm.selectedTopup = null;
            vm.wizard.step=2;
          }
        },
        {
          text: '<b>'+vm.popup_translations.pay_now+'</b>',
          type: 'button-balanced',
          onTap: function(e) {
            $log.debug("tapped paynow");
            vm.showCart();
            vm.scrollToBottom();
          }
        }
      ]
    });

    myPopup.then(function(res) {
      // $log.debug('Tapped!', res);
    });

    // $timeout(function() {
    //    myPopup.close(); //close the popup after 3 seconds for some reason
    // }, 3000);
  }

  vm.payOrAddRecharge = function(isNauta, validate_in) {
    // An elaborate, custom popup
    if(vm.selectedTopup=="") {
      DialogService.alert("", vm.popup_translations.please_select_topup_amount, angular.noop,$scope,'local-call-popup custom-popup popup-body-center','error');

      return;
    }



    if (isNauta) {
      _shopPopupPayOrAddRecharge();
    } else {
      var num = angular.copy(vm.rechargeNumber);
      if (num!=null && num!="") {
        num = vm.selectedCountry.country_code + num.toString();
        _shopPopupPayOrAddRecharge();
      } else {
        DialogService.alert("", vm.popup_translations.phone_number_must_not_empty, angular.noop,$scope,'local-call-popup custom-popup popup-body-center','warning');
      }
    }



  };

  // A confirm dialog
  $scope.showConfirm = function(title,msg,ok,cancel,onSuccess,onFailure) {
   var confirmPopup = $ionicPopup.confirm({
     title: '<div class="error-icon-popup"><i class="icon ion-ios-close-outline" ></i></div>',
     template: msg,
     cssClass: 'local-call-popup custom-popup popup-body-center',
     cancelText: cancel,
     okText: ok
   });

   confirmPopup.then(function(res) {
     if(res) {
       $log.debug('You are sure');
       if(onSuccess){
         onSuccess();
       }
     } else {
       $log.debug('You are not sure');
       if(onFailure){
         onFailure();
       }
     }
   });
  };


  vm.payWithCard = function(){
    vm.card.show = true;
    vm.scrollToTop();
  };

  vm.choosePaypal = function() {
    vm.paypalReady.show = true;
    vm.wizard.step = 6;

    RechargeService.calculatePayPalFee({amount:vm.cart.subTotal}).then(function(res){
      $log.debug(res);

      vm.cart.paypalfee = res.paypal_fee;
      vm.cart.paypalTotal = res.paypal_fee + vm.cart.totalToPay;

    },function(err){
      $ionicLoading.notify(err,2000);
    });

    vm.scrollToTop();
  };
  vm.payWithPayPal= function(){
    $ionicLoading.show();

    PaypalService.initPaymentUI().then(function () {
      PaypalService.makePayment(vm.cart.paypalTotal, "Total Amount").then(function (response) {
        $log.debug(JSON.stringify(response));
        var pms = _createPayPalParams();
        RechargeService.payByPayPal({paypal_transaction_id: response.response.id,recharges: pms.recharges}).then(function(){
          // $ionicLoading.notify("Transaction completed successfuly");
          $ionicLoading.hide();
          $rootScope.$emit('updateBalance');
          vm.choiceAfterPayment();
        },function(err){
          $ionicLoading.notify(err,2000);
        });
      }, function (error) {
        $ionicLoading.notify(error,2000);
      });
    });


  };

  vm.choiceAfterPayment = function(){
    $ionicPopup.show({
      // template: '<input type="password" ng-model="data.wifi">',
      title: vm.popup_translations.payment_successful,
      subTitle:vm.popup_translations.what_to_do,
      scope: $scope,
      buttons: [
        {
          text: vm.popup_translations.add_another_recharge,
          type: 'button-energized',
          onTap: function(e) {
            vm.cart.reset();
            _resetSteps();
            vm.card.reset(true);
            vm.wizard.step = 1;
          }
        },
        {
          text: vm.popup_translations.close,
          type: 'button-balanced',
          onTap: function(e) {
            $state.transitionTo('pay-history');
          }
        }
      ]
    });
  };

  vm.makeCardPayment= function(){

    var params = _createCardParams();
    $log.debug(params);
    // params.sandbox = true;
    $ionicLoading.show();
    RechargeService.payByCard(params).then(function(res){
      $ionicLoading.hide();
      // $ionicLoading.notify("Transaction Successful");
      vm.choiceAfterPayment();
      $rootScope.$emit('updateBalance');
    },function(err){ // multi-language
      $ionicLoading.hide();
      DialogService.alert("", err, angular.noop,$scope,'local-call-popup custom-popup popup-body-center','error');
    });
  };

  vm.scrollToTop = function() {
    $ionicScrollDelegate.$getByHandle('rechargeScroll').scrollTop(true);
  };
  vm.scrollToBottom = function() {
    $ionicScrollDelegate.$getByHandle('rechargeScroll').scrollBottom(true);
  };


  vm.stepBack = function(){
   if(vm.wizard.step>1){
     vm.wizard.step = vm.wizard.step-1;
     if(vm.wizard.step == 4){
       vm.cart.show = false;
       vm.cart.items.splice(vm.cart.items.length-1,1);
       if(vm.card.show){
         vm.card.show = false;
       }
     }
     else if(vm.wizard.step==5){
       vm.card.show = false;
     }
   }
   else if(vm.wizard.step==1){
     $ionicHistory.goBack();
   }
  };

});

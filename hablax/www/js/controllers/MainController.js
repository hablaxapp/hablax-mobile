/** MainController*/
hablaxControllers.controller('MainController', function($ionicPopup,
  $ionicPlatform,$scope,$state,$cordovaInAppBrowser,$ionicLoading, $log,
  $localStorage,UserService, $rootScope, $translate, CommonService, AuthService) {
  // Login view's bindings go here.
  $log.debug("inside main controller");
  $ionicLoading.notify = function(message,duration){
    $ionicLoading.hide();
    duration = duration ? duration : 1500;
    $ionicLoading.show({
      template: message,
      duration: duration
    });
  };

  var vm = this;
  vm.translations = {};
  function _translate() {
    $translate(['exit_hablax','are_your_sure_exit']).then(function (translations) {
      vm.translations=translations;
    }, function (translationIds) {
      vm.translations=translationIds;
    });
  }

  _translate();

  $scope.$on("languageChanged",function(e,args){
    _translate();
  });


  vm.updateApp = function() {
    CommonService.updateApp();
  };

  vm.showForceUpdate = false;
  vm.checkVersion = function() {

    CommonService.appInfo().then(function (response) {
ionic.Platform.ready(function() {
      if (ionic.Platform.isAndroid() || ionic.Platform.isIOS()) {
        cordova.getAppVersion.getVersionNumber().then(function (version) {
          currentVersion = version;
          vm.showForceUpdate = parseFloat(response.app_minimum_version) > parseFloat(currentVersion);
          if (vm.showForceUpdate) {
            if (minimumVersionSchedule!=null) {
              clearTimeout(minimumVersionSchedule);
              minimumVersionSchedule = null;
              $state.go('forceUpdate');
            }
          }
        });
      } else { // just for test force update
        //vm.showForceUpdate = true;
        //if (vm.showForceUpdate) {
        //  if (minimumVersionSchedule!=null) {
        //    clearTimeout(minimumVersionSchedule);
        //    minimumVersionSchedule = null;
        //    $state.go('forceUpdate');
        //  }
        //}
      }

    }, function (error) {
      // nothing
    });
     });
  };


  var minimumVersionSchedule=null;
  vm.startCheckMinimumVersion = function(result){
    if (minimumVersionSchedule==null) {
      minimumVersionSchedule = setInterval(function(){
        vm.checkVersion();
      }, 300000); // 5 min
      vm.checkVersion();
    }
  };
  vm.startCheckMinimumVersion();

  var backButtonHandler = null;
  var registerBackButton = function(){
      backButtonHandler = $ionicPlatform.registerBackButtonAction(function (event) {
        event.preventDefault();
        event.stopImmediatePropagation();
        $ionicPopup.confirm({
            title: vm.translations.exit_hablax,
            template: vm.translations.are_your_sure_exit
        }).then(function(res){
            if( res ){
              navigator.app.exitApp();
            }
        });
      }, 101);
  };
  var deRegisterBackButton = function(){
      if(backButtonHandler)
          backButtonHandler();
  };

  $rootScope.$on('$cordovaInAppBrowser:loadstop', function(e, event){
    // insert CSS via code / file
    if(event.url.match("https://www.hablax.com/app/help/close")){
      $cordovaInAppBrowser.close();
    }
  });

  $rootScope.$on('$ionicView.enter',function(e){
    if($state.current.name=='tab.dial'){
      registerBackButton();
    }
    else{
      deRegisterBackButton();
    }
  });

  $ionicPlatform.on("pause", function onPause() {
    // nothing
  });
  $ionicPlatform.on("resume", function onResume() {
    $rootScope.$emit('updateBalance');
    UserService.getUserProfile().then(function(userData) {
      if (!userData) {
        $rootScope.$emit('invalid_token');
      }
    }, function() {
      // nothing
    });
  });
  $ionicPlatform.on("online", function onOnline() {
    CommonService.online = true;
    $rootScope.$emit('online');
  });
  $ionicPlatform.on("offline", function onOffline() {
    CommonService.online = false;
    $rootScope.$emit('offline');
  });


  window.addEventListener('native.keyboardshow', function () {
//    document.body.classList.add('keyboard-open');
//    var content = angular.element(document.getElementById($state.current.name));
//    content.removeClass("has-footer");
  });
  window.addEventListener('native.keyboardhide', function () {
//    document.body.classList.remove('keyboard-open');
//    var content = angular.element(document.getElementById($state.current.name));
//    content.addClass("has-footer");
  });


});

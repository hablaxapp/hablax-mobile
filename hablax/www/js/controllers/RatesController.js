hablaxControllers.controller('RatesController',
  ['RechargeService', '$ionicLoading','$log', '$scope', 'AuthService', '$state', 'RatesService','$ionicPopover', '$ionicHistory', 'typeOfRates', 'DialogService', 'UserService', '$translate', 'StoreService',
    function (RechargeService, $ionicLoading,  $log, $scope, AuthService, $state, RatesService,$ionicPopover, $ionicHistory, typeOfRates, DialogService, UserService, $translate, StoreService) {

      // Login view's bindings go here.
      $log.debug("inside rates controller");

      var vm = this;

$ionicPopover.fromTemplateUrl('templates/rates/nauta_popover.html', {
          scope: $scope
        }).then(function(popover) {
          console.log('aca entro');
          $scope.popover = popover;
        });

      

      vm.nautas=function($event)
      {
          $scope.popover.show($event);
             RatesService.getNauta().then(function (resp) {
              vm.nautas_recargas=resp.recargas;
              $scope.seleccionado=resp.recargas[0];
               console.log(resp);
                
              }, function (resp) {
                $ionicLoading.notify(vm.translations.failed_get_sms_country_list);
              });
      }

  
        $scope.closePopover = function() {
          $scope.popover.hide();
        };
        //Cleanup the popover when we're done with it!
        $scope.$on('$destroy', function() {
          $scope.popover.remove();
        });
        // Execute action on hidden popover
        $scope.$on('popover.hidden', function() {
          // Execute action
        });
        // Execute action on remove popover
        $scope.$on('popover.removed', function() {
          // Execute action
        });
  

      vm.smsCountryList = [];
      vm.selectedCountry = null;
      vm.selectedCallCountry = null;
      vm.selectedSmsCountry = null;
      vm.rates = [];
      vm.unknown = [];
      vm.convertRate = 1;
      vm.selectedCallCurrency = "";
      vm.selectedSMSCurrency = null;
      vm.selectedSMSCurrencyRate = "";
      vm.selectedRechargeCurrency = "";
      vm.selectedCountryRate = 0;
      vm.rechargeQuantity = ["CUC 10", "CUC 15", "CUC 20", "CUC 25", "CUC 30", "CUC 35", "CUC 40", "CUC 45", "CUC 50"];
      vm.selectedRechargeQuantity = "";
      vm.currencies = ["EUR", "USD", "CAD", "BRL"];
      vm.selectedSMSCurrency = vm.currencies[1];


      vm.selectedRechargeQuantity = vm.rechargeQuantity[0];
      vm.searchPhoneNumber = "";
      vm.searchedCountryName = "";
      vm.step = 1;

      vm.translations = {};

      function _doTranslate() {
        $translate(['failed_get_sms_country_list','please_enter_number','could_not_validate_number',
                'rate_disclaimer_title','rate_disclaimer_content','HELP','no_results_found']).then(function (translations) {
          vm.translations=translations;
        }, function (translationIds) {
          vm.translations=translationIds;
        });

        StoreService.get("hablax_lang",
            function (value) {
              if (value) {
                vm.langCode = value;
              } else {
                vm.langCode = "en";
              }
            }, function (error) {
              vm.langCode = "en";
            });
      }
      _doTranslate();

      $scope.$on("languageChanged",function(e,args){
        _doTranslate();
      });

vm.nuevabusqueda=function(){
  $state.reload();
}
      // navigate to previous page
      vm.goBack = function () {
        $ionicHistory.goBack();
      };
      //get sms country and rates  list
      vm.getPriceList = function () {
        RatesService.getPrices().then(function (prices) {
          vm.smsCountryList = prices.calls;

          $log.debug(vm.smsCountryList);
        }, function (resp) {
          $ionicLoading.notify(vm.translations.failed_get_sms_country_list);
        });
      };

      vm.getCountryList = function () {
        RatesService.getCountryList().then(function (countries) {
          vm.smsCountryList = countries;

          $log.debug(vm.smsCountryList);
        }, function (resp) {
          $ionicLoading.notify(vm.translations.failed_get_sms_country_list);
        });
      };

      vm.getCountryListForRecharge = function () {
        RechargeService.getCountriesToRecharge().then(function (countries) {
          vm.smsCountryList = countries;

          $log.debug(vm.smsCountryList);
        }, function (resp) {
        //  alert('Failed to get sms Country List');
        });
      };
      vm.onSmsRateChange = function () {
        vm.selectedSMSCurrency = vm.currencies[1];
        console.log(vm.selectedRate);
      };
      vm.onCallRateChange = function () {
        vm.selectedSMSCurrency = vm.currencies[1];
        console.log(vm.selectedRate);
      };

      vm.validateNumber = function(number){
        // 3221385168
        if(!number){
          DialogService.alert("Error",vm.translations.please_enter_number);
          return;
        }
        var num = angular.copy(number);
        num = vm.selectedCountry.country_code + num.toString();
        $ionicLoading.show();
        RechargeService.getDestinationCharges(num).then(function(res){
          $log.debug(res);
          vm.rechargeValidation = res;
          vm.selectedTopup = "";
          $ionicLoading.hide();
        },function(err){
          $log.debug(err);
          DialogService.alert("Error",vm.translations.could_not_validate_number);
          $ionicLoading.hide();
        })
      };

      vm.searchRates = function() {

        $ionicLoading.show();
        RatesService.getAllRates(vm.searchPhoneNumber).then(function (rates) {
          $ionicLoading.hide();
          vm.rates = rates;
          if (vm.rates.recharges.error_code=="0") {
            vm.recarga = true;
          }else{

            vm.recarga = false;
          }
          $log.debug(vm.rates);
          vm.step = 2;

        }, function (resp) {
          $ionicLoading.hide();
          if (resp.error_code == "no_results_found") {
            DialogService.alert(vm.translations.HELP, vm.translations.no_results_found, angular.noop,$scope,'local-call-popup custom-popup popup-body-center', 'info');
          }

        });

      };



      vm.onTopUpSelect = function(){

      };

      vm.convertUSDRates = function () {
        if (UserService.needShowCurrencyDisclaimer) {
          UserService.needShowCurrencyDisclaimer = false;
          DialogService.alert(vm.translations.rate_disclaimer_title,vm.translations.rate_disclaimer_content);
        }
        var outputCurrency = vm.selectedSMSCurrency;
        switch (outputCurrency) {
          case "USD":
            $log.debug(outputCurrency);
            vm.convertRate = 1;
            break;
          case "CAD":
            vm.convertRate = 1 / 1.34;
            break;
          case "EUR":
            vm.convertRate = 1.10;
            break;
          case "BRL":
            vm.convertRate = 1/3;
            break;
          default:
            break;
        }

      };
      //get  rates  list
      vm.getRates = function () {
        RatesService.getRates().then(function (rates) {
          vm.rates = rates;
          $log.debug(vm.rates);
        }, function (resp) {
        //  alert('Failed to get rates List');
        });
      };

        vm.moveUpHeight = 0;
    window.addEventListener('native.keyboardshow', function (e) {
        $scope.$apply(function () {
            vm.moveUpHeight = 100;
            console.log(vm.moveUpHeight);
        });
    });
    window.addEventListener('native.keyboardhide', function () {
        $scope.$apply(function () {
            console.log(vm.moveUpHeight);
            vm.moveUpHeight = 0;

        });
    });

      //to be implemented for recharge quantity change
      vm.onCountrySelect  = function (countrySelected) {
        vm.selectedCountry = countrySelected;
        $ionicLoading.show();
        RechargeService.getCountryRecharges(vm.selectedCountry.row_id).then(function (res) {
          $log.debug(res);
          $ionicLoading.hide();
          vm.selectedService = null;
          vm.selectedTopup = null;
          // vm.wizard.step=2;
          vm.countryServices = res;
          vm.getRates();
        }, function (err) {
          $ionicLoading.hide();
          $log.debug(err);
        });
      };

      vm.selectService = function(service){
        vm.selectedService = service;
        if(vm.countryServices.validate_in=='server'){
          vm.serverValidateNext(true);
        }
      };

      vm.serverValidateNext = function(dontChangeStep){
        $ionicLoading.show();
        $log.debug(vm.wizard);
        var obj = angular.copy(vm.selectedService);
        obj.topups = obj.recharges;
        obj.topups = RechargeService.processTopups(obj);

        vm.rechargeValidation = obj;
        vm.selectedTopup = "";
        $ionicLoading.hide();
      };


      //to be implemented for recharge quantity change
      vm.onQuantityChange = function () {

      };
      //load data according to views
      vm.onControllerLoad = function () {
        switch (typeOfRates) {
          case 'call':
            vm.getPriceList();
            break;
          case 'sms':
            vm.getCountryList();
            break;
          case 'main':
            break;
          default:
            vm.getCountryListForRecharge();
            break;

        }
      };
      vm.onControllerLoad();
    }]);

hablaxControllers.controller('MessagesController',
    ['$log', '$scope', '$state', '$rootScope', '$stateParams','$ionicLoading', 'MessagesService','mode', '$ionicPopup','$translate','$localStorage','$location','$anchorScroll','UserService','StoreService','$ionicScrollDelegate','NewsService','ContactsService','DialogService',
      function($log, $scope, $state, $rootScope, $stateParams,$ionicLoading,  MessagesService,mode, $ionicPopup,$translate,$localStorage,$location,$anchorScroll,UserService,StoreService,$ionicScrollDelegate,NewsService, ContactsService, DialogService) {

        var self = this;
        // self.chat = Chats.get($stateParams.chatId);
        // self.chats = Chats.all();
        self.messagesList = [];
        self.destinationAPIIndex = 0;
        self.allDataLoaded = false;
        self.messageData = {};
        self.messageIs = 'Lorem ipsum simple dummy content';
        self.countryList=[];
        self.selectedCountry=null;
        self.newMessage={};
        self.isFreshPage=true;
        self.keyboardShown=false;
        self.currentRate=null;

        self.records = [];
        self.callIndex = 0;
        self.allDataLoaded = false;

        self.hideNotify = true;
        self.notifyMessage = null;

        self.messageText=null;
        self.maxLengthMessage = 160;


        var _NORMAL_BOTTOM_CONTENT_MESSAGE_SENT = 25;
        self.keyboardHeightContent=_NORMAL_BOTTOM_CONTENT_MESSAGE_SENT;

        var _NORMAL_BOTTOM_CHAT_BOX = 55;
        self.keyboardHeight=_NORMAL_BOTTOM_CHAT_BOX;

        var _groupMessages;

        function _doTranslate() {
          $translate(['message_sent_successfully', 'confirm_delete','confirm_delete_sms', 'sure_to_delete','Information', 'sms_no_phone_number', 'sms_no_message',
              'failed_get_sms_country_list','destination_number_must_be','entered_number_invalid','failed_to_send_message', 'Cancel',
              'not_found_number_in_contact','yes_new_contact','hablax','contact_could_not_be_saved','contact_name_must_be_filled',
              'Information']).then(function (translations) {
            self.popup_translations=translations;
          }, function (translationIds) {
            self.popup_translations=translationIds;
          });

          StoreService.get("hablax_lang",
              function (value) {
                if (value) {
                  var language=value;
                  self.langCode = value;
                } else {
                  self.langCode = "en";
                }
              }, function (error) {
                self.langCode = "en";
              });
        }
        _doTranslate();

        $scope.$on("languageChanged",function(e,args){
          _doTranslate();
        });

        function _getNews(lang, news) {
          if (lang == "en") {
            self.notifyMessage = news.notice.notice.en;
          } else if (lang == "es") {
            self.notifyMessage = news.notice.notice.es;
          }
        }

        NewsService.getNews('sms').then(function (news) {
          $log.debug(news);
          if (news.status != 'error') {
            self.hideNotify = false;
            StoreService.get("hablax_lang",
                function (value) {
                  if (value) {
                    var language=value;
                    _getNews(language, news);
                  } else {
                    _getNews("en", news);
                  }
                }, function (error) {
                  _getNews("en", news);
                });

          }
        }, function (resp) {
          //$ionicLoading.notify('Failed to get news');
        });
        $scope.$on('$ionicView.afterEnter', function(){
        $ionicScrollDelegate.scrollBottom();
        $ionicLoading.hide();
        });
        self.getSentMessagesList=function() {
          $ionicLoading.show();
          MessagesService.sentMessagesList().then(function(resp) {
            self.messagesList = resp.sms;
            $log.debug(self.messagesList);
            console.log(self.messagesList);
            $ionicLoading.hide();
            angular.forEach(self.messagesList,function(value,key) {
              self.messagesList[key].limit=20;
            });
          }, function(resp) {
            $ionicLoading.hide();
            //  alert('Send Messages List Failed to be fetched.')
          });
        };

        $scope.$watch(function(){
          return $state.$current.name
        }, function(newVal, oldVal){
          if (newVal != "tab.messages.write-message" && oldVal == "tab.messages.write-message") {
            $rootScope.$broadcast("message_name_show", {show: false});
          }
          //else if (newVal == "tab.messages.write-message") {
          //
          //}

        });

        self.goHistory = function(index) {
          var listItem = self.messagesList.filter(function (el) {
            return el.key === index;
          });
          if (listItem.length > 0) {
            item = listItem[0];
            item.number = item.destination;
            $state.go('tab.messages.write-message', {'contact': item});
            $ionicLoading.show();
            // $ionicScrollDelegate.scrollBottom(true);
          }
        };

        self.searchLastDestinations=function(refresh) {
          //$ionicLoading.show();
          MessagesService.searchLastDestinations(self.destinationAPIIndex).then(function(resp) {
            if (resp.sms==false) {
              self.allDataLoaded = true;
              $ionicLoading.hide();
              $scope.$broadcast('scroll.infiniteScrollComplete');
              return;
            }

            var begin = self.messagesList.length;
            self.messagesList = self.messagesList.concat(resp.sms);
            self.destinationAPIIndex = self.messagesList.length;
            if (self.destinationAPIIndex >= resp.total_count) {
              self.allDataLoaded = true;
            }

            $scope.$broadcast('scroll.infiniteScrollComplete');


            //for (index = begin; index < self.messagesList.length; index++) {
            angular.forEach(self.messagesList,function(value,key) {
              if (key >= begin && key < self.messagesList.length) {

                value.limit = 20;
                value.key = key;

                value.show_name = value.contact_phone;
                if (value.contact_name != null) {
                  value.show_name = value.contact_name
                }

                var destination = value.number;
                if (value.number == null) {
                  destination = value.contact_phone.replace(value.country_code, "");
                }

                MessagesService.smsSearchHistory(destination, value.country_code, value.contact_country_iso).then(function (resp) {
                  value.smsList = resp.sms;
                  $ionicLoading.hide();
                }, function (resp) {
                  $ionicLoading.hide();
                });
              }
            });

            $log.debug(self.messagesList);
            console.log(self.messagesList);
          }, function(resp) {
            $ionicLoading.hide();
            $scope.$broadcast('scroll.infiniteScrollComplete');
            //  alert('Send Messages List Failed to be fetched.')
          });
        };


        self.tappedNum=function() {
          // set the location.hash to the id of
          // the element you wish to scroll to.
          $location.hash('msgNum');

          // call $anchorScroll()
          $anchorScroll();
        };
        self.tappedMsg=function() {
          // set the location.hash to the id of
          // the element you wish to scroll to.
          $location.hash('msgMsg');

          // call $anchorScroll()
          $anchorScroll();
        };

        self.deleteAllMessage = function(index, $event) {
          if ($event) {
            $event.stopImmediatePropagation();
          }

          self.showConfirm(self.popup_translations.confirm_delete,self.popup_translations.sure_to_delete,function(){

            var value = self.messagesList[index];
            var destination = value.number;
            if (value.number == null) {
               destination = value.contact_phone.replace(value.country_code, "");
             }
            MessagesService.deleteAllMessage(destination, value.country_code, value.contact_country_iso).then(function(resp) {
              self.messagesList.splice(index,1);
            }, function(resp) {
              //alert('Failed to Delete');

            });
          },function(){

          });
        };

        self.deleteMessage = function(index) {
          self.showConfirm(self.popup_translations.confirm_delete,self.popup_translations.sure_to_delete,function(){
            self.messageData.row_id = index;
            MessagesService.deleteMessage(self.messageData).then(function(resp) {
              console.log(index, self.messageData,self.messagesList);
              self.messagesList=self.messagesList.filter(function(el) {
                return el.row_id !== index;
              });

              // console.log(self.messagesList);
            }, function(resp) {
              //alert('Failed to Delete');

            });
          },function(){

          });
        };

        self.getCountryList = function() {
          $ionicLoading.show();
          MessagesService.getCountryList().then(function(countries) {
            self.countryList=countries;
            $ionicLoading.hide();
            if($stateParams.contact!=null) {

              var result = self.countryList.filter(function( obj ) {
                return obj.iso == $stateParams.contact.contact_country_iso;
              });
              self.selectedCountry=result[0];

              if ($stateParams.contact.contact_name != null) {
                self.messageData.show_name=$stateParams.contact.contact_name;
                $rootScope.$broadcast("message_name_update", {show_name: self.messageData.show_name});


                var phone = $stateParams.contact.contact_phone.replace("+", "");
                phone = phone.replace("011", "");
                phone = phone.replace($stateParams.contact.country_code, "");
                self.messageData.number = phone;

              } else {
                if ($stateParams.contact.contact_phone != null) {
                  self.messageData.show_name = $stateParams.contact.contact_phone;
                  $rootScope.$broadcast("message_name_update", {show_name: self.messageData.show_name});
                  var phone = $stateParams.contact.contact_phone.replace("+", "");
                  // phone = phone.replace("011", "");
                  // phone = phone.replace(self.selectedCountry.country_code, "");
                  self.messageData.destination_number = phone;

                }

              }


            }
          }, function(resp) {
            $ionicLoading.notify(self.popup_translations.failed_get_sms_country_list);
          });
        };

        if(window.cordova && window.cordova.plugins.Keyboard) {
          window.addEventListener('native.keyboardshow', function(e) {
            $scope.$apply(function () {
              self.keyboardShown = true;
              console.log(window.screen.height);
              $ionicScrollDelegate.scrollBottom(true);
              self.keyboardHeight = e.keyboardHeight;
              self.keyboardHeightContent = e.keyboardHeight + _NORMAL_BOTTOM_CONTENT_MESSAGE_SENT;
              self.currentRate = null;
              if (self.selectedCountry!=null) {
                self.currentRate = _lookForCountryToGetRate(self.selectedCountry) * 100; // Cent unit
              } else {
                if (self.messageData.destination_number != null) {
                  var country = _lookForCountry(self.messageData.destination_number);
                  self.currentRate = country.rate * 100;  // Cent unit
                }
              }

            });
          });
          window.addEventListener('native.keyboardhide', function(e) {
            $scope.$apply(function () {
              self.keyboardHeight = _NORMAL_BOTTOM_CHAT_BOX;
              self.keyboardHeightContent = _NORMAL_BOTTOM_CONTENT_MESSAGE_SENT;
              self.keyboardShown = false;
            });
          });
        }


        function _relateDate(date) {
          var now = new Date();
          var nowEqualized = date.getDate() == now.getDate()
              && date.getMonth() == now.getMonth()
              && date.getYear() == now.getYear();
          if (nowEqualized) {
            return "Today";
          } else {
            var yesterday = new Date();
            yesterday.setDate(yesterday.getDate()-3);
            var yesterdayEqualized = date.getDate() == yesterday.getDate()
                && date.getMonth() == yesterday.getMonth()
                && date.getYear() == yesterday.getYear();
            if (yesterdayEqualized) {
              return "Yesterday";
            }
          }
          return null;
        }

        function _initGroupMessages(value) {
          //var miamiGMT = -5;
          //var offset = new Date().getTimezoneOffset();
          //var clientGMT = offset/60;
          //console.log(offset);

          date = value.sent_at.split(" ")[0].split("-");
          var valueDatetime = new Date( date[0], date[1], date[2], 0, 0, 0 ); // this for ios


          if (_groupMessages == null) {
            _groupMessages = {};
            _groupMessages.sentAtDate = value.sent_at;
            _groupMessages.sentAtDateTitle = _relateDate(valueDatetime);
            _groupMessages.messages = [];
            //if (self.messageData.orderedSmsList) {
            //
            //}
            self.messageData.orderedSmsList.push(_groupMessages);
          } else {

            var date = _groupMessages.sentAtDate.split(" ")[0].split("-");
            var groupDatetime = new Date( date[0], date[1], date[2], 0, 0, 0 );  // this for ios

            var equalized = groupDatetime.getDate() == valueDatetime.getDate()
                && groupDatetime.getMonth() == valueDatetime.getMonth()
                && groupDatetime.getYear() == valueDatetime.getYear();
            if (!equalized) {
              _groupMessages = {};
              _groupMessages.sentAtDate = value.sent_at;
              _groupMessages.sentAtDateTitle = _relateDate(valueDatetime);
              _groupMessages.messages = [];
              self.messageData.orderedSmsList.push(_groupMessages);
            }

          }
        }

        self.searchHistory=function() {
          if($stateParams.contact!=null) {
            self.isFreshPage = false;
            $rootScope.$broadcast("message_name_show", {show: true});
            self.messageData.destination_number = $stateParams.contact.number;
            if ($stateParams.contact.smsList == null) {
              $ionicLoading.show();
              MessagesService.smsSearchHistory(self.messageData.destination_number,
                  $stateParams.contact.country_code,
                  $stateParams.contact.contact_country_iso).then(function(resp) {
                self.messageData.smsList = resp.sms;
                $log.debug(resp.sms);
                console.log(resp.sms);
                $ionicLoading.hide();
              }, function (resp) {
                $ionicLoading.hide();
              });
            } else {
              self.messageData.smsList = $stateParams.contact.smsList;
              self.messageData.destination_number=self.messageData.smsList[0].destination;
              console.log( self.messageData.destination_number);
            }
          }

          self.messageData.orderedSmsList = [];
          if (self.messageData.smsList) {
            var index;
            for (index = self.messageData.smsList.length - 1; index >= 0; index--) {
              var key = index;
              var value = self.messageData.smsList[index];
              _initGroupMessages(value);
              _groupMessages.messages.push(value);
            }
          }
        };

        function _lookForCountry(destinationNumber) {
          var tempNumber = destinationNumber;
          tempNumber = tempNumber.replace("+", "");
          var result = self.countryList.filter(function( obj ) {
            return tempNumber.indexOf(obj.country_code) == 0;
          });

          return result[0];
        }

        function _lookForCountryToGetRate(countrySelect) {
          var result = self.countryList.filter(function( obj ) {
            return countrySelect.country_code == obj.country_code
                && countrySelect.iso == obj.iso;
          });
          if (result.length == 0) {
            return 0;
          }
          return result[0].rate;
        }

        self.sendMessage=function() {
          $ionicLoading.show();

          if (self.messageText == null) {
            $ionicLoading.notify(self.popup_translations.sms_no_message);
            return;
          }

          if (self.selectedCountry != null) {
            self.newMessage.iso=self.selectedCountry.iso;
            self.newMessage.country_code=self.selectedCountry.country_code;
            if (self.messageData.number != null) {
              self.newMessage.destination_number = self.messageData.number;
            } else {
              self.newMessage.destination_number = self.messageData.destination_number.toString().replace(self.selectedCountry.country_code, "");
            }
            self.newMessage.message = self.messageText;
          } else {

            if (self.messageData.destination_number == null) {
              $ionicLoading.notify(self.popup_translations.sms_no_phone_number);
              return;
            }

            self.selectedCountry=_lookForCountry(self.messageData.destination_number);

            if (self.selectedCountry==null) {
              $ionicLoading.notify(self.popup_translations.entered_number_invalid);
              return;
            }
            var number = self.messageData.destination_number.replace(self.selectedCountry.country_code, "");
console.log(number);
            self.newMessage.iso=self.selectedCountry.iso;
            self.newMessage.country_code=self.selectedCountry.country_code;
            self.newMessage.destination_number = number;
            self.newMessage.message = self.messageText;
          }

          //if (self.isFreshPage) { // Ask user add new contact before send SMS.
          //  ContactsService.getPhoneContacts().then(function(contacts) {
          //    $ionicLoading.hide();
          //    if (contacts == "none") {
          //      _displayRequestAddNewContact();
          //      return
          //    }
          //    var result = contacts.filter(function( obj ) {
          //      return self.newMessage.destination_number == obj.number
          //          && self.newMessage.iso == obj.contact_country_iso;
          //    });
          //    if (result.length == 0) {
          //      _displayRequestAddNewContact();
          //    } else {
          //      _sendSMS();
          //    }
          //
          //  }, function() {
          //    //if something goes Wrong
          //    $ionicLoading.hide();
          //  });
          //} else {
            _sendSMS();
          //}


        };

        function _displayRequestAddNewContact() {
          var dialog = DialogService.requestOption({
            saveText: self.popup_translations.yes_new_contact,
            cancelText: self.popup_translations.Cancel,
            template:
            self.popup_translations.not_found_number_in_contact +
            "<div class='item item-input full-width item-icon-left item-icon-right item-flag-input'>" +
              "<input class='contact-name-cap' autocomplete='off' autocorrect='off' autocapitalize='on' type='text' ng-model='msgsCtrl.newContactName' placeholder='{{\"Contact Name\" | translate}}'>" +
            "</div>"+
            "<div class='item item-input full-width item-icon-left item-icon-right item-flag-input' ng-show='msgsCtrl.selectedCountry.iso == \"CU\"'>" +
              "<input autocomplete='off' autocorrect='off' autocapitalize='off' type='email' ng-model='msgsCtrl.newContactEmail' placeholder='{{\"Contact_Email_Nauta\" | translate}}'>"+
            "</div>"
            ,
            title: self.popup_translations.hablax,
            cssClass: 'local-call-popup',
            onSaveCb: function(){
              self.newContact = {};
              self.newContact.contact_name=self.newContactName;
              self.newContact.contact_country_iso=self.selectedCountry.iso;
              self.newContact.country_code=self.selectedCountry.country_code; // additional field, API wont use this
              self.newContact.contact_email = self.newContactEmail;
              self.newContact.contact_phone="011-"+self.selectedCountry.country_code + "-" +self.newMessage.destination_number;

              if (self.newContact.contact_name == null || self.newContact.contact_name == "") {
                var popup = $ionicPopup.alert({
                  title: self.popup_translations.Information,
                  template: self.popup_translations.contact_name_must_be_filled
                });
                popup.then(function(res) {
                  _displayRequestAddNewContact();
                });
                return;
              }
              ContactsService.addContact(self.newContact).then(function(result) {
                _sendSMS();
              }, function(result) {
                //if something goes Wrong
                $ionicLoading.notify(self.popup_translations.contact_could_not_be_saved);
                $log.debug(result);
              });

            },
            onCancelCb: function(){
              $log.debug('no');
            },
            $scope: $scope
          });
        }

        function _sendSMS() {
          StoreService.setObject('messageCountry',self.selectedCountry);
          MessagesService.sendMessage(self.newMessage).then(function(resp) {



            ContactsService.getPhoneContacts().then(function(contacts) {
              $ionicLoading.hide();
              if (contacts === "none") {
                return
              }
              var result = contacts.filter(function( obj ) {
                return self.newMessage.destination_number == obj.number
                    && self.newMessage.iso == obj.contact_country_iso;
              });
              if (result.length > 0) {
                self.isFreshPage = false;
                self.messageData.show_name=result[0].contact_name;
                $rootScope.$broadcast("message_name_update", {show_name: self.messageData.show_name});
                $rootScope.$broadcast("message_name_show", {show: true});
                self.messageData.destination_number=result[0].destination;
              }

              self.messageText="";
              self.newMessage={};

            }, function() {
              //if something goes Wrong
              $ionicLoading.hide();
            });

            var value = {};
            value.message = resp.message;
            value.sent_at = resp.sent_at;
            _initGroupMessages(value);
            _groupMessages.messages.push(value);
            // self.selectedCountry=self.countryList[0];
            $ionicLoading.notify(self.popup_translations.message_sent_successfully);
            self.messageText="";
            self.newMessage={};
            $rootScope.$emit('updateBalance');
          }, function(resp) {
            $ionicLoading.hide();
            if(resp.error_code=="invalid_destination_length"){
              $ionicLoading.notify(self.popup_translations.destination_number_must_be);
            }
            else{
              $ionicLoading.notify(self.popup_translations.failed_to_send_message);

            }

          });
        }

        self.onControllerLoad=function() {
          switch (mode) {
            case 'sent':
              break;
            case 'write':
              self.getCountryList();
              self.searchHistory();
              break;
            case 'both':
              break;
            default:
              break;

          }
        };

        self.refreshList = function(item) {
          self.messagesList = [];
          self.destinationAPIIndex = 0;
          self.allDataLoaded = false;
          self.searchLastDestinations();
        };

        self.showConfirm = function(title,msg,onSuccess,onFailure) {
          var confirmPopup = $ionicPopup.confirm({
            title: title,
            template: msg
          });

          confirmPopup.then(function(res) {
            if(res) {
              $log.debug('You are sure');
              if(onSuccess){
                onSuccess();
              }
            } else {
              $log.debug('You are not sure');
              if(onFailure){
                onFailure();
              }
            }
          });
        };
        self.onControllerLoad();
      }]);

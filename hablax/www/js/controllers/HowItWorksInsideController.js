
hablaxControllers.controller('HowItWorksInsideController', function($ionicPopup, $state, $scope, $ionicLoading, $log, $translate, $scope,$localStorage,UserService, DialogService) {

  var vm = this;

  vm.goDetail = function(index) {

    var item = vm.features[Object.keys(vm.features)[index]];
    $state.go('how-it-works-inside-detail', { 'title': item.name, 'description': item.description });
  };
  //vm.features[index]
  vm.features = {
    'ion-ios-telephone-outline':  {'name': 'call_question', 'description': 'call_answer', 'actionIndex': 0},

    'ion-iphone':	{'name': 'recharge_question', 'description': 'recharge_answer', 'actionIndex': 1},
    'ion-email':	{'name': 'sms_question', 'description': 'sms_answer', 'actionIndex': 2},
    'ion-card':	{'name': 'add_fund_question', 'description': 'add_fund_answer', 'actionIndex': 3},
    'ion-trash-a':	{'name': 'delete_contact_question', 'description': 'delete_contact_answer', 'actionIndex': 4},
    'ion-android-textsms':	{'name': 'delete_sms_question', 'description': 'delete_sms_answer', 'actionIndex': 5},
    'ion-edit':	{'name': 'edit_contact_question', 'description': 'edit_contact_answer', 'actionIndex': 6}
  };



});


hablaxControllers.controller('HowItWorksController',function($scope,$state, $ionicSlideBoxDelegate,$ionicHistory,$localStorage,UserService, AuthService, CommonService, StoreService){

  $scope.options = {
    loop: false,
    speed: 300
  };
  $scope.activeIndex=0;
  //called on slide change
  $scope.slideHasChanged=function(index){
    $scope.activeIndex=index;
  };
  //called to navigate in between the slider
  $scope.next = function() {
    $ionicSlideBoxDelegate.next([600]);
  };
//state changed to home
  $scope.finish=function() {
    StoreService.set("hablaxIntroSeen",true);

    StoreService.get("goLogin",
        function (value) {
          if (value) {
            $state.go('login');
          } else {
            $state.go('validate-phone');
          }
        }, function (error) {
          $state.go('validate-phone');
        });

  };
// navigate to previous page
  $scope.goBack = function(){
    $ionicHistory.goBack();
  }
});

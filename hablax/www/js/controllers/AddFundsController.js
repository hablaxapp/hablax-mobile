hablaxControllers.controller('AddFundsController',['$log', '$rootScope','DialogService', '$scope','$state','$filter','$ionicPlatform','$ionicHistory','$ionicPopup','$ionicLoading','AddFundsService','$localStorage','$ionicScrollDelegate','RechargeService','UserService','PaypalService','$cordovaInAppBrowser', '$translate','NewsService','StoreService',
function($log, $rootScope, DialogService,$scope,$state,$filter,$ionicPlatform,$ionicHistory,$ionicPopup,$ionicLoading,AddFundsService,$localStorage,$ionicScrollDelegate,RechargeService,UserService,PaypalService,$cordovaInAppBrowser,$translate,NewsService,StoreService){

  var addfundsCtrl = this;
  addfundsCtrl.wizard={
   step:1
  };
  addfundsCtrl.selectUnchanged=true;
  addfundsCtrl.rates = [];
  addfundsCtrl.rateOn = 1;
  addfundsCtrl.amount = 0;
  addfundsCtrl.taxes = 0;
  addfundsCtrl.total = 0;

  addfundsCtrl.cardMonths = [{"month":1},{"month":2},{"month":3},{"month":4},{"month":5},{"month":6},{"month":7},{"month":8},{"month":9},{"month":10},{"month":11},{"month":12}];
  addfundsCtrl.cardYears = [{"year":2016},{"year":2017},{"year":2018},{"year":2019},{"year":2020},{"year":2021},{"year":2022},{"year":2023},{"year":2024},{"year":2025},{"year":2026},{"year":2027},{"year":2028},{"year":2029},{"year":2030}];
  addfundsCtrl.card = {
    show: false,
    number: "",
    // month: "",
    // year: "",
    cvc: "",
    remember:true,
    postalCode: "",
    mode: "",
    reset: function(hide){
      if(hide){
        this.show = false;
      }
      this.month = "";
      this.year = "";
      this.number = "";
      this.postalCode = "";
      this.cvc = "";
    }
  };

  addfundsCtrl.hideNotify = true;
  addfundsCtrl.notifyMessage = null;

  addfundsCtrl.paypalReady ={
    show: false
  };

  addfundsCtrl.hablaxBalance = UserService.balance;
  // navigate to previous page
  addfundsCtrl.goBack = function(){
    if(addfundsCtrl.wizard.step = 2){

      addfundsCtrl.wizard.step = 1;
      addfundsCtrl.card.show=false;
      addfundsCtrl.paypalReady.show=false;
    } else {
      $ionicHistory.goBack();
    }
  };

  addfundsCtrl.popup_translations={};
  function _translate() {
    $translate(['close', 'add_more_funds', 'what_to_do', 'payment_successful', 'cent', 'Tax_exempt', 'error']).then(function (translations) {
      addfundsCtrl.popup_translations=translations;
      console.log(addfundsCtrl.popup_translations);

    }, function (translationIds) {
      addfundsCtrl.popup_translations=translationIds;
      console.log(addfundsCtrl.popup_translations);
    });
  }
  _translate();
  $scope.$on("languageChanged",function(e,args){
    _translate();
  });

  function _getNews(lang, news) {
    if (lang == "en") {
      addfundsCtrl.notifyMessage = news.notice.notice.en;
    } else if (lang == "es") {
      addfundsCtrl.notifyMessage = news.notice.notice.es;
    }
  }

  NewsService.getNews('funds').then(function (news) {
    $log.debug(news);
    if (news.status != 'error') {
      addfundsCtrl.hideNotify = false;
      StoreService.get("hablax_lang",
          function (value) {
            if (value) {
              var language=value;
              _getNews(language, news);
            } else {
              _getNews("en", news);
            }
          }, function (error) {
            _getNews("en", news);
          });

    }
  }, function (resp) {
    //$ionicLoading.notify('Failed to get news');
  });

  $rootScope.$on('updateBalance', function(e, event){
    addfundsCtrl.updateBalance(event);
  });
  addfundsCtrl.updateBalance = function(balance){
    if (balance != null) {
      addfundsCtrl.hablaxBalance = balance;
    } else {
      UserService.getBalance().then(function (res) {
        addfundsCtrl.hablaxBalance = UserService.balance;
      });
    }
  };
    addfundsCtrl.updateBalance();

  AddFundsService.getFundsAmounts().then(function(data){

    if(data.status == "success") {
      $log.debug(data);
      addfundsCtrl.rates = data.amounts.length ? data.amounts : null;
    }

  },function(){});

  addfundsCtrl.scrollToTop = function() {
    $ionicScrollDelegate.$getByHandle('addfundsScroll').scrollTop(true);
  };
  addfundsCtrl.showTaxHelp = function(){
    // $ionicPopup.alert({
    //      title: 'TAX/VAT',
    //      template: '35c over sales total.'
    //    });
    DialogService.alert("", addfundsCtrl.popup_translations.Tax_exempt, angular.noop,$scope,'local-call-popup custom-popup popup-body-center','info');
  };

  addfundsCtrl.payWithPayPal = function(){
    addfundsCtrl.wizard.step=2;
    $ionicLoading.show();

    PaypalService.initPaymentUI().then(function () {
      PaypalService.makePayment(addfundsCtrl.paypalTotal, "Total Amount").then(function (response) {
        $log.debug(JSON.stringify(response));
        AddFundsService.payByPayPal({paypal_transaction_id: response.response.id, amount: addfundsCtrl.rateOn.amount}).then(function(){
          // $ionicLoading.notify("Transaction completed successfuly");
          // $ionicLoading.hide();
          // vm.choiceAfterPayment();
          addfundsCtrl.choiceAfterPayment();
          $rootScope.$emit('updateBalance');
          $ionicLoading.hide();
        },function(err){
          $ionicLoading.notify(err,2000);
        });
      }, function (error) {
        $ionicLoading.notify(error,2000);
      });
    });

    // PaypalService.initPaymentUI().then(function () {
    //
    //   PaypalService.makePayment(addfundsCtrl.total, "Total Amount").then(function (response) {
    //
    //   alert("success"+JSON.stringify(response));
    //   $log.debug(JSON.stringify(response));
    //   }, function (error) {
    //
    //   alert("Transaction Canceled");
    //
    //   });
    //
    // });
  };

  addfundsCtrl.payWithCard = function(){
    addfundsCtrl.card.show = true;
    addfundsCtrl.scrollToTop();
    addfundsCtrl.wizard.step=2;
  };

  addfundsCtrl.rateSelected = function(rateOn){
    $log.debug(addfundsCtrl.rateOn);
    //addfundsCtrl.rateOn = addfundsCtrl.rates[index].amount;
    addfundsCtrl.amount = addfundsCtrl.rateOn.amount;
    addfundsCtrl.taxes = addfundsCtrl.rateOn.tax;
    addfundsCtrl.total = addfundsCtrl.rateOn.price;
    addfundsCtrl.selectUnchanged=false;
  };
  function _resetSteps() {
    addfundsCtrl.rateOn = null;
    addfundsCtrl.card.show = false;
    addfundsCtrl.wizard.step=1;
    addfundsCtrl.selectUnchanged=true;
    addfundsCtrl.amount = 0;
    addfundsCtrl.taxes = 0;
    addfundsCtrl.total = 0;
  }

  function _createPayPalParams(){
    var params = {};
    var recharges = [];
    for(var i=0,l=vm.cart.items.length; i<l; i++){
      var str = "";
      var obj = vm.cart.items[i];
      str += obj.destination + "|";
      str += obj.serviceType + "|";
      str += obj.validate_in + "|";
      str += obj.product + "|";
      str += obj.recharge_id + "|";
      str += obj.country_id;
      recharges.push(str);
    }

    params.recharges = recharges;
    return params;
  }
  function _createCardParams() {
    var params = {};
    if(addfundsCtrl.card.mode=="new"){
      params.card_number = addfundsCtrl.card.number;
      params.exp_month = addfundsCtrl.card.month.month;
      params.exp_year = addfundsCtrl.card.year.year;
      params.cvv = addfundsCtrl.card.cvc;
      params.zip = addfundsCtrl.card.postalCode;
      params.save_card = addfundsCtrl.card.remember;
      params.card_option = "new";
    }
    else{
      params.card_number = addfundsCtrl.card.selectedCard.customerPaymentProfileId["0"];
      params.card_option = "saved";
    }
    params.amount= addfundsCtrl.amount;
    return params;

  }
  addfundsCtrl.choiceAfterPayment = function(){
    $ionicPopup.show({
      // template: '<input type="password" ng-model="data.wifi">',
      title: addfundsCtrl.popup_translations.payment_successful,
      subTitle: addfundsCtrl.popup_translations.what_to_do,
      scope: $scope,
      buttons: [
        {
          text: addfundsCtrl.popup_translations.add_more_funds,
          type: 'button-energized',
          onTap: function(e) {
            _resetSteps();
            addfundsCtrl.card.reset(true);
          }
        },
        {
          text: addfundsCtrl.popup_translations.close,
          type: 'button-balanced',
          onTap: function(e) {
            $state.transitionTo('pay-history');
          }
        }
      ]
    });
  };
  addfundsCtrl.makeCardPayment= function(){
    $ionicLoading.show();
    var params = _createCardParams();
    $log.debug(params);
    // params.sandbox = true;

    AddFundsService.payByCard(params).then(function(res){
      $ionicLoading.hide();
      addfundsCtrl.choiceAfterPayment();
      var data = {
        balance: res.balance,
        currency: res.currency
      };

      $rootScope.$emit("updateBalance", data);
    },function(err){ // multi-language
      $ionicLoading.hide();
      DialogService.alert("", err, angular.noop,$scope,'local-call-popup custom-popup popup-body-center','error');
    });
  };
  // addfundsCtrl.openHelp = function(){
  //   var options = {
  //     location: 'yes',
  //     clearcache: 'yes'
  //   };
  //
  //   $cordovaInAppBrowser.open('https://www.hablax.com/app/help', '_self', options)
  //     .then(function(event) {
  //       // success
  //     })
  //     .catch(function(event) {
  //       // error
  //     });
  // }

  addfundsCtrl.help = function(){
    // $ionicPopup.alert({
    //      title: 'TAX/VAT',
    //      template: '35c over sales total.'
    //    });
    DialogService.alert("", addfundsCtrl.popup_translations.Tax_exempt, angular.noop,$scope,'local-call-popup custom-popup popup-body-center','info');
  };
  addfundsCtrl.paypalHelp = function(){
    DialogService.alert("", "2.9% + USD 0.30 "+addfundsCtrl.popup_translations.cent, angular.noop,$scope,'local-call-popup custom-popup popup-body-center','info');
  };

  addfundsCtrl.choosePaypal = function() {
    addfundsCtrl.paypalReady.show = true;
    addfundsCtrl.wizard.step=2;
    addfundsCtrl.scrollToTop();

    RechargeService.calculatePayPalFee({amount:addfundsCtrl.amount}).then(function(res){
      $log.debug(res);

      addfundsCtrl.paypalfee = res.paypal_fee;
      addfundsCtrl.paypalTotal = res.paypal_fee + addfundsCtrl.total;


    },function(err){
      $ionicLoading.notify(err);
    });
  };

  $scope.$on('$cordovaInAppBrowser:exit', function(e, event){
    $cordovaInAppBrowser.close();
  });


}]);


hablaxControllers.controller('WhoWeAreController', function($ionicPopup, $state, $scope, $ionicLoading, $log, $translate, $scope,$localStorage,UserService, DialogService) {

  var vm = this;

  vm.goDetail = function(index) {
    var key = Object.keys(vm.phoneContacts)[index];
    var item = vm.phoneContacts[key];
    $state.go('who-we-are-detail', { 'title': key, 'description': item.description });
  };

  vm.phoneContacts = {
    'FIDEL GARCIA':	{'avatar': '', 'role': 'role_fidel_garcia', 'description': 'cv_fidel_garcia', 'actionIndex': 0},
    'CARLOS RIVERA': {'avatar': '', 'role': 'role_carlos_rivera', 'description': 'cv_carlos_rivera', 'actionIndex': 1},
    'JEAN SCHMILINSKY':	{'avatar': '', 'role': 'role_jean_schmilinsky', 'description': 'cv_jean_schmilinsky', 'actionIndex': 2},
    'DOMINGO FALERO':	{'avatar': '', 'role': 'role_domingo_falero', 'description': 'cv_domingo_falero', 'actionIndex': 3}
  };



});

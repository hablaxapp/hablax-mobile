hablaxControllers.controller('ContactsController',
 ['$timeout','MessagesService','CallService','DialogService','$log', '$state','$ionicActionSheet', '$scope', '$ionicHistory', '$rootScope', '$ionicPopup','$ionicPlatform', 'ContactsService', '$ionicLoading','type','$localStorage','$translate','UserService', '$ionicListDelegate',
function($timeout, MessagesService,CallService,DialogService,$log, $state, $ionicActionSheet, $scope, $ionicHistory, $rootScope, $ionicPopup, $ionicPlatform, ContactsService, $ionicLoading, type,$localStorage,$translate,UserService, $ionicListDelegate) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  var self = this;
  self.searchKey = "";
  self.fullContacts = [];
  self.contacts = []; // We will use it to load contacts
  self.addedContacts = [];
  self.newContact = { // We will use it to save a contact
    "contact_name": "alpha alpha alpha",
    "contact_phone":"01192775787",
    "contact_country_iso":"en"
  };
  self.dialCountry={};
  self.deleteRowId="";
  // navigate to previous page
  self.goBack = function(){
    $ionicHistory.goBack();
  };
  self.contact={};
  self.choices={};
  self.popup_translations={};
  self.notShowAgain = false;

  function _translate() {
      $translate(['message_sent_successfully', 'Cancel', 'Send_Message', 'Call_Contact', 'Recharge_Number',
                'Recharge_Phone', 'Recharge_Email', 'confirm_delete', 'sure_to_delete','Information', 'Choose_action',
                'contact_deleted','not_available', 'local_call_not_available', 'contact_deleted', 'failed_delete',
                'Delete_Contact','Edit_Contact','not_show_again','llamada_local']).then(function (translations) {
          self.popup_translations=translations;
          console.log(self.popup_translations);
      }, function (translationIds) {
          self.popup_translations=translationIds;
          console.log(self.popup_translations);
      });
  }
    _translate();
    $scope.$on("languageChanged",function(e,args){
        _translate();
    });


  self.showConfirm = function(title,msg,onSuccess,onFailure) {
   var confirmPopup = $ionicPopup.confirm({
     title: title,
     template: msg
   });
   confirmPopup.then(function(res) {
     if(res) {
       $log.debug('You are sure');
       if(onSuccess){
         onSuccess();
       }
     } else {
       $log.debug('You are not sure');
       if(onFailure){
         onFailure();
       }
     }
   });
 };

    self.onSearchChanged = function() {
        if (self.searchKey == null || self.searchKey === "") {
            self.contacts = self.fullContacts;
            return;
        }
        var keyForSearch = self.searchKey.toLowerCase();
        self.contacts=self.fullContacts.filter(function(el) {
            return (el.contact_phone.toLowerCase().indexOf(keyForSearch) > -1)
                || (el.contact_name.toLowerCase().indexOf(keyForSearch) > -1)
        });
    };



    self.showAlert = function() {
        self.notShowAgain = CallService.nowShowAgain;
        if (self.notShowAgain) {
            self.localCall();
            return;
        }
        var alertPopup = $ionicPopup.alert({
            title: '<div class="info-icon-popup"><i class="icon ion-ios-information-outline"></i></div>',
            template: '<div>' +
            self.popup_translations.llamada_local +'<br/>' +
            '<input type="checkbox" class="not-again" ng-model="contactsCtrl.notShowAgain" ng-checked="dialCtrl.notShowAgain" ng-true-value="true" ng-false-value="false"/>'+
            '<span ng-click="contactsCtrl.notShowAgain = !contactsCtrl.notShowAgain">' + self.popup_translations.not_show_again + '</span><br>' +
            '</div>',
            scope: $scope,
            cssClass:'local-call-popup custom-popup'
        });

        alertPopup.then(function(res) {
            CallService.nowShowAgain = self.notShowAgain;
            self.localCall();
        });
    };

 self.showContactActions = function(contact, contactIndex){
   self.contact=contact;
   console.log(contact);
     var actionSheetButtons = [
       { text: '<i class="as-icon call-btn icon ion-ios-telephone-outline"></i> ' + self.popup_translations.Call_Contact + ''},
       { text: '<i class="as-icon message-btn icon ion-paper-airplane"></i> ' + self.popup_translations.Send_Message + '' },
       { text: '<i class="as-icon recharge-btn icon ion-android-phone-portrait"></i> ' + self.popup_translations.Recharge_Phone + ''},
     ];

     if(contact.contact_email){
       actionSheetButtons.push({text: '<i class="as-icon recharge-btn icon ion-ios-email"></i>' + self.popup_translations.Recharge_Email + ''});
     }
     actionSheetButtons.push({ text: '<i class="as-icon recharge-btn icon ion-edit"></i> ' + self.popup_translations.Edit_Contact + ''});
     actionSheetButtons.push({ text: '<i class="as-icon delete-btn icon ion-ios-trash-outline"></i> ' + self.popup_translations.Delete_Contact + ''});
     var hideSheet = $ionicActionSheet.show({
       buttons: actionSheetButtons,
       //  destructiveText: 'Delete',
       //titleText: self.popup_translations.Choose_action,
       cancelText: self.popup_translations.Cancel,
       cancel: function() {
         // add cancel code..
       },
       buttonClicked: function(index) {
           var order = 0;
         if(index==order++){
           if(self.contact.contact_phone.length>=9){
             $ionicLoading.show();
             CallService.verifyNumberForCall(self.contact.contact_phone).then(function(res){
               $ionicLoading.hide();
               $timeout(function(){
                   self.showAlert();
                 //self.localCall();
                 //self.showChoices();
               });
               $log.debug(res);
               self.dialCountry = res;
               console.log(res);
               self.selectedCountry = null;
               for(var i=0,l=self.countryList.length; i<l ; i++){
                 if(self.dialCountry.country_code && self.countryList[i].country_code == self.dialCountry.country_code && self.countryList[i].iso == self.dialCountry.iso){
                   self.selectedCountry = self.countryList[i];
                   break;
                 }
               }
             },function(err){
               $ionicLoading.hide();
               $timeout(function(){
                 self.showAlert();
                 //self.localCall();
                 //self.showChoices();
               });
               $log.debug(err);
               self.dialCountry = null;
               self.selectedCountry = null;
             })
           }
           else{
             self.dialCountry = null;
             self.selectedCountry = null;
             $timeout(function(){
               self.showAlert();
               //self.localCall();
               //self.showChoices();
             });
           }


         }
         else if(index == order++){
           $state.transitionTo("tab.messages.write-message",{contact:contact});
         }
         else if(index == order++){
           $state.transitionTo("tab.recharge",{contact:contact, rechargeType: 'mobile'});

         } else if(contact.contact_email && index == order++){
             $state.transitionTo("tab.recharge",{contact:contact, rechargeType: 'nauta'});
         }
         else if(index == order++){
             self.editContact(contact);
         }
         else if(index == order++){
             self.deleteContact(contactIndex);
         }
         return true;
       }
     });



 };
  self.showChoices = function(){
      self.choices.show = true;
  };
  self.hideChoices = function(){
    // $timeout(function(){
    self.choices.show = false;
    // },800);
  };
  MessagesService.getCountryList().then(function(countries) {
    self.countryList=countries;
    // vm.selectedCountry=vm.countryList[0];
    $log.debug(self.countryList);
  }, function(resp) {
  //  alert('Failed to get sms Country List');
  });
  self.makeCall=function () {
    $state.transitionTo("tab.dial",{contact: self.contact});

  };
  self.localCall = function(){
    var user = UserService.userData;
    console.log(user);
    // return;
    $ionicLoading.show();
    CallService.getAccessNumbers(self.contact_phone).then(function(res){
      $ionicLoading.hide();
      self.accessNumbers = res;
      var num = null;
      for(var i=0,l=self.accessNumbers.length; i<l; i++){
        if(self.accessNumbers[i].country_code == user.country_code && self.accessNumbers[i].iso == user.iso){
          num = self.accessNumbers[i];
          break;
        }
      }
      if(!num){
        DialogService.alert(self.popup_translations.not_available,self.popup_translations.local_call_not_available,angular.noop,$scope,'local-call-popup custom-popup','info');
      }
      else{
        CallService.saveCallOnServer(self.contact.contact_phone, function(q){
            q.then(function(res){
                $ionicLoading.hide();
                window.plugins.CallNumber.callNumber(self.onSuccess, self.onError, num.phone, false);
            },function(err){
                $ionicLoading.hide();
            })
        });
      }

      //
      // if(!self.dialCountry){
      //   DialogService.alert("Not available","Local call is not available for the dialed number",angular.noop,$scope,'local-call-popup');
      // }
      // else{
      //   var num = self.dialNumber.replace("011","");
      //   if(self.dialCountry.country_code){
      //     num = num.replace(self.dialCountry.country_code,"");
      //   }
      //   window.open("tel:" + num);
      // }


      // DialogService.alert("Select Country",_accessNumberString,function(res){
      //   $ionicLoading.show();
      //   CallService.saveCallOnServer(self.dialNumber).then(function(res){
      //     $ionicLoading.hide();
      //     window.open("tel:" + self.selectedAccessNumber.phone);
      //   },function(err){
      //     $ionicLoading.hide();
      //   });
      // },$scope,'local-call-popup');
    },function(err){
      $ionicLoading.hide();
    });
  };

  self.onSuccess = function(result){
    console.log("Success:"+result);
  };

  self.onError = function(result) {
    console.log("Error:"+result);
  };

  $translate(['HELP', 'Local Number', 'Help_Button_Wifi',  'recharge_popup_subtitle', 'Help_Button_Local_Content','Help_Button_Wifi_Content']).then(function (translations) {
    self.translations=translations;
  }, function (translationIds) {
    self.translations=translationIds;
  });


  // some settings for contact list
  self.listCanSwipe = true;

  // //hablax contacts
  // self.remove = function(chat) {
  //   Chats.remove(chat);
  // };

  //function to add a contact
  self.addContact = function() {
      $rootScope.$broadcast("addNewContact");
  };

  //function to edit contacts
  self.editContact = function(contact,$event) {
      if ($event) {
          $event.stopImmediatePropagation();
      }
      $ionicListDelegate.closeOptionButtons();
      $rootScope.$broadcast("editContact",{editContact:contact});
  };

  self.removeConstantNumber = function(number) {
      return number.replace("011", "");
  };

  //function to get all contacts
  self.getAllContacts = function(refresher) {
    if(!refresher) $ionicLoading.show();
    ContactsService.getPhoneContacts().then(function(contacts) {
      $ionicLoading.hide();
      if (contacts == "none") {
          return
      }
      self.contacts=contacts;
      self.fullContacts = contacts;
      $log.debug(self.contacts);

      $scope.$broadcast('scroll.refreshComplete');
    }, function() {
      //if something goes Wrong
      $ionicLoading.hide();
      $scope.$broadcast('scroll.refreshComplete');
    });
  };

  //on contact swipe Up
  self.onContactTap = function(currentContact) {
    var currState = angular.copy(currentContact.showOptions);
    if(self.contacts!='none'){
          self.contacts.forEach(function(contact, index, contacts){
            contact.showOptions = false;
          });
    }
    currentContact.showOptions = !currState;
  };

  //close options on every time load of controller  self.contacts.forEach(closeOptions);
  self.listOptionsClose = function() {
    // self.chats.forEach(closeOptions);
    self.contacts.forEach(closeOptions);
  };

  //all options close
  function closeOptions(element, index, array) {
    element.showOptions = false;
  }

  self.deleteContact = function(index,$event) {
    if ($event) {
      $event.stopImmediatePropagation();
    }
    $ionicListDelegate.closeOptionButtons();
    self.showConfirm(self.popup_translations.confirm_delete,self.popup_translations.sure_to_delete,function(){
      self.deleteRowId = self.contacts[index].row_id;
      $log.debug(self.deleteRowId,index);
      $ionicLoading.show();
      ContactsService.deleteContact({row_id : self.deleteRowId}).then(function(resp) {
        self.contacts.splice(index,1);
        $ionicLoading.notify(self.popup_translations.contact_deleted);
      }, function(resp) {
        $ionicLoading.notify(self.popup_translations.failed_delete);
      });
    },function(){

    });
  };
  self.updateList = function(addedContact) {
    var newContact = {};
    newContact=addedContact[0];
    //sort by name
      self.contacts.unshift(newContact);
      self.contacts.sort(function(a, b) {
      if (a.contact_name > b.contact_name) {
        return 1;
      } else if (a.contact_name < b.contact_name) {
        return -1;
      }
      // a must be equal to b
      return 0;
});

  };

  //load data according to views

  $rootScope.$on("contact-event: edited",function(e,args){
    var contact = args.contact;
    for(var i=0,l=self.contacts.length; i<l; i++){
      if(contact.row_id == self.contacts[i].row_id){
        ContactsService.processContacts([contact], function(procContactsResult) {
            angular.merge(self.contacts[i],procContactsResult.contacts[0]);
        });

        break;
      }
    }
  });

  $rootScope.$on("contact-event: added",function(e,args){
    var contact = args.contact;
    self.contacts.push(contact);
    self.contacts = ContactsService.sortContacts(self.contacts);
  });
  self.getAllContacts(false);

}]);

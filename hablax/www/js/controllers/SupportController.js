
hablaxControllers.controller('SupportController', function($ionicPopup,$ionicHistory, $scope, $state, $ionicLoading, $log, $translate, $localStorage,UserService, SupportService, DialogService) {

  var vm = this;

  vm.phoneContacts = [];

  vm.params = {
    subject:"",
    message: ""
  };

  vm.translations={};
  $translate(['email_sent', 'email_error_sent', 'enter_subject_message']).then(function (translations) {
    vm.translations=translations;
    console.log(vm.translations);
  }, function (translationIds) {
    vm.translations=translationIds;
    console.log(vm.translations);
  });

  vm.sendEmailTemplate = "<div class='send-email-container list'>" +
    "<div class='input-label'>Subject</div>" +
    "<div class='item item-input'>" +
      "<input type='text' placeholder='' ng-model='supportCtrl.params.subject'>" +
    "</div>" +
    "<div class='input-label'>Message</div>" +
    "<div class='item item-input'>" +
      "<textarea class='input' placeholder='' ng-model='supportCtrl.params.message'></textarea>" +
    "</div>" +
  "</div>";

  vm.callNumber = function(phone) {
    window.plugins.CallNumber.callNumber(vm.onSuccess, vm.onError, phone+"", false);
  };

  vm.onSuccess = function(result){
    console.log("Success:"+result);
  };

  vm.onError = function(result) {
    console.log("Error:"+result);
  };

  vm.getAccessNumbers = function() {
    $ionicLoading.show();
    SupportService.getAccessNumbers().then(function(data){
      vm.phoneContacts = data.numbers;
      $ionicLoading.hide();
    },function(res){
      $ionicLoading.hide();
    });
  };

  vm.getAccessNumbers();

  vm.goBack = function(){
    $ionicHistory.goBack();
  }

  vm.sendEmail = function(){
    $state.go("send-email");

    //var options = {
    //  title: "Send Email",
    //  template:vm.sendEmailTemplate,
    //  scope: $scope,
    //  buttons: [
    //    {
    //      text: "Cancel",
    //      onTap: function(e){
    //        return false;
    //      }
    //    },
    //    {
    //      text: '<b>Send</b>',
    //      type: 'button-positive',
    //      onTap: function(e) {
    //        if(!vm.params.subject || !vm.params.message){
    //          e.preventDefault();
    //          $ionicLoading.notify(vm.translations.enter_subject_message);
    //          return;
    //        }
    //        $ionicLoading.show();
    //        UserService.sendEmailToSupport(vm.params).then(function(){
    //          $ionicLoading.notify(vm.translations.email_sent);
    //        },function(res){
    //          $ionicLoading.notify(vm.translations.email_error_sent);
    //        });
    //        return false;
    //      }
    //    }
    //  ]
    //};
    //vm.passwordRecoveryPopup = $ionicPopup.show(options);
  }


});

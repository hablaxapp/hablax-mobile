/** HomeController*/
hablaxControllers.controller('ForceUpdateController', function(CommonService, $translate, $log) {

    $log.debug("inside force update controller");
    var vm=this;

    vm.popup_translations={};
    function _translate() {
        $translate(['update_now', 'must_update_latest', 'HELP']).then(function (translations) {
            vm.popup_translations=translations;
            console.log(vm.popup_translations);
        }, function (translationIds) {
            vm.popup_translations=translationIds;
            console.log(vm.popup_translations);
        });
    }

    _translate();


    vm.updateApp = function() {
        CommonService.updateApp();
    };

    vm.showForceUpdate = true;








});

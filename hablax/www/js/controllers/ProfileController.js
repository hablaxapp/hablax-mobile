
hablaxControllers.controller('ProfileController',
['$log','$scope', 'AuthService', '$state', 'UserService', '$ionicHistory', 'userProfile','$localStorage', '$ionicLoading', 'DialogService','$translate','NewsService'
,function($log, $scope, AuthService, $state, UserService, $ionicHistory, userProfile,$localStorage, $ionicLoading, DialogService, $translate, NewsService) {
  // Login view's bindings go here.

  $log.debug("inside profile controller");

  var vm = this;
  vm.user = angular.copy(userProfile);

  vm.currencies = ["EUR","USD","CAD","BRL"];

  vm.hideNotify = true;
  vm.notifyMessage = null;

  vm.popup_translations = {};
  function _translate() {
    $translate(['Change_password','Please_enter_password','Please_retype_password','Passwords_do_not_match','you_sure_delete_this_card','confirm_delete',
        'card_deleted','profile_updated','no_change_to_update']).then(function (translations) {
      vm.popup_translations=translations;
    }, function (translationIds) {
      vm.popup_translations=translationIds;
    });
  }
  _translate();
  $scope.$on("languageChanged",function(e,args){
    _translate();
  });

  // navigate to previous page
  vm.goBack = function(){
    $ionicHistory.goBack();
  };

  var _changePasswordTemplate = '<label class="item item-input item-icon-left item-stacked-label">';
  _changePasswordTemplate    +=   '<span class="input-label text_uppercase">{{\'New_password\' | translate}}</span>';
  _changePasswordTemplate    +=   '<input type="password" placeholder="{{\'Type_your_password\' | translate}}" ng-model="profileCtrl.popupParams.newPass">';
  _changePasswordTemplate    += '</label> ';
  _changePasswordTemplate    += '<label class="item item-input item-icon-left item-stacked-label">';
  _changePasswordTemplate    +=   '<span class="input-label text_uppercase">{{\'Confirm_password\' | translate}}</span>';
  _changePasswordTemplate    +=   '<input type="password" placeholder="{{\'Re_type_password\' | translate}}" ng-model="profileCtrl.popupParams.confirmPass">';
  _changePasswordTemplate    += '</label> ';

  vm.deleteCard = function(card){
    DialogService.confirm(vm.popup_translations.confirm_delete,vm.popup_translations.you_sure_delete_this_card,function(){
      UserService.deleteCard({card_number:card.customerPaymentProfileId[0]}).then(function(){
        $ionicLoading.notify(vm.popup_translations.card_deleted);
        vm.user.anet.payment_profiles.splice(vm.user.anet.payment_profiles.indexOf(card),1);
      },function(e){
        $ionicLoading.notify(e);
        vm.user.anet.payment_profiles.splice(vm.user.anet.payment_profiles.indexOf(card),1);
      });

    },function(){
      // nothing
    });
  };




  function _getNews(lang, news) {
    if (lang == "en") {
      vm.notifyMessage = news.notice.notice.en;
    } else if (lang == "es") {
      vm.notifyMessage = news.notice.notice.es;
    }
  }

  NewsService.getNews('profile').then(function (news) {
    $log.debug(news);
    if (news.status != 'error') {
      vm.hideNotify = false;
      StoreService.get("hablax_lang",
          function (value) {
            if (value) {
              var language=value;
              _getNews(language, news);
            } else {
              _getNews("en", news);
            }
          }, function (error) {
            _getNews("en", news);
          });

    }
  }, function (resp) {
    //$ionicLoading.notify('Failed to get news');
  });


  vm.onChangePasswordClick = function(){
    vm.popupParams = {
      newPass: '',
      confirmPass: ''
    };
    DialogService.show({
      template: _changePasswordTemplate,
      title: vm.popup_translations.Change_password,
      onSaveCb: function(){
        if(!vm.popupParams.newPass){
          DialogService.alert(vm.popup_translations.Please_enter_password);
        }
        else if(!vm.popupParams.confirmPass){
          DialogService.alert(vm.popup_translations.Please_retype_password);
        }
        else if(vm.popupParams.newPass!=vm.popupParams.confirmPass){
          DialogService.alert(vm.popup_translations.Passwords_do_not_match);
        }
        else{
          $log.debug('yes');
          vm.user.password = angular.copy(vm.popupParams.newPass);
          vm.updateProfile();
        }
      },
      onCancelCb: function(){
        $log.debug('no');
      },
      $scope: $scope
    });
  };

  vm.updateProfile = function(){
    $log.debug(vm.user);
    // token, first_name, last_name, currency, email, password (optional), telephone_2 (optional)

    var params = {};
    params.first_name = vm.user.first_name;
    params.last_name = vm.user.last_name;
    params.currency = vm.user.currency;
    params.email = vm.user.email;
    if(vm.user.password){
      params.password = vm.user.password;
    }
    if(vm.user.telephone_2 != null){
      params.telephone_2 = vm.user.telephone_2;
    }
    $ionicLoading.show();
    UserService.updateProfile(params).then(function(res){
      $log.debug(res);
      $ionicLoading.notify(vm.popup_translations.profile_updated);
      vm.user = angular.copy(res);
      $scope.$emit("updateBalance");
    },function(err){
      $ionicLoading.hide();
      if(err.error_code == "no_valid_changes_were_found"){
        $ionicLoading.notify(vm.popup_translations.no_change_to_update);
      }
      $log.debug(err);
    });
  }
}]);

hablaxControllers.controller('LoginController', ['$cordovaInAppBrowser','$scope', 'AuthService', '$log', '$state', 'UserService', '$ionicHistory', '$ionicLoading', 'DialogService', '$translate', '$ionicPopup', 'CommonService', 'StoreService', '$ionicModal','$ionicScrollDelegate', 'PushService',
  function ($cordovaInAppBrowser, $scope, AuthService, $log, $state, UserService, $ionicHistory, $ionicLoading, DialogService, $translate, $ionicPopup, CommonService, StoreService, $ionicModal, $ionicScrollDelegate, PushService) {

    // Login view's bindings go here.
    $log.debug("inside login controller");

    var vm = this;
    vm.User = {
      userName: '',
      password: '',
      remember: true
    };

    //vm.User = {
    //  userName: 'app@hablax.com',
    //  password: 'hablaxapp',
    //  remember: true
    //};


    vm.allDataLoaded = false;
    vm.showError= true;
    vm.showErrorUsername = false;
    vm.showErrorPassword = false;
    vm.recoveryEmail = "";
    vm.recoveryNumber = "";
    vm.blockShow = 20;

    vm.translations = {};

    function _translate() {
      $translate(['Recover_Password_Content', 'Recover_Email', 'Recover_Password', 'Cancel', 'Get_It_Back', 'Recover_Password_Content', 'What_Country_Do_You_Live',
        'please_select_country', 'please_enter_number','code_sent_to_your_number','phone_number_not_found','could_not_send_code_to_number','please_enter_email',
        'password_sent_to_email','could_not_send_password_to_email','invalid_username_or_password_please_retry','HELP','internet_unreachable',
        'your_email_is','account_restricted_message','warning','hablax','How_do_info1']).then(function (translations) {
        vm.translations = translations;
      }, function (translationIds) {
        vm.translations = translationIds;
      });

      StoreService.get("hablax_lang",
          function (value) {
            if (value) {
              vm.langCode = value;
            } else {
              vm.langCode = "en";
            }
          }, function (error) {
            vm.langCode = "en";
          });
    }
    _translate();
    $scope.$on("languageChanged",function(e,args){
      _translate();
    });

    vm.customerSupport = function(){
      var options = {
        location: 'yes',
        clearcache: 'yes'
      };

      var isIOS = ionic.Platform.isIOS();
      var target = isIOS ?  "_system" : "_self";

      $cordovaInAppBrowser.open('https://www.hablax.com/login#contact', target, options)
          .then(function(event) {
            // success
          })
          .catch(function(event) {
            // error
          });
    };

    vm.stopShowEmailError = function() {
      vm.showErrorUsername = false;
    };

    vm.onUsernameChanged = function() {
      vm.showErrorUsername = true;
    };

    vm.onPasswordChanged = function() {
      vm.showErrorPassword = true;
    };

    $scope.$on('$cordovaInAppBrowser:exit', function(e, event){
      $cordovaInAppBrowser.close();
    });

    $scope.$on('$destroy',function(){
      if(vm.emailRecoveryPopup){
        vm.emailRecoveryPopup.close();
      }
      if(vm.passwordRecoveryPopup){
        vm.passwordRecoveryPopup.close();
      }
    });

    vm.showSelectCountryModal = function() {
      //vm.recoverEmail();
      //vm.modal.hide();
    };

    vm.closeSelectCountryModal = function() {
      vm.recoverEmail();
      vm.modal.hide();
    };

    vm.closeEmailRecoveryPopup = function() {
      vm.emailRecoveryPopup.close();
    };

    vm.loadMore = function() {
      if (vm.countryList==null) {
        AuthService.getRegistrationCountries().then(function(countries){
          vm.countryList = countries;
          vm.searchCountryList = countries;
          vm.currentCountryList = countries.slice(0, vm.blockShow);
        },function(err){
          $log.debug(err);
        });
      } else {
        vm.currentCountryList = vm.searchCountryList
            .slice(0, vm.currentCountryList.length + vm.blockShow);
      }

      $scope.$broadcast('scroll.infiniteScrollComplete');
      $scope.$broadcast('scroll.refreshComplete');
    };

    vm.selectCountry = function(index) {
      vm.countrySelected = vm.searchCountryList[index];
      vm.inputFocus = true;
      vm.closeSelectCountryModal();
    };

    vm.showCountryModal = function() {
      $scope.inLogin = true;
      $ionicModal.fromTemplateUrl('templates/modals/country-select-modal.html', {
        scope: $scope
      }).then(function(modal) {
        vm.modal = modal;
        vm.modal.show();
      });

      vm.emailRecoveryPopup.close();
    };

    vm.scrollToTopCountrySelect = function() {
      $ionicScrollDelegate.$getByHandle('countrySelectScroll').scrollTop(true);
    };

    vm.onSearchChanged = function() {
      if (vm.searchKey == null || vm.searchKey === "") {
        vm.searchCountryList = vm.countryList;
        return;
      }
      var keyForSearch = vm.searchKey.toLowerCase();
      vm.scrollToTopCountrySelect();

      vm.searchCountryList=vm.countryList.filter(function(el) {
        return (el.country_code.toLowerCase().indexOf(keyForSearch) > -1)
            || (el.country.toLowerCase().indexOf(keyForSearch) > -1)
            || (el.iso.toLowerCase().indexOf(keyForSearch) > -1)
            || (vm.langCode=="en" && el.country.toLowerCase().indexOf(keyForSearch) > -1)
            || (vm.langCode=="es" && el.country_es.toLowerCase().indexOf(keyForSearch) > -1)
      });
      vm.currentCountryList = vm.searchCountryList.slice(0, vm.blockShow);

    };

    vm.howToPopup = function() {
      DialogService.alert("", vm.translations.How_do_info1, angular.noop,$scope,'local-call-popup custom-popup popup-body-center','info');
    };

    vm.authError = null;
    vm.recoverEmail = function () {
      var content = '<div style="position: absolute; top: 3px; right: 7px; font-size:20px" ng-Click="loginCtrl.emailRecoveryPopup.close();"><i class="icon ion-ios-close-outline"></i></div>'+
          "<div class='call-help-content country-select-recover-email'" +
            //" <p>" + vm.translations["What_Country_Do_You_Live"] + "</p>" +
            //"<country-fullscreen-select " +
            //    "select-type='recovery-email' " +
            //    "select-style='normal' " +
            //    "country-selected='loginCtrl.countrySelected' " +
            //    "lang-code='loginCtrl.langCode' " +
            //    "on-external-modal-show='loginCtrl.showCountryModal();' " +
            //    //"on-country-view-close='loginCtrl.showSelectCountryModal();' " +
            //    "on-country-view-open='loginCtrl.closeEmailRecoveryPopup();' " +
            //"></country-fullscreen-select>" +

          "<div class='country-selection'>" +
          " <p>" + vm.translations["What_Country_Do_You_Live"] + "</p>" +
          "<label class = 'item item-input item-select select-center item-flag' ng-click='loginCtrl.showCountryModal();'>" +
          "<div class = 'input-label'>" +
          "<span class='flag-icon flag-icon-{{loginCtrl.countrySelected.iso | lowercase}}'  ng-class='{\"visible-hidden\":!loginCtrl.countrySelected.iso}'></span>" +
          "</div>" +
          "<div ng-if='loginCtrl.countrySelected != null'>" +
          "<span ng-if=\"loginCtrl.langCode == 'en'\">{{loginCtrl.countrySelected.country}}</span>" +
          "<span ng-if=\"loginCtrl.langCode == 'es'\">{{loginCtrl.countrySelected.country_es}}</span>" +
          "</div>" +
          "</label>" +
          "</div>" +
          "<span class='item item-button-left country-code-wrap normalize-inline' ng-show='loginCtrl.countrySelected.iso'>" +
            "<div class='input-label country-code-label item-float-left input-select-a' >" +
              "<span class='custom-country-code'> +{{loginCtrl.countrySelected.country_code}}</span>" +
            "</div>" +
            "<input class='input select-phonenumber-label input-select-c' autocomplete='off' autocorrect='off' autocapitalize='off' type='tel' ng-model='loginCtrl.recoveryNumber' placeholder='{{\"type_phone_number_here\" | translate}}' only-digits>" +
            "<a class='input-guide input-guide-normal' ng-click='loginCtrl.howToPopup()'>" +
              "<span class='question-guide'>?</span>" +
            "</a>" +
          "</span>" +

          "</div>";
      var options = {
        title:vm.translations["Recover_Email"],
        template:content,
        scope: $scope,
        cssClass: "local-call-popup phone-number-popup",
        buttons: [
          // {
          //   text: vm.translations["Cancel"],
          //   onTap: function(e){
          //     return false;
          //   }
          // },
          {
            text: '<b>' + vm.translations["Get_It_Back"] + '</b>',
            type: 'button-positive',
            onTap: function(e) {
              if(!vm.countrySelected){
                e.preventDefault();
                $ionicLoading.notify(vm.translations.please_select_country);
                return;
              }
              else if(!vm.recoveryNumber){
                e.preventDefault();
                $ionicLoading.notify(vm.translations.please_enter_number);
                return;
              }

              $ionicLoading.show();
              AuthService.lostEmail({
                country_code: vm.countrySelected.country_code,
                phone: vm.recoveryNumber
              }).then(function(res){
                $ionicLoading.hide();
                DialogService.alert(vm.translations.Recover_Email, vm.translations.your_email_is + res.email, angular.noop,$scope,'local-call-popup custom-popup','info');
                vm.User.userName=res.email;
                vm.User.password="";

              },function(err){
                $ionicLoading.hide();
                if(err.error_code=="phone_not_found"){
                  $ionicLoading.notify(vm.translations.phone_number_not_found);
                }
                else{
                  $ionicLoading.notify(vm.translations.could_not_send_code_to_number);
                }
              })
            }
          }
        ]
      };
      vm.emailRecoveryPopup = $ionicPopup.show(options);

    };
    vm.recoverPassword_function = function(cancel){
      if(cancel==true)
      {
        vm.passwordRecoveryPopup.close();
      }
      else
      {
        if(!vm.recoveryEmail){

          $ionicLoading.notify(vm.translations.please_enter_email);
          return;
        }
        $ionicLoading.show();
        AuthService.lostPassword({
          email: vm.recoveryEmail
        }).then(function(res){
          $ionicLoading.hide();
          DialogService.alert(vm.translations.hablax, vm.translations.password_sent_to_email, angular.noop,$scope,'local-call-popup custom-popup popup-body-center','success');
        },function(err){
          $ionicLoading.hide();
          $ionicLoading.notify(vm.translations.could_not_send_password_to_email);
        });
        vm.passwordRecoveryPopup.close();
      }



    };

    vm.recoverPassword = function () {
      var content = '<div style="position: absolute; top: 3px; right: 7px; font-size:20px" ng-Click="loginCtrl.recoverPassword_function(true);"><i class="icon ion-ios-close-outline"></i></div>'+
          "<div class='call-help-content'>" +
          " <p>" + vm.translations["Recover_Password_Content"]+ "</p>" +
          " <p>Email</p>" +
          " <div class='item item-input'>" +
          " <form ng-submit='loginCtrl.recoverPassword_function()'>  <input type='email' ng-model='loginCtrl.recoveryEmail'></div>" +
          "</div>"+
          "<button ng-click='loginCtrl.recoverPassword_function()' class='button button-full button-balanced' style='text-transform: uppercase;'>{{'Get_It_Back' | translate}}</button>"+
          "<button ng-click='loginCtrl.customerSupport()' class='button button-full button-positive' style='text-transform: uppercase;'>{{'contact_us_link' | translate}}</button></form>";
      // "<button ng-click='loginCtrl.recoverPassword_function(true)' class='button button-full button-assertive' style='text-transform: uppercase;'>{{'Cancel' | translate}}</button>";

      var options = {
        title:vm.translations["Recover_Password"],
        template:content,
        scope: $scope,
        cssClass: "local-call-popup"
        // buttons: [

        //   {
        //     text: '<b>' + vm.translations["Get_It_Back"] + '</b>',
        //     type: 'button-block button-positive',
        //     onTap: function(e) {
        //       if(!vm.recoveryEmail){
        //         e.preventDefault();
        //         $ionicLoading.notify(vm.translations.please_enter_email);
        //         return;
        //       }
        //       $ionicLoading.show();
        //       AuthService.lostPassword({
        //         email: vm.recoveryEmail
        //       }).then(function(res){
        //         $ionicLoading.hide();
        //         DialogService.alert(vm.translations.hablax, vm.translations.password_sent_to_email, angular.noop,$scope,'local-call-popup custom-popup','success');
        //       },function(err){
        //         $ionicLoading.hide();
        //         $ionicLoading.notify(vm.translations.could_not_send_password_to_email);
        //       });
        //       return false;
        //     }
        //   },
        //    {
        //     text: vm.translations["Cancel"],
        //     type: 'button-block',

        //     onTap: function(e){
        //       return false;
        //     }
        //   }
        // ]
      };
      vm.passwordRecoveryPopup = $ionicPopup.show(options);


    };


    vm.Authenticate = function () {
      $log.debug(vm.User);
      $ionicLoading.show();
      AuthService.login(vm.User.userName, vm.User.password, vm.User.remember).then(function (data) {
        vm.showError= false;
        $log.debug(data);
        $ionicHistory.nextViewOptions({
          disableAnimate: true,
          disableBack: true
        });



        if (ionic.Platform.isAndroid() || ionic.Platform.isIOS()) {
          if (!device.isVirtual) {
            FCMPlugin.getToken(function(token){
              if (!token) {
                return
              }
              PushService.submitToken({
                pushToken: token,
                devicePlatform: device.platform,
                deviceModel: device.model,
                deviceVersion: device.version,
                deviceUUID: device.uuid
              }).then(function (data) {
                // nothing
              });

              //PushService.submitTest({
              //  token: token
              //}).then(function (data) {
              //  // nothing
              //});
            });

            UserService.getUserProfile().then(function (data) {
              var topicKey = "iso-"+data.iso.toLowerCase();
              FCMPlugin.subscribeToTopic(topicKey);
              PushService.submitTopic({
                topicName: "Push with country ISO: " + data.iso.toUpperCase(),
                topicString: topicKey
              }).then(function (data) {
                // nothing
              });
            });

          }
        }

        $state.go('tab.dial');

        $ionicLoading.hide();
      }, function (data) {
        if (data.error_code == "invalid_account_data") {
          // vm.showError= true;
          // vm.authError = vm.translations.invalid_username_or_password_please_retry;
          DialogService.alert(vm.translations.HELP, vm.translations.invalid_username_or_password_please_retry, angular.noop,$scope,'local-call-popup custom-popup popup-body-center','error');

        } else if (data.error_code == "account_restricted") {
          // vm.showError= true;
          // vm.authError = vm.translations.account_restricted_message;
          DialogService.alert(vm.translations.HELP, vm.translations.account_restricted_message, angular.noop,$scope,'local-call-popup custom-popup popup-body-center','error');

          //DialogService.alert(vm.translations.warning, vm.translations.account_restricted_message, angular.noop,$scope,'local-call-popup');
        }
        else {
          DialogService.alert(vm.translations.HELP, vm.translations.account_restricted_message, angular.noop,$scope,'local-call-popup custom-popup popup-body-center','info');

          // DialogService.alert(vm.translations.HELP, vm.translations.internet_unreachable, angular.noop,$scope,'local-call-popup');
          console.error(data);
        }
        $ionicLoading.hide();
      });

    };

    vm.ForgotPass = function () {
      $log.debug('Forgot Password Event Starting');
      // $state.go('forgot-pass');
    };

  }]);

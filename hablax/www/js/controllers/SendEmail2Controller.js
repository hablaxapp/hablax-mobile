
hablaxControllers.controller('SendEmail2Controller', function($ionicPopup,$state, $scope,$ionicHistory, $ionicLoading, $log, $translate, $scope,$localStorage,UserService, SupportService, DialogService) {

  var vm = this;

  vm.phoneContacts = [];

  vm.emailSubject;
  vm.emailMessage;


  vm.translations={};
  $translate(['email_sent', 'email_error_sent', 'enter_subject_message']).then(function (translations) {
    vm.translations=translations;
    console.log(vm.translations);
  }, function (translationIds) {
    vm.translations=translationIds;
    console.log(vm.translations);
  });
 vm.goBack = function(){
    $ionicHistory.goBack();
  }

  vm.goHistory = function(index) {
    var listItem = self.messagesList.filter(function (el) {
      return el.key === index;
    });
    if (listItem.length > 0) {
      item = listItem[0];
      item.number = item.destination;
      $state.go('tab.messages.write-message', {'contact': item});
    }
  };

  vm.sendEmail = function(){
    if(!vm.emailSubject || !vm.emailMessage){
      $ionicLoading.notify(vm.translations.enter_subject_message);
      return;
    }
    $ionicLoading.show();

    var params = {
      subject: vm.emailSubject,
      message: vm.emailMessage
    };
    UserService.sendEmailToSupport2(params).then(function(){
      $ionicLoading.notify(vm.translations.email_sent);
      $state.go('home');

    },function(res){
      $ionicLoading.notify(vm.translations.email_error_sent);
    });


    //var options = {
    //  title: "Send Email",
    //  template:vm.sendEmailTemplate,
    //  scope: $scope,
    //  buttons: [
    //    {
    //      text: "Cancel",
    //      onTap: function(e){
    //        return false;
    //      }
    //    },
    //    {
    //      text: '<b>Send</b>',
    //      type: 'button-positive',
    //      onTap: function(e) {
    //        if(!vm.params.subject || !vm.params.message){
    //          e.preventDefault();
    //          $ionicLoading.notify(vm.translations.enter_subject_message);
    //          return;
    //        }
    //        $ionicLoading.show();
    //        UserService.sendEmailToSupport(vm.params).then(function(){
    //          $ionicLoading.notify(vm.translations.email_sent);
    //        },function(res){
    //          $ionicLoading.notify(vm.translations.email_error_sent);
    //        });
    //        return false;
    //      }
    //    }
    //  ]
    //};
    //vm.passwordRecoveryPopup = $ionicPopup.show(options);
  }


});

var hablaxDirectives = angular.module('hablax.directives',[]);


hablaxDirectives.directive('focusMe', function($timeout) {
    return {
        scope: { trigger: '@focusMe' },
        link: function(scope, element) {
            scope.$watch('trigger', function(value) {
                if(value === "true") {
                    $timeout(function() {
                        element[0].focus();
                    });
                }
            });
        }
    };
});

hablaxDirectives.directive('elastic', [
    '$timeout',
    function($timeout) {
        return {
            restrict: 'A',
            link: function($scope, element) {
                $scope.initialHeight = $scope.initialHeight || element[0].style.height;
                var resize = function() {
                    element[0].style.height = $scope.initialHeight;
                    element[0].style.height = "" + element[0].scrollHeight + "px";
                };
                element.on("input change", resize);
                $timeout(resize, 0);
            }
        };
    }
]);

hablaxDirectives.directive('onlyDigits', function () {
    return {
        require: 'ngModel',
        restrict: 'A',
        link: function (scope, element, attr, ctrl) {
            function inputValue(val) {
                if (val) {
                    var digits = val.replace(/[^0-9]/g, '');

                    if (digits !== val) {
                        ctrl.$setViewValue(digits);
                        ctrl.$render();
                    }
                    return parseInt(digits,10);
                }
                return undefined;
            }
            ctrl.$parsers.push(inputValue);
        }
    };
});

hablaxDirectives.directive('onlyMessage', function () {
    return {
        require: 'ngModel',
        restrict: 'A',
        link: function (scope, element, attr, ctrl) {
            function inputValue(val) {
                if (val) {
                    var value = val.replace(/[^0-9a-zA-Z\ \.\,\-\_\!\?\u00F1A]/g, '');

                    if (value !== val) {
                        ctrl.$setViewValue(value);
                        ctrl.$render();
                    }
                    return value;
                }
                return undefined;
            }
            ctrl.$parsers.push(inputValue);
        }
    };
});

app.directive('myShow', function($animate) {
    return {
        scope: {
            'myShow': '=',
            'afterShow': '&',
            'afterHide': '&'
        },
        link: function(scope, element) {
            scope.$watch('myShow', function(show, oldShow) {
                if (show) {
                    $animate.removeClass(element, 'ng-hide').then(scope.afterShow);
                }
                if (!show) {
                    $animate.addClass(element, 'ng-hide').then(scope.afterHide);
                }
            });
        }
    }
});

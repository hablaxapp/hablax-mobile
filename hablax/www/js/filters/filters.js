
angular.module('hablax')
.filter('rates', function () {
  return function (item,currency,rates) {
    if(!item || !currency || !rates) return item;

    else{
      rates = rates.rates? rates.rates : rates;
      return item * rates[currency];
    }
  }
})
.filter('country_contact',function(){
  return function(contacts, selectedCountry){
    if (selectedCountry == null) {
      return [];
    }

    if(!selectedCountry.country_code || !contacts) return contacts;
    var list = [];
    for(var i=0,l=contacts.length; i<l; i++){
      if(contacts[i].country_code == selectedCountry.country_code
          && contacts[i].contact_country_iso == selectedCountry.iso){
        list.push(contacts[i]);
      }
    }
    return list;
  }
});

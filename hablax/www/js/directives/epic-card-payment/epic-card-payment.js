
var epicCardPayment = angular.module('epic-card-payment',['credit-cards']);

epicCardPayment.directive('epicCardPayment',function(){
  var definition = {
    restrict: "AE",
    link: linkFn,
    templateUrl: 'js/directives/epic-card-payment/epic-card.template.html',
    controller: controllerFn,
    controllerAs: "epicCardCtrl",
    bindToController: {
      card:"=",
      onPayment: "&"
    }
  };

  function linkFn(scope,elem,attrs){

  }

  function controllerFn($scope,$ionicPopup, UserService, $log, $translate,StoreService){
    var vm = this;
    vm.cardMonths = [{"month":"01"},{"month":"02"},{"month":"03"},{"month":"04"},{"month":"05"},{"month":"06"},{"month":"07"},{"month":"08"},{"month":"09"},{"month":"10"},{"month":"11"},{"month":"12"}];
    vm.cardYears = [{"year":2016},{"year":2017},{"year":2018},{"year":2019},{"year":2020},{"year":2021},{"year":2022},{"year":2023},{"year":2024},{"year":2025},{"year":2026},{"year":2027},{"year":2028},{"year":2029},{"year":2030}];
    vm.makeCardPayment = function(){
      $log.debug("console from directive");
      vm.onPayment({card: vm.card});
    };
    vm.cardModes = {
      NEW_CARD: "new",
      SAVED_CARDS: "saved"
    };
    vm.hasCards = false;
    vm.card.mode = vm.cardModes.NEW_CARD;
    vm.card.selectedCard = null;


    vm.translations={};
    $translate(['zip_code_explain']).then(function (translations) {
      vm.translations=translations;
      console.log(vm.translations);
    }, function (translationIds) {
      vm.translations=translationIds;
      console.log(vm.translations);
    });

    $scope.$on("languageChanged",function(e,args){
      $translate(['zip_code_explain']).then(function (translations) {
        vm.translations=translations;
        console.log(vm.translations);
      }, function (translationIds) {
        vm.translations=translationIds;
        console.log(vm.translations);
      });
    });

    UserService.getUserProfile().then(function(profile){
      if (profile) {
        vm.profile = profile;
        if(vm.profile && vm.profile.anet && vm.profile.anet.payment_profiles && vm.profile.anet.payment_profiles.length>0){
          vm.hasCards = true;
          vm.card.mode = vm.cardModes.SAVED_CARDS;
          vm.cards = vm.profile.anet.payment_profiles;
        }
      }
    },function(err){
      $log.debug(err);
    });

    vm.useNewCard = function(){
      vm.card.mode = vm.cardModes.NEW_CARD;
    };

    vm.useSavedCard = function(){
      vm.card.mode = vm.cardModes.SAVED_CARDS;
    };

    vm.zipHelp = function(){
      var alertPopup = $ionicPopup.alert({
        title: 'Zip code',
        template: vm.translations.zip_code_explain
      });
      alertPopup.then(function(res) {
        $log.debug('Thank you for not eating my delicious ice cream cone');
      });
    };

    function _cvcHelp(language) {
      var alertPopup = $ionicPopup.alert({
        title: 'CVV',
        template: '<img src="img/cvv_'+language+'.png" class="img-responsive">'
      });
      alertPopup.then(function(res) {
        $log.debug('Thank you for not eating my delicious ice cream cone');
      });
    }

    vm.cvcHelp = function(){
      StoreService.get("hablax_lang",
          function (value) {
            if (value) {
              var language = value;
              _cvcHelp(language);
            } else {
              _cvcHelp("en");
            }
          }, function (error) {
            _cvcHelp("en");
          });

    };
  }
  return definition;
});

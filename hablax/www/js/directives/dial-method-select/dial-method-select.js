
var dialMethodSelect = angular.module('dial-method-select',[]);

dialMethodSelect.directive('dialMethodSelect',function(){
  var definition = {
    restrict: "AE",
    link: linkFn,
    templateUrl: 'js/directives/dial-method-select/dial-method.template.html',
    controller: controllerFn,
    controllerAs: "dialMethodCtrl",
    bindToController: {
      dialNumber:"=",
      selectedCountry: "=",
      onHideView: "&",
      onLocalCall: "&",
      onWifiCall: "&"
    }
  };

  function linkFn(scope,elem,attrs){

  }

  function controllerFn($scope, DialogService, $translate){
    var vm = this;

    vm.translations={};

    function _translate() {
      $translate(['Help_Button_Wifi', 'Local Number', 'Help_Button_Local_Content', 'Help_Button_Wifi_Content',
          'HELP']).then(function (translations) {
        vm.translations=translations;
      }, function (translationIds) {
        vm.translations=translationIds;
      });
    }
    _translate();
    $scope.$on("languageChanged",function(e,args){
      _translate();
    });



    vm.hideChoices = function(){
      vm.onHideView();
    };

    vm.localCall = function() {
      vm.onLocalCall();
    };

    vm.wifiCall = function() {
      vm.onWifiCall();
    };

    vm.showHelpButtonContent= function(){
      var content = "<div class='call-help-content'>" +
          "<div><b><h4>" + vm.translations["Local Number"] + "</h4></b> <p>" + vm.translations.Help_Button_Local_Content + "</p></div>" +
          "<div><b><h4>" + vm.translations.Help_Button_Wifi + "</h4></b> <p>" + vm.translations.Help_Button_Wifi_Content + "</p></div>" +
          "</div>";
      DialogService.alert(vm.translations.HELP,content,angular.noop,$scope,'local-call-popup');
    };

  }
  return definition;
});

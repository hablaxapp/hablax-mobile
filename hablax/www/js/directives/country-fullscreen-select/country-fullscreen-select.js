
var epicCardPayment = angular.module('country-fullscreen-select',['credit-cards']);

epicCardPayment.directive('countryFullscreenSelect',function(){
  var definition = {
    restrict: "AE",
    link: linkFn,
    templateUrl: 'js/directives/country-fullscreen-select/country-fullscreen-select.html',
    controller: controllerFn,
    controllerAs: "countrySelectCtrl",
    bindToController: {
      selectType:"@",
      countrySelected:"=",
      langCode:"=",
      selectStyle:"@",
      phoneNumber:"=",
      onExternalModalShow:"&",
      onCountrySelected: "&",
      onCountryViewOpen: "&",
      onCountryViewClose: "&"
    }
  };

  function linkFn(scope,elem,attrs){
    scope.$watch("countryList",function(newValue,oldValue) {
      //if (scope.countryList != null) {
      //  scope.searchCountryList = scope.countryList;
      //}
    });

  }

  function controllerFn($scope, $ionicModal, AuthService, RatesService, RechargeService, $ionicLoading,
                        $translate, StoreService, $log, $ionicScrollDelegate, $ionicPopup, DialogService){
    var vm = this;
    vm.inputFocus = false;
    vm.allDataLoaded = false;
    vm.translations = {};
    vm.blockShow = 20;


    function _getCountryList(){

        if (vm.selectType == "sms-rate") {

        } else if (vm.selectType == "call-rate") {

        } else if (vm.selectType == "mobile-recharge") {

        } else { // Default

        }

    }

    function _doTranslate() {
      $translate(['failed_get_sms_country_list', 'contact_number', 'How_do_info1']).then(function (translations) {
        vm.translations=translations;
        //_getCountryList();
      }, function (translationIds) {
        vm.translations=translationIds;
        //_getCountryList();
      });

      StoreService.get("hablax_lang",
          function (value) {
            if (value) {
              vm.langCode = value;
            } else {
              vm.langCode = "en";
            }
          }, function (error) {
            vm.langCode = "en";
          });
    }
    _doTranslate();

    $scope.$on("languageChanged",function(e,args){
      _doTranslate();
    });

    vm.loadMore = function(){
      if (vm.selectType == "sms-rate") {

        if (vm.smsRateList==null) {
          RatesService.getCountryList().then(function (countries) {
            vm.smsRateList = countries;
            vm.searchSmsRateList = countries;
            vm.currentSmsRateList = countries.slice(0, vm.blockShow);
            $log.debug(vm.smsRateList);
          }, function (resp) {
            $ionicLoading.notify(vm.translations.failed_get_sms_country_list);
          });
        } else {
          vm.currentSmsRateList = vm.searchSmsRateList
              .slice(0, vm.currentSmsRateList.length + vm.blockShow);
        }

        $scope.$broadcast('scroll.infiniteScrollComplete');
        $scope.$broadcast('scroll.refreshComplete');

      } else if (vm.selectType == "call-rate") {

        if (vm.callRateList==null) {
          RatesService.getPrices().then(function (prices) {
            vm.callRateList = prices.calls;
            vm.searchCallRateList = prices.calls;
            vm.currentCallRateList = prices.calls.slice(0, vm.blockShow);
            $log.debug(vm.callRateList);
          }, function (resp) {
            $ionicLoading.notify(vm.translations.failed_get_sms_country_list);
          });
        } else {
          vm.currentCallRateList = vm.searchCallRateList
              .slice(0, vm.currentCallRateList.length + vm.blockShow);
        }

        $scope.$broadcast('scroll.infiniteScrollComplete');
        $scope.$broadcast('scroll.refreshComplete');

      } else if (vm.selectType == "mobile-recharge") {

        if (vm.mobileRechargeList==null) {
          RechargeService.getCountriesToRecharge().then(function (countries) {
            vm.mobileRechargeList = countries;
            vm.searchMobileRechargeList = countries;
            vm.currentMobileRechargeList = countries.slice(0, vm.blockShow);
            $log.debug(vm.mobileRechargeList);
          }, function (resp) {
            //  alert('Failed to get sms Country List');
          });
        } else {
          vm.currentMobileRechargeList = vm.searchMobileRechargeList
              .slice(0, vm.currentMobileRechargeList.length + vm.blockShow);
        }


        $scope.$broadcast('scroll.infiniteScrollComplete');
        $scope.$broadcast('scroll.refreshComplete');

      } else { // Default
        vm.selectType = "default";

        if (vm.countryList==null) {
          AuthService.getRegistrationCountries().then(function(countries){
            vm.countryList = countries;
            vm.searchCountryList = countries;
            vm.currentCountryList = countries.slice(0, vm.blockShow);
          },function(err){
            $log.debug(err);
          });
        } else {
          vm.currentCountryList = vm.searchCountryList
              .slice(0, vm.currentCountryList.length + vm.blockShow);
        }

        $scope.$broadcast('scroll.infiniteScrollComplete');
        $scope.$broadcast('scroll.refreshComplete');

      }


    };

    vm.howToPopup = function() {
      DialogService.alert("", vm.translations.How_do_info1, angular.noop,$scope,'local-call-popup custom-popup popup-body-center','info');
    };

    vm.warnPhoneNumber = function() {
      //if (phoneNumber != null) {
      //  el.country_code.toLowerCase().indexOf(keyForSearch) > -1
      //}
      //var alertPopup = $ionicPopup.alert({
      //  title: vm.translations.contact_number,
      //  template:
      //  '<p class="hl-text-justify">{{"How_do_info1" | translate}}</p>' +
      //  '<p class="hl-text-justify">{{"How_do_info2" | translate}}</p>'
      //});
    };

    $scope.$watch('phoneNumber', function (newValue, oldValue) {
      if (newValue !== oldValue) {
        // It's updated - Do something you want here.
        vm.howToPopup();
      }
    });

    vm.showCountrySelection = function() {

      if (vm.selectType=='recovery-email') {
        vm.onExternalModalShow();
      } else {
        $ionicModal.fromTemplateUrl('templates/modals/country-select-modal.html', {
          scope: $scope
        }).then(function(modal) {
          vm.callModal = modal;
          vm.callModal.show();
        });
      }


      if (vm.onCountryViewOpen != null) {
        vm.onCountryViewOpen();
      }

    };

    vm.hideCountrySelection = function() {
      vm.callModal.hide();
      if (vm.onCountryViewClose != null) {
        vm.onCountryViewClose();
      }
    };

    vm.selectCountry = function(index) {
      vm.countrySelected = vm.searchCountryList[index];
      vm.inputFocus = true;
      if (vm.onCountrySelected != null) {
        vm.onCountrySelected({countrySelected: vm.countrySelected});
      }
      vm.hideCountrySelection();
    };

    vm.selectCallRate = function(index) {
      vm.countrySelected = vm.searchCallRateList[index];
      vm.inputFocus = true;
      if (vm.onCountrySelected != null) {
        vm.onCountrySelected({countrySelected: vm.countrySelected});
      }
      vm.hideCountrySelection();
    };

    vm.selectSmsRate = function(index) {
      vm.countrySelected = vm.searchSmsRateList[index];
      vm.inputFocus = true;
      if (vm.onCountrySelected != null) {
        vm.onCountrySelected({countrySelected: vm.countrySelected});
      }
      vm.hideCountrySelection();
    };

    vm.selectMobileRecharge = function(index) {
      vm.countrySelected = vm.searchMobileRechargeList[index];
      vm.inputFocus = true;
      if (vm.onCountrySelected != null) {
        vm.onCountrySelected({countrySelected:vm.countrySelected});
      }
      vm.hideCountrySelection();
    };

    vm.scrollToTop = function() {
      $ionicScrollDelegate.$getByHandle('countrySelectScroll').scrollTop(true);
    };

    vm.onSearchChanged = function() {
      if (vm.searchKey == null || vm.searchKey === "") {
        vm.searchCountryList = vm.countryList;
        return;
      }
      var keyForSearch = vm.searchKey.toLowerCase();
      vm.scrollToTop();
      if (vm.selectType == "sms-rate") {
        vm.searchSmsRateList=vm.smsRateList.filter(function(el) {
          return (el.country_code.toLowerCase().indexOf(keyForSearch) > -1)
              || (el.country.toLowerCase().indexOf(keyForSearch) > -1)
              || (el.iso.toLowerCase().indexOf(keyForSearch) > -1)
              || (vm.langCode=="en" && el.country.toLowerCase().indexOf(keyForSearch) > -1)
              || (vm.langCode=="es" && el.country_es.toLowerCase().indexOf(keyForSearch) > -1)
        });
        vm.currentSmsRateList = vm.searchSmsRateList.slice(0, vm.blockShow);
      } else if (vm.selectType == "call-rate") {
        vm.searchCallRateList=vm.callRateList.filter(function(el) {
          return (el.country.toLowerCase().indexOf(keyForSearch) > -1)
              || (el.iso.toLowerCase().indexOf(keyForSearch) > -1)
              || (vm.langCode=="en" && el.country.toLowerCase().indexOf(keyForSearch) > -1)
              || (vm.langCode=="es" && el.country_es.toLowerCase().indexOf(keyForSearch) > -1)
        });
        vm.currentCallRateList = vm.searchCallRateList.slice(0, vm.blockShow);
      } else if (vm.selectType == "mobile-recharge") {
        vm.searchMobileRechargeList=vm.mobileRechargeList.filter(function(el) {
          return (el.country_code.toLowerCase().indexOf(keyForSearch) > -1)
              || (el.country.toLowerCase().indexOf(keyForSearch) > -1)
              || (el.iso.toLowerCase().indexOf(keyForSearch) > -1)
              || (vm.langCode=="en" && el.country.toLowerCase().indexOf(keyForSearch) > -1)
              || (vm.langCode=="es" && el.country_es.toLowerCase().indexOf(keyForSearch) > -1)
        });
        vm.currentMobileRechargeList = vm.searchMobileRechargeList.slice(0, vm.blockShow);
      } else { // Default
        vm.searchCountryList=vm.countryList.filter(function(el) {
          return (el.country_code.toLowerCase().indexOf(keyForSearch) > -1)
              || (el.country.toLowerCase().indexOf(keyForSearch) > -1)
              || (el.iso.toLowerCase().indexOf(keyForSearch) > -1)
              || (vm.langCode=="en" && el.country.toLowerCase().indexOf(keyForSearch) > -1)
              || (vm.langCode=="es" && el.country_es.toLowerCase().indexOf(keyForSearch) > -1)
        });
        vm.currentCountryList = vm.searchCountryList.slice(0, vm.blockShow);
      }

    };

  }
  return definition;
});
